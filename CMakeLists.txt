#
#
# root project List...
# usage
# mkdir -vp bld
# cd bld
# cmake ..
# enjoy...
#

project (OpenAirInterface C CXX)


cmake_minimum_required (VERSION 2.8.2)

if(${CMAKE_MAJOR_VERSION} GREATER 3)
  #  cmake_policy(SET CMP0065 OLD)
  #For compatibility with older versions of CMake, additional flags may be
  #added to export symbols on all executables regardless of thier
  #ENABLE_EXPORTS property.
endif()

list(INSERT CMAKE_MODULE_PATH 0
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets"
  "${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets/tools/MODULES"
  "${PROJECT_BINARY_DIR}"
  )

list(APPEND  CMAKE_PREFIX_PATH  ${PROJECT_BINARY_DIR})

# Offer the user the choice of overriding the installation directories
set(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")
set(INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables")
set(INSTALL_INCLUDE_DIR share/oai
  CACHE PATH "Installation directory for header files"
  )
if(WIN32 AND NOT CYGWIN)
  set(DEF_INSTALL_CMAKE_DIR CMakeFiles)
else()
  set(DEF_INSTALL_CMAKE_DIR CMakeFiles/oai)
endif()
#set(DEF_INSTALL_CMAKE_DIR .) #for nothing

set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR}
  CACHE PATH  "Installation directory for CMake files"
  )

if(NOT IS_ABSOLUTE "${INSTALL_INCLUDE_DIR}")
  set(BINARY_INCLUDE_DIR ${PROJECT_BINARY_DIR}/${INSTALL_INCLUDE_DIR})
endif()

#force this to project binary dir
#if (DEFINED OPENAIRINTERFACE_BINARY_DIR)
#  set(CMAKE_INSTALL_PREFIX  ${OPENAIRINTERFACE_BINARY_DIR})
#endif()

# Make relative paths absolute (needed later on)
foreach(p LIB BIN INCLUDE CMAKE)
  set(var INSTALL_${p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
  endif()
endforeach()

include(mExternalProject)

# set up include-directories
include_directories(
  "${PROJECT_SOURCE_DIR}"   # to find foo/foo.h
  "${PROJECT_BINARY_DIR}"   # to find foo/config.h
  )

list(APPEND CONF_INCLUDE_DIRS
  "${PROJECT_SOURCE_DIR}"   # to find foo/foo.h
  "${PROJECT_BINARY_DIR}"   # to find foo/config.h
  )



#CYGIN and epoll
#################
include(CheckIncludeFile)
check_include_file("sys/epoll.h"  HAVE_SYS_EPOLL_H)
if(NOT "${HAVE_SYS_EPOLL_H}" AND "${CYGWIN}")
  set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/upoll)
  ExternalProject_Add(ep_upoll
    PREFIX ${PROJECT_BINARY_DIR}
    #GIT_REPOSITORY https://github.com/richardhundt/upoll.git
    GIT_REPOSITORY git://koala/upoll.git
    SOURCE_DIR ${_src_d}
    UPDATE_DISCONNECTED 1
    #CONFIGURE_COMMAND ${_src_d}/configure --prefix=${CMAKE_CURRENT_BINARY_DIR} --enable-shared
    CONFIGURE_COMMAND ${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared
    #BINARY_DIR    ${_src_d}
    BUILD_COMMAND make
    #INSTALL_DIR  ${PROJECT_BINARY_DIR}/lib
    #INSTALL_COMMAND install -D  ${_src_d}/src/libup.so ${PROJECT_BINARY_DIR}/lib/libup.dll && install -D  ${_src_d}/src/upoll.h ${PROJECT_BINARY_DIR}/include/upoll.h && install -D ${_src_d}/src/include/up.h ${PROJECT_BINARY_DIR}/include/up.h
    INSTALL_COMMAND make install
    BUILD_BYPRODUCTS libup.la up.h ${CMAKE_SHARED_LIBRARY_PREFIX}up-0${CMAKE_SHARED_LIBRARY_SUFFIX}
    STEP_TARGETS  install
    )
  ExternalProject_Add_Step(ep_upoll autoupdate
    DEPENDEES download
    COMMAND autoupdate --verbose
    WORKING_DIRECTORY ${_src_d}
    )
  ExternalProject_Add_Step(ep_upoll init
    DEPENDEES autoupdate
    COMMAND autoreconf --force --install --verbose --warnings=all --symlink
    COMMENT "asn1c OAI customised :: being autoreconfed"
    WORKING_DIRECTORY ${_src_d}
    BYPRODUCTS configure Makefile.in config.h.in
    )

  
  set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/cygepoll)
  set(EP_SYS_EPOLL_SOURCE_DIR  ${_src_d})
  ExternalProject_Add(ep_sys_epoll
    PREFIX ${PROJECT_BINARY_DIR}
    DEPENDS ep_upoll
    ##GIT_REPOSITORY git@github.com:fd00/cygepoll.git
    #GIT_REPOSITORY https://github.com/fd00/cygepoll.git
    GIT_REPOSITORY git://koala/cygepoll.git
    SOURCE_DIR ${_src_d}
    #CONFIGURE_COMMAND test -f config.status
    #CONFIGURE_COMMAND ${_src_d}/configure --prefix=${CMAKE_CURRENT_BINARY_DIR} --enable-shared UPOLL_LIBS=${PROJECT_BINARY_DIR}/lib/libup.la UPOLL_CFLAGS=-I"${PROJECT_BINARY_DIR}/include/include" UPOLL_LDFLAGS="-L${PROJECT_BINARY_DIR}/lib"
    CONFIGURE_COMMAND ${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared UPOLL_LIBS=${PROJECT_BINARY_DIR}/lib/libup.la UPOLL_CFLAGS=-I"${PROJECT_BINARY_DIR}/include/include" UPOLL_LDFLAGS="-L${PROJECT_BINARY_DIR}/lib"
    
    UPDATE_DISCONNECTED 1
    BUILD_COMMAND make
    INSTALL_COMMAND make install
    BUILD_BYPRODUCTS sys/epoll.h libepoll.dll.a config.status ${CMAKE_SHARED_LIBRARY_PREFIX}epoll-0${CMAKE_SHARED_LIBRARY_SUFFIX}
    )
  #patch libepoll_la_CFLAGS = $(UPOLL_CFLAGS) into Makefile.am
  ExternalProject_Add_Step(ep_sys_epoll autoupdate
    DEPENDEES download
    COMMAND autoupdate --verbose
    WORKING_DIRECTORY ${_src_d}
    )
  ExternalProject_Add_Step(ep_sys_epoll init
    DEPENDEES autoupdate
    COMMAND autoreconf --force --install --verbose --warnings=all --symlink
    COMMENT "asn1c OAI customised :: being autoreconfed"
    WORKING_DIRECTORY ${_src_d}
    BYPRODUCTS configure Makefile.in config.h.in
    )

  set(_lib_rdx epoll)
  #add_dependencies(ep_sys_epoll ep_upoll)
  set(EPOLL_TARGETS ep_sys_epoll)
  
  externalproject_get_property(ep_sys_epoll install_dir binary_dir)
  #include_directories(${install_dir}/lib/)
  list(APPEND EPOLL_INCLUDE_DIRS "${install_dir}/include")
  list(APPEND EPOLL_LIBS
    ${_lib_rdx}
    #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}epoll${CMAKE_SHARED_LIBRARY_SUFFIX}"
    )
#  add_library(${_lib_rdx} SHARED IMPORTED)
  add_library(${_lib_rdx} STATIC IMPORTED)
#  set_target_properties(${_lib_rdx}     PROPERTIES

  set_property(TARGET ${_lib_rdx}
    PROPERTY
    IMPORTED_LOCATION
    "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
    #"${binary_dir}/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}-0${CMAKE_SHARED_LIBRARY_SUFFIX}"
    )
  message( "${_lib_rdx} in ${binary_dir}/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}")
  message( "${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}")
  add_dependencies(${_lib_rdx} ep_sys_epoll)


  
  set(_lib_rdx up)
  externalproject_get_property(ep_upoll install_dir binary_dir)
  list(APPEND EPOLL_INCLUDE_DIRS "${install_dir}/include")
  list(APPEND EPOLL_LIBS
    ${_lib_rdx}
    #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
    )
  add_library(${_lib_rdx} STATIC IMPORTED)
  #add_library(${_lib_rdx} SHARED IMPORTED)
  set_property(TARGET ${_lib_rdx}
    PROPERTY
    IMPORTED_LOCATION
    "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
    #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
    )
  message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
  add_dependencies(${_lib_rdx} ep_upoll)
  
else (NOT "${HAVE_SYS_EPOLL_H}" AND "${CYGWIN}")
  
find_path(EPOLL_INCLUDE_DIRS "sys/epoll.h" 
  PATHS /usr /usr/local ${PROJECT_BINARY_DIR} ${EP_SYS_EPOLL_SOURCE_DIR}
  PATH_SUFFIXES include
  )
endif (NOT "${HAVE_SYS_EPOLL_H}" AND "${CYGWIN}")

include_directories(
  ${EPOLL_INCLUDE_DIRS}
  ${PROJECT_BINARY_DIR}/include
  )



#
# setup Module for OpenAirInterface projects
# typicaly callable from other sub projects...
#

set(CMAKE_INCLUDE_CURRENT_DIR ON)

include(openairinterface-use)



#
#  find GIT version project is on.
#
set(GIT_BRANCH        "UNKNOWN")
set(GIT_COMMIT_HASH   "UNKNOWN")
set(GIT_COMMIT_DATE   "UNKNOWN")

find_package(Git)

if(GIT_FOUND)
  message("git found: ${GIT_EXECUTABLE}")
  # Get the current working branch
  execute_process(
    COMMAND git rev-parse --abbrev-ref HEAD
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_BRANCH
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )

  # Get the latest abbreviated commit hash of the working branch
  execute_process(
    COMMAND git log -1 --format=%h
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_COMMIT_HASH
    OUTPUT_STRIP_TRAILING_WHITESPACE
  )
  
  # Get the latest commit date of the working branch
  execute_process(
    COMMAND git log -1 --format=%cd
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    OUTPUT_VARIABLE GIT_COMMIT_DATE
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )

  #TODO:PTZ160811 handling compatible.. with project name!
  set(git_tag  ${GIT_BRANCH})
  set(GIT_REPOSITORY git@gitlab.eurecom.fr:ptizoom/openair5g.git)
  set(GIT_REMOTE_NAME ptizoom)
  set(GIT_SUBMODULES dependencies/asn1c)
  #GIT_REMOTE_NAME eurocom
  #GIT_REMOTE_NAME ptizoom
  #GIT_TAG "PTZ160803_x86_emu"
  #GIT_TAG "openair"
endif ()
#$(shell git describe --tag)

# Below is a hard-coded info
set (FIRMWARE_VERSION "No svn information")
add_definitions("-DFIRMWARE_VERSION=\"${FIRMWARE_VERSION}\"")
add_definitions("-DPACKAGE_VERSION=\"Branch: ${GIT_BRANCH} Abrev. Hash: ${GIT_COMMIT_HASH} Date: ${GIT_COMMIT_DATE}\"")
#
add_definitions("-DPACKAGE_BUGREPORT=\"openair4g-devel@lists.eurecom.fr\"")


#############################################
# Base directories, compatible with legacy OAI building
################################################
set (OPENAIR_DIR     ${PROJECT_SOURCE_DIR} CACHE PATH "root project directory")
set (OPENAIR1_DIR    ${OPENAIR_DIR}/openair1)
set (OPENAIR2_DIR    ${OPENAIR_DIR}/openair2)
set (OPENAIR3_DIR    ${OPENAIR_DIR}/openair3)
set (OPENAIR_TARGETS ${OPENAIR_DIR}/targets)
set (OPENAIR_CMAKE   ${OPENAIR_DIR}/cmake_targets)
set (OPENAIR_BIN_DIR ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY})

# The interesting stuff goes here
# ===============================
#set(CMAKE_FILES_DIRECTORY /CMakeFiles)
#set(OPENAIRINTERFACE_CMAKE_DIR "${INSTALL_DIR}${CMAKE_FILES_DIRECTORY}/openairinterface")
# Offer the user the choice of overriding the installation directories
set(OPENAIRINTERFACE_INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")
set(OPENAIRINTERFACE_INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables")
set(OPENAIRINTERFACE_INSTALL_INCLUDE_DIR share/oai
  CACHE PATH "Installation directory for header files"
  )
set(OPENAIRINTERFACE_INSTALL_CMAKE_DIR .
  CACHE PATH "Installation directory for CMake files"
  )

# Offer the user the choice of overriding the installation directories
set(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")
set(INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables")
set(INSTALL_INCLUDE_DIR share/oai
  CACHE PATH "Installation directory for header files"
  )


if(WIN32 AND NOT CYGWIN)
  set(DEF_INSTALL_CMAKE_DIR CMakeFiles)
else()
  set(DEF_INSTALL_CMAKE_DIR CMakeFiles/oai)
endif()
#set(DEF_INSTALL_CMAKE_DIR .) #for nothing

set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR}
  CACHE PATH  "Installation directory for CMake files"
  )

if(NOT IS_ABSOLUTE "${INSTALL_INCLUDE_DIR}")
  set(BINARY_INCLUDE_DIR ${PROJECT_BINARY_DIR}/${INSTALL_INCLUDE_DIR})
endif()

# Make relative paths absolute (needed later on)
foreach(p LIB BIN INCLUDE CMAKE)
  set(var OPENAIRINTERFACE_INSTALL_${p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
  endif()
endforeach()

# does not work with find_package... 
set(OPENAIRINTERFACE_BINARY_DIR ${PROJECT_BINARY_DIR} CACHE PATH "root build project directory")


#######################################################
#######################################################

add_list_string_option(PACKAGE_NAME "OpenAirInterface5G" "As per attribute name")


# Debug related options
#########################################
add_boolean_option(ASN_DEBUG           False "ASN1 coder/decoder Debug")
add_boolean_option(EMIT_ASN_DEBUG      False "ASN1 coder/decoder Debug")
add_boolean_option(MSG_PRINT           False "print debug messages")
add_boolean_option(DISABLE_XER_PRINT   False "print XER Format")
add_boolean_option(XER_PRINT           False "print XER Format")
add_boolean_option(RRC_MSG_PRINT       False "print RRC messages")
add_boolean_option(PDCP_MSG_PRINT      False "print PDCP messages to /tmp/pdcp.log")
add_boolean_option(DEBUG_PDCP_PAYLOAD  False "print PDCP PDU to stdout")  # if true, make sure that global and PDCP log levels are trace 
add_boolean_option(DEBUG_MAC_INTERFACE False "print MAC-RLC PDU exchange to stdout") # if true, make sure that global and PDCP log levels are trace 
add_boolean_option(TRACE_RLC_PAYLOAD   False "print RLC PDU to stdout") # if true, make sure that global and PDCP log levels are trace 
add_boolean_option(TEST_OMG            False "???")
add_boolean_option(DEBUG_OMG           False "???")
add_boolean_option(XFORMS              True  "This adds the possibility to see the signal oscilloscope")
add_boolean_option(PRINT_STATS         False "This adds the possibility to see the status")
add_boolean_option(T_TRACER            False "Activate the T tracer, a debugging/monitoring framework" )

add_boolean_option(DEBUG_CONSOLE False "makes debugging easier, disables stdout/stderr buffering")


####################################################
# compilation flags
#############################################
if (CMAKE_BUILD_TYPE STREQUAL "")
   set(CMAKE_BUILD_TYPE "RelWithDebInfo")
endif()

add_list_string_option(CMAKE_BUILD_TYPE "RelWithDebInfo" "Choose the type of build, options are: None(CMAKE_CXX_FLAGS or CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel." Debug Release RelWithDebInfo MinSizeRel)

Message("Architecture is ${CMAKE_SYSTEM_PROCESSOR}")
if (CMAKE_SYSTEM_PROCESSOR STREQUAL "armv7l")
  set(C_FLAGS_PROCESSOR "-gdwarf-2 -mfloat-abi=hard -mfpu=neon -lgcc -lrt")
else (CMAKE_SYSTEM_PROCESSOR STREQUAL "armv7l")
  if(EXISTS  "/proc/cpuinfo")
    file(STRINGS "/proc/cpuinfo" CPUINFO REGEX flags LIMIT_COUNT 1)
    if (CPUINFO MATCHES "avx2")
      set(C_FLAGS_PROCESSOR "${C_FLAGS_PROCESSOR} -mavx2")
    endif()
    if (CPUINFO MATCHES "sse4_1")
      set(C_FLAGS_PROCESSOR "${C_FLAGS_PROCESSOR} -msse4.1")
    endif()
    if (CPUINFO MATCHES "ssse3")
      set(C_FLAGS_PROCESSOR "${C_FLAGS_PROCESSOR} -mssse3")
    endif()
  else()
    Message(WARNING "/proc/cpuinfo does not exit. We will use manual CPU flags")
  endif()
endif()

set(C_FLAGS_PROCESSOR " ${C_FLAGS_PROCESSOR} ${CFLAGS_PROCESSOR_USER}")

Message("C_FLAGS_PROCESSOR is ${C_FLAGS_PROCESSOR}")

if (CMAKE_SYSTEM_PROCESSOR MATCHES "x86")
  if ( (NOT( C_FLAGS_PROCESSOR MATCHES "ssse3")) OR (NOT( C_FLAGS_PROCESSOR MATCHES "msse4.1")) )
    Message(WARNING "For x86 Architecture, you must have following flags: -mssse3 -msse4.1. The current detected flags are: ${C_FLAGS_PROCESSOR}. You can pass the flags manually in build script, for example: ./build_oai --cflags_processor \"-mssse3 -msse4.1 -mavx2\" ")
  endif()
endif()

##TODO: PTZ161018 let cmake do the job according to platform
#set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS_PROCESSOR} -Wall -Wstrict-prototypes -fno-strict-aliasing -rdynamic -funroll-loops -Wno-packed-bitfield-compat -fPIC"  #-std=gnu99 )
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS_PROCESSOR} -Wall -Wstrict-prototypes -fno-strict-aliasing -rdynamic -funroll-loops -Wno-packed-bitfield-compat")


#
find_package(BISON)
find_package(FLEX)

# find linux source for host and cross compiles
# look also for Kbuild, dkms, RTAI in it
include(LINUX)

# add autotools definitions that were maybe used!
##TODO: PTZ161016 we had rather to detect those...

include (CheckIncludeFiles)
check_include_files("sys/param.h;sys/mount.h" HAVE_SYS_MOUNT_H)
check_include_files(malloc.h HAVE_MALLOC_H)
check_include_files(strings.h HAVE_STRINGS_H)
check_include_files(string.h HAVE_STRING_H)

#STDC_HEADERS
include(CheckIncludeFile)
check_include_file(sys/eventfd.h HAVE_SYS_EVENTFD_H)
check_include_file(sys/types.h HAVE_SYS_TYPES_H)
check_include_file(sys/time.h HAVE_SYS_TIME_H)
check_include_file(sys/stat.h HAVE_SYS_STAT_H)
check_include_file(sys/socket.h HAVE_SYS_SOCKET_H)
check_include_file(inttypes.h HAVE_INTTYPES_H)
check_include_file(memory.h HAVE_MEMORY_H)
check_include_file(stdlib.h HAVE_STDLIB_H)
check_include_file(stdint.h HAVE_STDINT_H)
check_include_file(socket.h HAVE_SOCKET_H)
check_include_file(gmp.h HAVE_GMP_H)
check_include_file(unistd.h HAVE_UNISTD_H)
check_include_file(fcntl.h HAVE_FCNTL_H)

include(CheckFunctionExists)
check_function_exists(malloc HAVE_MALLOC)
check_function_exists(memset HAVE_MEMSET)
check_function_exists(gettimeofday HAVE_GETTIMEOFDAY)

#include(CheckSymbolExists)
#check_symbol_exists(LC_MESSAGES "locale.h" HAVE_LC_MESSAGES)

include(CheckLibraryExists)
#check_library_exists(volmgt volmgt_running "" HAVE_VOLMGT)

#PTZ170306 linux stuff is unlikely here on native CYGWIN
check_include_file(linux/netlink.h HAVE_LINUX_NETLINK_H)
check_include_file(linux/sched.h HAVE_LINUX_SCHED_H)
check_include_file(linux/if_packet.h HAVE_LINUX_IF_PACKET_H)

#allo allo netinet
check_include_file(netinet/ether.h HAVE_NETINET_ETHER_H)
#https://github.com/thom311/libnl.git
#https://github.com/blitz/tcptrace.git
#/cygwin-{includes,libs}
#git://sourceware.org/git/newlib-cygwin.git


# System packages that are required
# We use either the cmake buildin, in ubuntu are in: /usr/share/cmake*/Modules/
# or cmake provide a generic interface to pkg-config that widely used
###################################
include(FindPkgConfig)

pkg_search_module(LIBXML2 libxml-2.0 REQUIRED)
include_directories(${LIBXML2_INCLUDE_DIRS})

pkg_search_module(LIBXSLT libxslt REQUIRED)
include_directories(${LIBXSLT_INCLUDE_DIRS})

pkg_search_module(OPENSSL openssl REQUIRED)
include_directories(${OPENSSL_INCLUDE_DIRS})


###THREADS

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

#PTZ170216 looking for ether_header replacement, will probably not work
# together with linux/if_ether.h

  if (CYGWIN)
    set(_lib_rdx tcptrace)
    set(_lib_rdx_u LIBTCPTRACE)
    set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/tcptrace)
    
    ExternalProject_Add(ep_tcptrace
      PREFIX ${PROJECT_BINARY_DIR}
      GIT_REPOSITORY https://github.com/blitz/tcptrace.git
      SOURCE_DIR ${_src_d}
      UPDATE_DISCONNECTED 1
      #PTZ170220 not there yet
      CONFIGURE_COMMAND echo "${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared"
      BUILD_COMMAND echo make
      INSTALL_COMMAND echo make install
      #BUILD_BYPRODUCTS libtcptrace.la libtcptrace-1.0/libtcptrace.h ${CMAKE_SHARED_LIBRARY_PREFIX}tcptrace-1${CMAKE_SHARED_LIBRARY_SUFFIX}
      #STEP_TARGETS  install #update
      #STEP_TARGETS  download configure
      STEP_TARGETS  download
      )
    ExternalProject_Add_Step(ep_tcptrace autoupdate
      DEPENDEES download
      COMMENT "${_lib_rdx} customised :: autoupdate"
      COMMAND autoupdate --verbose
      WORKING_DIRECTORY ${_src_d}
      )
    ExternalProject_Add_Step(ep_tcptrace init
      DEPENDEES autoupdate
      COMMAND autoreconf --force --install --verbose --warnings=all --symlink
      COMMENT "${_lib_rdx} customised :: being autoreconfed"
      WORKING_DIRECTORY ${_src_d}
      BYPRODUCTS configure Makefile.in config.h.in
      )
      
    # #TODO:PTZ170210 installed in ${PROJECT_BINARY_DIR}/lib
    # externalproject_get_property(ep_tcptrace install_dir binary_dir)
    # list(APPEND LIBTCPTRACE_INCLUDEDIR "${install_dir}/include")
    # list(APPEND LIBTCPTRACE_LIBS
    #   ${_lib_rdx}
    #   #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
    #   )
    # add_library(${_lib_rdx} STATIC IMPORTED)
    # #add_library(${_lib_rdx} SHARED IMPORTED)
    # set_property(TARGET ${_lib_rdx}
    #   PROPERTY
    #   IMPORTED_LOCATION
    #   "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}"
    #   #"${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
    #   #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
    #   )
    # message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    # add_dependencies(${_lib_rdx} ep_tcptrace)
    # set(LIBTCPTRACE_LIBRARIES  ${_lib_rdx})
    # #TODO:PTZ170308 redo a search!
    add_custom_target(cygwin_extra_includes ALL
      DEPENDS ep_tcptrace
      #BYPRODUCTS ${_src_d}/cygwin-includes #/netinet/if_ether.h
      #[WORKING_DIRECTORY dir]
      COMMENT "here tcptrace is using a default netinet/if_ether.h under cygwin"
      SOURCES ${_src_d}/cygwin-includes
      )
    #add_dependencies(${_src_d}/${_lib_rdx} ep_tcptrace)
    set(CYGLINUX_INCLUDEDIR ${_src_d}/cygwin-includes)
    include_directories(${CYGLINUX_INCLUDEDIR})
  endif (CYGWIN)

#LIBSCTP
check_include_file(netinet/sctp.h  HAVE_NETINET_SCTP_H)
find_package(libsctp)
#check_library_exists(sctp "" "" HAVE_LIBSCTP)
#try sctp_socket = socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP);
#todo:PTZ170322 need "Findlibsctp.cmake" in CMAKE_MODULE_PATH

#if (HAVE_LIBSCTP-NOTFOUND) if (NOT HAVE_LIBCONFIG_H) #OR
if (libsctp_DIR MATCHES "NOTFOUND")
  pkg_search_module(SCTP libsctp)
  
endif (libsctp_DIR MATCHES "NOTFOUND")

if (libsctp_DIR MATCHES "NOTFOUND" AND NOT SCTP_FOUND)
  #if(DEBIAN_)
  #apt-get install libsctp-dev
  #else
  if (CYGWIN)
    #TODO:PTZ170206 http://www.sctp.de/sctp.html
    #http://www.sctp.de/download/sctplib-1.0.15.tar.gz
    #http://www.sctp.de/download/socketapi-2.2.8.tar.gz
    #TODO:PTZ170206 #include(SCTP)

    set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/sctplib)

    ExternalProject_Add(ep_sctplib
      PREFIX ${PROJECT_BINARY_DIR}
      #GIT_REPOSITORY https://github.com/richardhundt/sctplib.git
      #GIT_REPOSITORY git://koala/sctplib.git
      #DOWNLOAD http://www.sctp.de/download/sctplib-1.0.15.tar.gz
      URL http://www.sctp.de/download/sctplib-1.0.15.tar.gz
      SOURCE_DIR ${_src_d}
      UPDATE_DISCONNECTED 1
      CONFIGURE_COMMAND ${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared
      BUILD_COMMAND make
      INSTALL_COMMAND make install
      BUILD_BYPRODUCTS libsctp.la sctp.h ${CMAKE_SHARED_LIBRARY_PREFIX}sctp-0${CMAKE_SHARED_LIBRARY_SUFFIX}
      STEP_TARGETS  install
      )
    ExternalProject_Add_Step(ep_sctplib autoupdate
      DEPENDEES download
      COMMAND autoupdate --verbose
      WORKING_DIRECTORY ${_src_d}
      )
    ExternalProject_Add_Step(ep_sctplib init
      DEPENDEES autoupdate
      COMMAND autoreconf --force --install --verbose --warnings=all --symlink
      COMMENT "asn1c OAI customised :: being autoreconfed"
      WORKING_DIRECTORY ${_src_d}
      BYPRODUCTS configure Makefile.in config.h.in
      )
    
    #TODO:PTZ170210 installed in ${PROJECT_BINARY_DIR}/lib
    set(_lib_rdx sctp)
    externalproject_get_property(ep_sctplib install_dir binary_dir)
    list(APPEND SCTP_INCLUDEDIR "${install_dir}/include")
    list(APPEND SCTP_LIBS
      ${_lib_rdx}
      #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    add_library(${_lib_rdx} STATIC IMPORTED)
    #add_library(${_lib_rdx} SHARED IMPORTED)
    set_property(TARGET ${_lib_rdx}
      PROPERTY
      IMPORTED_LOCATION
      "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    add_dependencies(${_lib_rdx} ep_sctplib)
    set(SCTP_LIBRARIES  ${_lib_rdx})
    #set(SCTPLIB_LIBRARIES  /usr/src/openairinterface5g/bld/lib/lib${_lib_rdx}.a)
    

    
    #TODO:PTZ170206 set HAVE_LIBSCTP and relevant libraries


    
  else (CYGWIN)
    #TODO:PTZ170308 redo a search!

#PTZ170322 /usr/lib/x86_64-linux-gnu/pkgconfig/libsctp.pc
# is there!
#

    
    #add_definitions("-UENABLE_PGM_TRANSPORT")
    #set(SCTP_LIBRARIES  ${SCTP_LIBS})
  endif (CYGWIN)
  
endif ()
  #(HAVE_LIBSCTP-NOTFOUND)
#(CYGWIN AND NOT "${HAVE_LIBSCTP}")


#USB
#libusb homepage:
#http://libusb.info/

#needed by src/openairinterface5g/targets/ARCH/LMSSDR/USERSPACE/LIB/lms7002m/connectionManager/ConnectionManager.cpp:11:0:
# /usr/src/openairinterface5g/targets/ARCH/LMSSDR/USERSPACE/LIB/lms7002m/connectionManager
find_package(libusb)

if (libusb_DIR MATCHES "NOTFOUND")
  #if(DEBIAN_)
  #apt-get install libsctp-dev
  #else
  if (CYGWIN)
    #https://github.com/libusb/libusb.git
    
    set(_lib_rdx usb)
    set(_lib_rdx_u LIBUSB)
    set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/libusb)

    
    ExternalProject_Add(ep_usb
      PREFIX ${PROJECT_BINARY_DIR}
      GIT_REPOSITORY https://github.com/libusb/libusb.git
      #GIT_REPOSITORY git://koala/sctplib.git
      #DOWNLOAD http://www.sctp.de/download/sctplib-1.0.15.tar.gz
      SOURCE_DIR ${_src_d}
      UPDATE_DISCONNECTED 1
      CONFIGURE_COMMAND ${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared
      BUILD_COMMAND make
      INSTALL_COMMAND make install
      BUILD_BYPRODUCTS libusb.la libusb-1.0/libusb.h ${CMAKE_SHARED_LIBRARY_PREFIX}usb-1${CMAKE_SHARED_LIBRARY_SUFFIX}
      STEP_TARGETS  install
      )
    ExternalProject_Add_Step(ep_usb autoupdate
      DEPENDEES download
      COMMAND autoupdate --verbose
      WORKING_DIRECTORY ${_src_d}
      )
    ExternalProject_Add_Step(ep_usb init
      DEPENDEES autoupdate
      COMMAND autoreconf --force --install --verbose --warnings=all --symlink
      COMMENT "asn1c OAI customised :: being autoreconfed"
      WORKING_DIRECTORY ${_src_d}
      BYPRODUCTS configure Makefile.in config.h.in
      )
    
    #TODO:PTZ170210 installed in ${PROJECT_BINARY_DIR}/lib
    externalproject_get_property(ep_usb install_dir binary_dir)
    list(APPEND LIBUSB_INCLUDEDIR "${install_dir}/include")
    list(APPEND LIBUSB_LIBS
      ${_lib_rdx}
      #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    add_library(${_lib_rdx} STATIC IMPORTED)
    #add_library(${_lib_rdx} SHARED IMPORTED)
    set_property(TARGET ${_lib_rdx}
      PROPERTY
      IMPORTED_LOCATION
      "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    add_dependencies(${_lib_rdx} ep_usb)
    set(LIBUSB_LIBRARIES  ${_lib_rdx})
    #TODO:PTZ170308 redo a search!

  else  (CYGWIN)
    #add_definitions("-UENABLE_PGM_TRANSPORT")
    # lmsSDR will fail
    set(LIBUSB_LIBRARIES  )
  endif (CYGWIN)
  
endif (libusb_DIR MATCHES "NOTFOUND")
#(HAVE_LIBUSB-NOTFOUND)
#(CYGWIN AND NOT "${HAVE_LIBUSB}")

#PTZ170211 PNG needed in common/utils/tracer/gui
#PNG is mature and universal
find_package(libpng)

if (libpng_DIR MATCHES "NOTFOUND")
  message(WARNING "PACKAGE libpng is required by openairinterface5g/common/utils/T/tracer/gui:: trying to find it anyway")
  find_library(LIBPNG_LIBRARIES png png12
     PATHS / /usr /usr/local
     PATH_SUFFIXES lib
     )
  #set(LIBPNG_LIBS png)
endif (libpng_DIR MATCHES "NOTFOUND")

#####LIBNET NL netlink and so on...
#libnl-3-200
#http://www.infradead.org/~tgr/libnl/
#https://github.com/thom311/libnl.git
#https://github.com/tgraf/libnl.git

find_package(libnl)


if (libnl_DIR MATCHES "NOTFOUND")
  #if(DEBIAN_)
  #apt-get install libsctp-dev
  #else
  if (CYGWIN)
    #https://github.com/libnl/libnl.git
    
    set(_lib_rdx nl)
    set(_lib_rdx_u LIBNL)
    set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/libnl)
    
    ExternalProject_Add(ep_nl
      PREFIX ${PROJECT_BINARY_DIR}
      GIT_REPOSITORY https://github.com/tgraf/libnl.git
      SOURCE_DIR ${_src_d}
      UPDATE_DISCONNECTED 1
      CONFIGURE_COMMAND ${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared
      BUILD_COMMAND make
      INSTALL_COMMAND make install
      BUILD_BYPRODUCTS libnl.la libnl-3.0/libnl.h ${CMAKE_SHARED_LIBRARY_PREFIX}nl-3${CMAKE_SHARED_LIBRARY_SUFFIX}
      STEP_TARGETS  install
      )
    ExternalProject_Add_Step(ep_nl autoupdate
      DEPENDEES download
      COMMAND autoupdate --verbose
      WORKING_DIRECTORY ${_src_d}
      )
    ExternalProject_Add_Step(ep_nl init
      DEPENDEES autoupdate
      COMMAND autoreconf --force --install --verbose --warnings=all --symlink
      COMMENT "asn1c OAI customised :: being autoreconfed"
      WORKING_DIRECTORY ${_src_d}
      BYPRODUCTS configure Makefile.in config.h.in
      )
    
    #TODO:PTZ170210 installed in ${PROJECT_BINARY_DIR}/lib
    externalproject_get_property(ep_nl install_dir binary_dir)
    list(APPEND LIBNL_INCLUDEDIR "${install_dir}/include")
    list(APPEND LIBNL_LIBS
      ${_lib_rdx}
      #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    add_library(${_lib_rdx} STATIC IMPORTED)
    #add_library(${_lib_rdx} SHARED IMPORTED)
    set_property(TARGET ${_lib_rdx}
      PROPERTY
      IMPORTED_LOCATION
      "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    add_dependencies(${_lib_rdx} ep_nl)
    set(LIBNL_LIBRARIES  ${_lib_rdx})
    #TODO:PTZ170308 redo a search!

  else  (CYGWIN)
    #add_definitions("-UENABLE_PGM_TRANSPORT")
    # lmsSDR will fail
    set(LIBNL_LIBRARIES  )
  endif (CYGWIN)
  
endif (libnl_DIR MATCHES "NOTFOUND")



###############

add_definitions(-DSTDC_HEADERS=1 -DHAVE_ARPA_INET_H=1 -DHAVE_STRERROR=1)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/config.h.in ${CMAKE_CURRENT_BINARY_DIR}/config.h)
add_definitions(-DHAVE_CONFIG_H)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${C_FLAGS_PROCESSOR} -std=c++11")

#########################
set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} -Wl,-rpath -Wl,${CMAKE_CURRENT_BINARY_DIR}")
#########################
# set a flag for changes in the source code
# these changes are related to hardcoded path to include .h files
#-ggdb
add_definitions(-DCMAKER)
set(CMAKE_C_FLAGS_DEBUG "-g -DMALLOC_CHECK_=3 -O0")
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-g -DMALLOC_CHECK_=3 -O2")



#########################
# even more sub-project specific cmake scripts.
#####################################################################################
include(OpenAirInterface5g)

##TODO: PTZ161016 add_subdirectory(cmake_targets)! try not.

#
add_subdirectory(openair2)


#RPI and cross tools
set(with_rpi dependencies/rpi CACHE PATH "enable RaspberryPi cross compilation")
#TODO:PTZ170112 which linux.
if (with_rpi)
  add_subdirectory(${with_rpi})
endif(with_rpi)

###
# TODO:PTZ170119 ${OAI_RRC_LIBRARIES} ${OAI_S1AP_LIBRARIES} has to be in same directory
# and in this directory, shall have done a
#  install(TARGETS ... EXPORT openairinterface-targets ...)
#
set(_tgt_libs LFDS ${ITTI_LIB}
  L2 UTIL SIMU SECU_OSA GTPV1U SIMU_ETH
  SCHED_LIB HASHTABLE
  PHY
  )
install(TARGETS ${_tgt_libs}
  #RRC_LIB #asn1cskeletons
  EXPORT openairinterface-targets
  #NAMESPACE Upstream::
  #DESTINATION ${INSTALL_DIR}
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
  LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
  ARCHIVE DESTINATION "${INSTALL_LIB_DIR}" COMPONENT lib
  PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}" COMPONENT dev
  )

list(APPEND _tgt_libs ${OAI_RRC_LIBRARIES} ${OAI_S1AP_LIBRARIES})

# Add all targets to the build-tree export set
export(TARGETS ${_tgt_libs}
  FILE "${PROJECT_BINARY_DIR}/openairinterface-targets.cmake"
  )

# Export the package for use from the build-tree
# (this registers the build-tree with a global CMake-registry)
export(PACKAGE OpenAirInterface)
 
# Create the FooBarConfig.cmake and FooBarConfigVersion files
file(RELATIVE_PATH rel_include_dir
  "${INSTALL_CMAKE_DIR}"
  "${INSTALL_INCLUDE_DIR}"
  )
# ... for the build tree
#set(CONF_INCLUDE_DIRS "${PROJECT_SOURCE_DIR}" "${PROJECT_BINARY_DIR}")
configure_file(${OPENAIR_CMAKE}/openairinterface-config.cmake.in
  "${PROJECT_BINARY_DIR}/openairinterface-config.cmake" @ONLY)
# ... for the install tree
set(OPENAIRINTERFACE_CONF_INCLUDE_DIRS "\${OPENAIRINTERFACE_CMAKE_DIR}/${rel_include_dir}")
configure_file(${OPENAIR_CMAKE}/openairinterface-config.cmake.in
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/openairinterface-config.cmake" @ONLY)
# ... for both
configure_file(${OPENAIR_CMAKE}/openairinterface-config-version.cmake.in
  "${PROJECT_BINARY_DIR}/openairinterface-config-version.cmake" @ONLY)
 
# Install the FooBarConfig.cmake and FooBarConfigVersion.cmake
install(FILES
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/openairinterface-config.cmake"
  "${PROJECT_BINARY_DIR}/openairinterface-config-version.cmake"
  DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev
  )

# Install the export set for use with the install-tree
install(EXPORT openairinterface-targets
  DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev
  )

#
#  DOCUMENTATION
#
#
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/doc)



#    
#
#    TOOLS
#
#
#  itti_analyzer
add_subdirectory(common/utils)

#
# Packaging
#
#
list(APPEND _pkg_depends
  xmlstarlet
  unzip
  tshark
  sudo
    iperf;iproute;iptables;
  )

list(APPEND _pkg_build_depends
  
  autoconf;automake;
  bison;build-essential;cmake;
  #cmake-curses-gui

  doxygen;
  #doxygen-gui;
  texlive-latex-base;
  ethtool;
  flex;
  gccxml;
  gdb;
  git;
  graphviz;gtkwave;guile-2.0-dev;

  iptables-dev;
  libatlas-base-dev;libatlas-dev;
  libblas3gf;libblas-dev;libconfig8-dev;libforms-bin;
  libforms-dev;libgcrypt11-dev;libgmp-dev;libgtk-3-dev;
  libidn2-0-dev;libidn11-dev;libmysqlclient-dev;liboctave-dev;
  libpgm-5.1;libpgm-dev;libsctp1;libsctp-dev;libssl-dev;libtasn1-3-dev;
  libtool;libusb-1.0-0-dev;libxml2;libxml2-dev;mscgen;
  octave;octave-signal;
  openssh-client;openssh-server;openssl;python;subversion;xmlstarlet;
  python-pip;pydb;wvdial;python-numpy;sshpass;libxslt1-dev;android-tools-adb;
  nettle-dev;nettle-bin;dialog;dkms;gawk;gcc;g++;linux-headers;
  make;mysql-client;mysql-server-core-5.5;mysql-server;

  phpmyadmin;python-dev;sshfs;swig;
  valgrind;
  check;libblas;libpthread-stubs0-dev;openvpn;pkg-config;

  uml-utilities;
  vlan;
  
  libboost-all-dev;python-pexpect;
  python-netifaces;ctags;
  ntpdate;iperf3

  dkms
  libc-dev-armhf-cross
  crossbuild-essential-armhf
  linux-kernel-headers-armhf-cross
  linux-kernel-headers
  cross-config
  dpkg-cross
  install build-essential fakeroot
  build-dep linux devscripts
#This will install the packages required by the kernel build process.
#$ apt-get source linux
#
# librtai-dev #kver 2.6.28 not good!
  libsctp-dev
  #

  install build-essential git cmake pkg-config libboost-all-dev
  #mercurial 
#libfreetype6-dev libfreeimage-dev libzzip-dev libois-dev 
#libgl1-mesa-dev libglu1-mesa-dev libopenal-dev  
#libx11-dev libxt-dev libxaw7-dev libxrandr-dev 
#libssl-dev libcurl4-openssl-dev libgtk2.0-dev libwxgtk3.0-dev 
#nvidia-cg-toolkit


  
        )

# python packages...
#    $SUDO pip install paramiko
#    $SUDO pip install pyroute2

list(REMOVE_DUPLICATES _pkg_build_depends)

message(AUTHOR_WARNING "oai::_pkg_build_depends=${_pkg_build_depends}")

set(DEBIAN_PACKAGE_BUILDS_DEPENDS ${_pkg_build_depends})
set(CPACK_DEBIAN_PACKAGE_DEPENDS ${_pkg_depends})


##TODO: PTZ161002 other installs....
#other 
#
# check_install_usrp_uhd_driver(){
#         #first we remove old installation
#         $SUDO apt-get remove uhd libuhd-dev libuhd003 uhd-host -y
#         v=$(lsb_release -cs)
#         $SUDO apt-add-repository --remove "deb http://files.ettus.com/binaries/uhd/repo/uhd/ubuntu/$v $v main"
#         #The new USRP repository
#         $SUDO add-apt-repository ppa:ettusresearch/uhd -y
#         $SUDO apt-get update
#         $SUDO apt-get -y install  python python-tk libboost-all-dev libusb-1.0-0-dev
#         $SUDO apt-get -y install libuhd-dev libuhd003 uhd-host
#         $SUDO uhd_images_downloader 
# }
# check_install_bladerf_driver(){
# 	$SUDO add-apt-repository -y ppa:bladerf/bladerf
# 	$SUDO apt-get update
# 	$SUDO apt-get install -y bladerf libbladerf-dev
# 	$SUDO apt-get install -y bladerf-firmware-fx3
# 	$SUDO apt-get install -y bladerf-fpga-hostedx40	
# 	$SUDO bladeRF-cli --flash-firmware /usr/share/Nuand/bladeRF/bladeRF_fw.img	
# }


set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "ptizoom@gmail.com") #required
set(CPACK_DEBIAN_PACKAGE_DEPENDS "libc6 (>= 2.3.1-6), libgcc1 (>= 1:3.4.2-12)")

include(CPack)
 
# To use this:
# make package
# sudo dpkg -i DistanceBetweenPoints-0.1.1-Linux.deb
#
# This will result in the file:
#/usr/distance/DistanceBetweenPoints
#  IF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
#    SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Grass Root DiCoM")
#    SET(CPACK_PACKAGE_VENDOR "gdcm")
#    SET(CPACK_PACKAGE_VERSION_MAJOR "${GDCM_VERSION_MAJOR}")
#    SET(CPACK_PACKAGE_VERSION_MINOR "${GDCM_VERSION_MINOR}")
#    SET(CPACK_PACKAGE_VERSION_PATCH "${GDCM_VERSION_BUILD}")
#    SET(CPACK_PACKAGE_INSTALL_DIRECTORY "gdcm ${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}")
#    SET(CPACK_SOURCE_PACKAGE_FILE_NAME "gdcm-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
#    # List executables
#    INCLUDE(CPack)
#  ENDIF(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
#  INCLUDE(UseDebian)
#  IF(DEBIAN_FOUND)
# #  ADD_DEBIAN_TARGETS(GDCM)
#  ENDIF(DEBIAN_FOUND)




#
# testing
#
add_boolean_option(OAI_TEST            True  "project checking and testing enabler")

enable_testing()

add_subdirectory(t)
