#############################
# ASN.1 grammar C code generation & dependencies
################################
# A difficulty: asn1c generates C code of a un-predictable list of files
# so, generate the c from asn1c once at cmake run time
# So, if someone modify the asn.1 source file in such as way that it will create
# (so creating new asn.1 objects instead of modifying the object attributes)
# New C code source file, cmake must be re-run (instead of re-running make only)
#############

# SYNOPSIS
#
#  set OPENAIR_CMAKE
#
#  include(Asn1c)
#
#TODO:PTZ160808 needs it?
# preferably compiled with eurocom mods.
#nop do not do that here find_package(Asn1c)
#TODO: PTZ160822 has to be local project already own one.
set(asn1c_install_include_dir share/asn1c)
set(ASN1C_DIR ${OPENAIR_CMAKE} CACHE PATH "ASN1C tools directory")
set(ASN1C_SOURCE_DIR ${PROJECT_SOURCE_DIR}/dependencies/asn1c CACHE PATH "ASN1C native sources")
#TODO:PTZ160811 really deduct them; but not sure...could be 
#set(ASN1C_INSTALL_CMAKE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets")
set(ASN1C_INSTALL_CMAKE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets")
#
#done in asn1c-config include_directories(${ASN1C_INSTALL_DIR}/share/asn1c)
set(ASN1C_CMD ${PROJECT_BINARY_DIR}/bin/asn1c)

include(openairinterface-use)

#PTZ160822 buggers
#cd /usr/src/openairinterface5g/bld/openair2/RRC/LITE && /usr/bin/cc  -DASN_DEBUG -DCMAKER -DCMAKE_BUILD_TYPE=\"Debug\" -DDEBUG_CONSOLE -DDEBUG_MAC_INTERFACE -DDEBUG_OMG -DDEBUG_PDCP_PAYLOAD -DDISABLE_XER_PRINT -DEMIT_ASN_DEBUG -DENABLE_ITTI -DFIRMWARE_VERSION="\"No svn information\"" -DMSG_PRINT -DPACKAGE_VERSION="\"Branch: PTZ160803_x86_emu Abrev. Hash: 3bdacbb Date: Mon Aug 22 06:30:10 2016 +0100\"" -DPDCP_MSG_PRINT -DPRINT_STATS -DRRC_LIB_EXPORTS -DRRC_MSG_PRINT -DRel10=1 -DTEST_OMG -DTRACE_RLC_PAYLOAD -DT_TRACER -DXER_PRINT -DXFORMS -I/usr/src/openairinterface5g -I/usr/src/openairinterface5g/bld -I/usr/src/openairinterface5g/bld/share/asn1c -I/usr/src/openairinterface5g/openair2/RRC/LITE/MESSAGES -I/usr/src/openairinterface5g/bld/src/ep_oai_rrc_asn1c-build -I/usr/src/openairinterface5g/openair2/RRC/LITE/MESSAGES/asn1c/ASN1_files  -mssse3  -std=gnu99 -Wall -Wstrict-prototypes -fno-strict-aliasing -rdynamic -funroll-loops -Wno-packed-bitfield-compat -fPIC  -DSTDC_HEADERS=1 -DHAVE_SYS_TYPES_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_STDLIB_H=1 -DHAVE_STRING_H=1 -DHAVE_MEMORY_H=1 -DHAVE_STRINGS_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DHAVE_UNISTD_H=1 -DHAVE_FCNTL_H=1 -DHAVE_ARPA_INET_H=1 -DHAVE_SYS_TIME_H=1 -DHAVE_SYS_SOCKET_H=1 -DHAVE_STRERROR=1 -DHAVE_SOCKET=1 -DHAVE_MEMSET=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_STDLIB_H=1 -DHAVE_MALLOC=1 -DHAVE_LIBSCTP    -mssse3  -std=gnu99 -Wall -Wstrict-prototypes -fno-strict-aliasing -rdynamic -funroll-loops -Wno-packed-bitfield-compat -fPIC  -DSTDC_HEADERS=1 -DHAVE_SYS_TYPES_H=1 -DHAVE_SYS_STAT_H=1 -DHAVE_STDLIB_H=1 -DHAVE_STRING_H=1 -DHAVE_MEMORY_H=1 -DHAVE_STRINGS_H=1 -DHAVE_INTTYPES_H=1 -DHAVE_STDINT_H=1 -DHAVE_UNISTD_H=1 -DHAVE_FCNTL_H=1 -DHAVE_ARPA_INET_H=1 -DHAVE_SYS_TIME_H=1 -DHAVE_SYS_SOCKET_H=1 -DHAVE_STRERROR=1 -DHAVE_SOCKET=1 -DHAVE_MEMSET=1 -DHAVE_GETTIMEOFDAY=1 -DHAVE_STDLIB_H=1 -DHAVE_MALLOC=1 -DHAVE_LIBSCTP -g -DMALLOC_CHECK_=3 -fPIC   -o CMakeFiles/RRC_LIB.dir/__/__/__/share/oai/rrc/AC-BarringConfig.c.o   -c /usr/src/openairinterface5g/bld/share/oai/rrc/AC-BarringConfig.c
#/usr/src/openairinterface5g/bld/share/asn1c/asn_internal.h: In function ‘_ASN_STACK_OVERFLOW_CHECK’:
#<command-line>:0:11: error: called object is not a function or function pointer
#/usr/src/openairinterface5g/bld/share/asn1c/asn_internal.h:118:4: note: in expansion of macro ‘ASN_DEBUG’
#    ASN_DEBUG("Stack limit %ld reached",
add_boolean_option(ASN_DEBUG_          False "ASN1 coder/decoder Debug")
add_boolean_option(EMIT_ASN_DEBUG      True  "ASN1 coder/decoder Debug")

#TODO: PTZ160819 
set(_cmake_args
  -DCMAKE_INSTALL_PREFIX:PATH=${PROJECT_BINARY_DIR}
  -DINSTALL_DIR:PATH="${PROJECT_BINARY_DIR}"
  -DINSTALL_INCLUDE_DIR:PATH=${asn1c_install_include_dir}
  -DCMAKE_MODULE_PATH:PATH=${CMAKE_MODULE_PATH}
)
#
#lib/CMake/FooBar
#set(CMAKE_FILES_DIRECTORY /CMakeFiles)
#set(ASN1C_CMAKE_DIR "${ASN1C_INSTALL_DIR}${CMAKE_FILES_DIRECTORY}/asn1c")
#install(TARGETS foo
#  # IMPORTANT: Add the foo library to the "export-set"
#  EXPORT FooBarTargets
#
#TODO:PTZ160811
#OPENAIR_DIR needed
#${OPENAIR_BIN_DIR}
#
#ASN_DEBUG
#
#TODO:PTZ180811 if need for root project
#set(sub_src_d ${OPENAIR_DIR}/dependencies/asn1c)
ExternalProject_Add(ep_asn1c
  PREFIX ${PROJECT_BINARY_DIR}
  #PTZ160810 this fellow remove all of your .git  project... so back it up.
  #GIT_REPOSITORY git@gitlab.eurecom.fr:oai/asn1c.git
  GIT_REPOSITORY git@gitlab.eurecom.fr:ptizoom/asn1c.git
  #GIT_REPOSITORY ${OPENAIR_DIR}/.git/modules/asn1c
  #GIT_REMOTE_NAME eurocom
  #GIT_REMOTE_NAME ptizoom
  GIT_TAG "PTZ160803_x86_emu"
  #GIT_TAG "openair"
  #GIT_SUBMODULES ""
  SOURCE_DIR ${ASN1C_SOURCE_DIR}
  
  #UPDATE_COMMAND git submodule update -- ${CMAKE_CURRENT_SOURCE_DIR}
  #UPDATE_COMMAND git pull
  ##TODO:PTZ160825  update keeps hitting update
  #UPDATE_COMMAND autoupdate --verbose 
  #UPDATE_COMMAND && pwd
  #UPDATE_COMMAND && autoreconf --install --verbose --warnings=all --symlink ${ASN1C_SOURCE_DIR}
  #UPDATE_COMMAND && autoreconf --force --install --verbose --warnings=all --symlink .
  #TODO:PTZ160811 autoreconf seems to loose track of directory for now.
  #PTZ160925 triggers skip-update, but, has the ALWAYS flags,  then does not  stamps
  # , so -configure which depends on [skip]-update will always be remade... and so the rest...!
  UPDATE_DISCONNECTED 1
  
  #PTZ160925 CONFIGURE_COMMAND ${ASN1C_SOURCE_DIR}/configure --prefix=${CMAKE_CURRENT_BINARY_DIR} --enable-maintainer-mode --enable-shared
  CONFIGURE_COMMAND test -f config.status || ${ASN1C_SOURCE_DIR}/configure --prefix=${CMAKE_CURRENT_BINARY_DIR} --enable-maintainer-mode --enable-shared
  #--enable-make-include CFLAGS
  ##TODO:PTZ160811 --enable-ASN_DEBUG
  #  
  #BINARY_DIR ${OPENAIR_BIN_DIR}/asn1c
  BUILD_COMMAND make  
  #PTZ160809 might not need
  INSTALL_COMMAND make install
  #INSTALL_COMMAND #produce asn1c-config.cmake
  #INSTALL_DIR ${OPENAIR_CMAKE}/tools
  #PTZ160924 forget update it always trigger configure
  #STEP_TARGETS configure build install
  #TODO:PTZ160819 #asn1c project_asn1c_skeletons_sources
  BUILD_BYPRODUCTS bin/asn1c lib/libasn1cskeletons.a config.status
  #${ASN1C_DIR}/asn1c-config.cmake.in

  COMMENT "asn1c OAI customised -- "
  )
ExternalProject_Get_Property(ep_asn1c
  install_dir
  source_dir
  configure_command
#  CMAKE_INSTALL_PREFIX
  )
set(ASN1C_INSTALL_DIR ${install_dir})
set(PROJECT_ASN1C_SOURCE_DIR ${source_dir})
set(PROJECT_ASN1C_CONF_CMD ${configure_command})
set(ASN1C_INSTALL_INCLUDE_DIR ${ASN1C_INSTALL_DIR}/${asn1c_install_include_dir})
set(ASN1C_INCLUDE_DIR ${ASN1C_INSTALL_INCLUDE_DIR})

foreach(_p LIB BIN INCLUDE CMAKE)
  set(_var ASN1C_INSTALL_${_p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${_var} "${PROJECT_BINARY_DIR}/${${_var}}")
  endif()
endforeach(_p)


ExternalProject_Add_Step(ep_asn1c autoupdate
  DEPENDEES download
  COMMAND autoupdate --verbose
  WORKING_DIRECTORY ${PROJECT_ASN1C_SOURCE_DIR}
  )

#if(1)
# no need update step does that.
#$<SEMICOLON> is also \;
#might have done an autoupdate too
# this tqrget sall be hit once for all.
ExternalProject_Add_Step(ep_asn1c init
  DEPENDEES autoupdate
  #DEPENDERS configure
  #COMMAND cd ${project_asn1c_source_dir} $<SEMICOLON> test -f configure || \( autoupdate --verbose && autoreconf --force --install --verbose --warnings=all --symlink \)
  #COMMAND cd ${CMAKE_CURRENT_SOURCE_DIR} \; autoupdate --verbose && autoreconf --force --install --verbose --warnings=all --symlink
  #COMMAND cd ${project_asn1c_source_dir} \; autoupdate --verbose && autoreconf --force --install --verbose --warnings=all --symlink
  #COMMAND test -f configure || ( autoupdate --verbose && autoreconf --force --install --verbose --warnings=all --symlink )
  COMMAND autoreconf --force --install --verbose --warnings=all --symlink
  COMMENT "asn1c OAI customised :: being autoreconfed"
  WORKING_DIRECTORY ${PROJECT_ASN1C_SOURCE_DIR}
  BYPRODUCTS configure Makefile.in config.h.in
  )

# regenerate sources
file(GLOB asn1c_skeletons_sources
  LIST_DIRECTORIES false
  RELATIVE ${PROJECT_ASN1C_SOURCE_DIR}/skeletons
  ${PROJECT_ASN1C_SOURCE_DIR}/skeletons/*.[ch]
  )
##TODO:PTZ160823 need one at least...?
list(APPEND asn1c_skeletons_sources asn_internal.h)

list(REMOVE_DUPLICATES asn1c_skeletons_sources)

message(STATUS "found these asn1c_skeletons_sources in  ${PROJECT_ASN1C_SOURCE_DIR}/skeletons/ gives ${asn1c_skeletons_sources}")

file(GLOB ASN1C_SOURCES
  LIST_DIRECTORIES false
  RELATIVE ${ASN1C_INCLUDE_DIR}
  ${ASN1C_INCLUDE_DIR}/*.[ch]
  )
##TODO:PTZ160823 need one at least... here again?
list(APPEND ASN1C_SOURCES asn_internal.h)
list(REMOVE_DUPLICATES ASN1C_SOURCES)

foreach(_f IN LISTS ASN1C_SOURCES)
  list(APPEND ASN1C_SRCS ${ASN1C_INCLUDE_DIR}/${_f})
endforeach(_f)
list(REMOVE_DUPLICATES ASN1C_SRCS)

set(ASN1C_CONF_INCLUDE_DIRS 
  ${ASN1C_INCLUDE_DIR}
  )
list(APPEND
  CONF_INCLUDE_DIRS
  ${ASN1C_CONF_INCLUDE_DIRS}
  )
include_directories(${ASN1C_CONF_INCLUDE_DIRS})


#${ASN1C_INSTALL_DIR}/shared/asn1c/file-dependencies
#COMMON-FILES:			# THIS IS A SPECIAL SECTION
#message(STATUS "found these project_asn1c_skeletons_sources ${project_asn1c_source_dir} gives ${project_asn1c_skeletons_sources}")
#
# TODO:PTZ160811 put into asn1c-config.cmake
#
add_library(asn1cskeletons STATIC IMPORTED)
set_property(TARGET asn1cskeletons
  PROPERTY
  IMPORTED_LOCATION ${ASN1C_INSTALL_DIR}/lib/libasn1cskeletons.a
  )
#set_property(TARGET asn1cskeletons PROPERTY IMPORTED_LOCATION ${ASN1C_INSTALL_DIR}/lib/)
#set_property(TARGET asn1cskeletons PROPERTY LOCATION ${ASN1C_INSTALL_DIR}/lib/libasn1cskeletons.a)
#add_library(asn1cskeletons INTERFACE)
#target_include_directories(asn1cskeletons INTERFACE
#  #$<BUILD_INTERFACE:${ASN1C_INSTALL_DIR}/lib>
#  $<INSTALL_INTERFACE:${ASN1C_INCLUDE_DIR}>
#)
#set_target_properties(asn1cskeletons PROPERTIES
#PUBLIC_HEADER "${asn1c_skeletons_sources}"
#LOCATION ${ASN1C_INSTALL_DIR}/lib/libasn1cskeletons.a
#IMPORTED_LOCATION ${ASN1C_INSTALL_DIR}/lib/libasn1cskeletons.a
#  )
add_dependencies(asn1cskeletons ep_asn1c)
#
add_executable(asn1c IMPORTED)
set_property(TARGET asn1c
  PROPERTY
  LOCATION ${ASN1C_CMD}
  COMPONENT bin
  )
#add_dependencies(asn1c ep_asn1c asn1c-config.cmake.in)


if(0)
  #install(TARGETS asn1cskeletons EXPORT asn1c-export)
  #CMake Error at cmake_targets/Asn1c.cmake:204 (install):
  #  install TARGETS given target "asn1c" which does not exist in this
  #  directory.
  install(TARGETS asn1c asn1cskeletons 
  EXPORT asn1c-targets
  #NAMESPACE Upstream::
  #DESTINATION ${ASN1C_INSTALL_DIR}
  #lib/cmake/Eigen
  RUNTIME DESTINATION "${ASN1C_INSTALL_DIR}/bin" #COMPONENT bin
  LIBRARY DESTINATION "${ASN1C_INSTALL_DIR}/lib" #COMPONENT shlib
  PUBLIC_HEADER DESTINATION "${ASN1C_INSTALL_DIR}/share/asn1c" #COMPONENT dev
  )
endif()

message( STATUS current bin dir: ${CMAKE_CURRENT_BINARY_DIR})
message( STATUS current src dir: ${CMAKE_CURRENT_SOURCE_DIR})
# TODO:PTZ160811
#install(TARGETS asn1cskeletons-targets DESTINATION lib/prj/tgts EXPORT project_asn1c-targets)
#install(EXPORT project_asn1c-targets DESTINATION lib/prj)
#add_library(mylib STATIC mylib.c mylib.h)
#install(FILES mylib.h DESTINATION include/myproj)
#install(TARGETS mylib EXPORT mylib-targets DESTINATION lib/myproj)
#install(EXPORT asn1c-targets DESTINATION lib/prj)
#install(FILES asn1c-config.cmake DESTINATION lib/prj)
# Make relative paths absolute (needed later on)
#if(WIN32 AND NOT CYGWIN)
#  set(DEF_INSTALL_CMAKE_DIR CMake)
#else()
#  set(DEF_INSTALL_CMAKE_DIR lib/CMake/FooBar)
#endif()
#set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH
#  "Installation directory for CMake files")
message( STATUS install dir: ${ASN1C_INSTALL_DIR})
message( STATUS cmake install pref: ${CMAKE_INSTALL_PREFIX} -- ${CMAKE_FILES_DIRECTORY})
message( STATUS prj bin dir: ${PROJECT_BINARY_DIR})

if(0)
export(TARGETS #"" #ep_asn1c #asn1c asn1cskeletons 
  FILE "${PROJECT_BINARY_DIR}/asn1c-targets.cmake"
  )
##TODO: PTZ160925 redo configure_file() and 
##ExternalProject_Add_StepDependencies(ep_asn1c build ${PROJECT_BINARY_DIR}/asn1c-config.cmake)

endif()

export(PACKAGE ep_asn1c)

# Create the FooBarConfig.cmake and FooBarConfigVersion files
file(RELATIVE_PATH asn1c_rel_include_dir
  "${ASN1C_INSTALL_CMAKE_DIR}"
  "${ASN1C_INSTALL_INCLUDE_DIR}"
  )

message (STATUS "asn1c::CMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}")
message (STATUS "asn1c::CMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}")

# ... for the build tree
#set(ASN1C_CONF_INCLUDE_DIRS 
#  "${PROJECT_SOURCE_DIR}" "${PROJECT_BINARY_DIR}"
#  )
configure_file(${ASN1C_DIR}/asn1c-config.cmake.in
  "${PROJECT_BINARY_DIR}/asn1c-config.cmake" @ONLY)

# ... for the install tree
set(ASN1C_CONF_INCLUDE_DIRS "\${ASN1C_CMAKE_DIR}/${asn1c_rel_include_dir}")
configure_file(${ASN1C_DIR}/asn1c-config.cmake.in
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/asn1c-config.cmake" @ONLY)

# ... for both
configure_file(${ASN1C_DIR}/asn1c-config-version.cmake.in
  "${PROJECT_BINARY_DIR}/asn1c-config-version.cmake" @ONLY)

# Install the FooBarConfig.cmake and FooBarConfigVersion.cmake
install(FILES
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/asn1c-config.cmake"
  "${PROJECT_BINARY_DIR}/asn1c-config-version.cmake"
  DESTINATION "${ASN1C_INSTALL_CMAKE_DIR}" COMPONENT dev
  )

if(asn1c-targets)
# Install the export set for use with the install-tree
install(EXPORT asn1c-targets
  DESTINATION "${ASN1C_INSTALL_CMAKE_DIR}" COMPONENT dev
  )
endif()

