#
# check for RaspberryPi - raspdebian
if (0)
  #which linux#./debian/bin/genorig.py ../linux-2.6.35.tar.bz2#
  #rpi-raspdebian#git clone --depth=1 https://github.com/raspberrypi/linux
  ExternalProject_Add(ep_linux_source_rpi
    PREFIX ${PROJECT_BINARY_DIR}
    GIT_REPOSITORY  https://github.com/raspberrypi/linux
    SOURCE_DIR  ${PROJECT_SOURCE_DIR}/dependencies/rpi
    UPDATE_DISCONNECTED 1
    COMMENT "raspberrypi -- linux -- for OAI customised -- "
    )
endif (0)
