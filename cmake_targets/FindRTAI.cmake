##TODO: PTZ161020 RTAI... will likely not work with LINUX>-3.18.22, unlike RTlinux

#add_boolean_option(RTAI False "Use RTAI")

set(_prj_n rtai)
string(TOUPPER ${_prj_n} _prj_nu)

set(${_prj_nu}_SOURCE_DIR ${PROJECT_SOURCE_DIR}/dependencies/${_prj_n})

find_package(${_prj_n})
if(NOT ${_prj_nu}_FOUND)
  # find existing lib${_prj_n}-dev
endif()

#TODO:PTZ170112 needs a /usr/src/linux howevere would not want to build
# this old version ....really 
#
if (${with_linux_version} VERSION_LESS_EQUAL "3.5")

ExternalProject_Add(ep_${_prj_n}
  PREFIX ${PROJECT_BINARY_DIR}
  DEPENDS src_linux
  #ep_linux_source src_linux
  GIT_REPOSITORY https://github.com/ShabbyX/RTAI.git
  SOURCE_DIR  ${${_prj_nu}_SOURCE_DIR}
  UPDATE_DISCONNECTED 1
  CONFIGURE_COMMAND test -f config.status || ${${_prj_nu}_SOURCE_DIR}/configure --prefix=${PROJECT_BINARY_DIR} --enable-maintainer-mode --enable-shared
  BUILD_COMMAND make
  INSTALL_COMMAND make install
  BUILD_BYPRODUCTS config.status
  COMMENT "${_prj_nu} - customised -- "
  )
ExternalProject_Add_Step(ep_${_prj_n} autoupdate
  DEPENDEES download
  COMMAND autoupdate --verbose
  WORKING_DIRECTORY ${${_prj_nu}_SOURCE_DIR}
  )
ExternalProject_Add_Step(ep_${_prj_n} init
  DEPENDEES autoupdate
  COMMAND autoreconf --force --install --verbose --warnings=all --symlink
  COMMENT "${_prj_nu}  customised :: being autoreconfed"
  WORKING_DIRECTORY ${${_prj_nu}_SOURCE_DIR}
  BYPRODUCTS configure Makefile.in config.h.in
  )

endif (${with_linux_version} VERSION_LESSER "3.5")
