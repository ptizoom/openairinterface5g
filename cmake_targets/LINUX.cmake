#
#
# SYNOPSIS
#
#  include(LINUX)
#
#
#PTZ170106 also seen in /openair2/NETWORK_DRIVER/
#project(host_n_cross_LINUX C CC)
#
#
# the kernel target shall not have to be built by this project
# 
# -it would work better with this linux  kernel.
# -Real Time configuration shall be used. RTAI seems not being supported 
# -only compile if cross compiling..., even so, we shall not bother.
# and this shall be done by say, openembedded/poky projects
#
#
# target must be what ever cross we want...!
#
#TODO:PTZ161216 in fact would need linux source...!
# --with-linux-dir=/usr/src/linux by default
#  --with-module-dir=${exec_prefix}/modules   /usr/realtime
#  --with-kconfig-file=
# package ... linux-source-4.8 with linux-patch-4.8-rt.patch
#bla bla bla ... very much complex
# DO NOT USE RTAI! but https://rt.wiki.kernel.org
# also use ie. amd64_rt_amd64... on debian
#CONFIG_PREEMPT=y
#CONFIG_PREEMPT_RT_BASE=y
#CONFIG_HAVE_PREEMPT_LAZY=y
#CONFIG_PREEMPT_LAZY=y
#CONFIG_PREEMPT_RT_FULL=y


# kernel architecture of host - native
set(_kharch amd64)
# kernel architecture of target
set(_ktarch amd64)

# other embeded native kernels etc... (ie, GPUs)
list(APPEND _knarch_fw )

function(_kbuild_arch_replacer
    _arch
    )
  # Pairs of pattern-replace for postprocess architecture string from 'uname -m'.
  # Taken from ./Makefile of the kernel build.
  set(_kbuild_arch_replacers
    "i.86" "x86"
    "x86_64" "amd64"
    "sun4u" "sparc64"
    "arm.*" "arm"
    "sa110" "arm"
    "s390x" "s390"
    "parisc64" "parisc"
    "ppc.*" "powerpc"
    "mips.*" "mips"
    "sh[234].*" "sh"
    "aarch64.*" "arm64"
    )
  set(_current_pattern)
  foreach(_p ${_kbuild_arch_replacers})
    if(_current_pattern)
      string(REGEX REPLACE "${_current_pattern}" "${_p}" ARCH_DEFAULT "${_arch}")
      set(_current_pattern)
    else(_current_pattern)
      set(_current_pattern "${_p}" PARENT_SCOPE)
    endif(_current_pattern)
  endforeach(p ${_kbuild_arch_replacers})
  #  return(_current_pattern) ??  
endfunction(_kbuild_arch_replacer
  _arch
  )


#
#
#
#  Kbuild_VERSION_STRING - kernel version. is CMAKE_SYSTEM_VERSION
#  Kbuild_ARCH - architecture.
#  Kbuild_BUILD_DIR - directory for build kernel and/or its components.
#  Kbuild_VERSION_{MAJOR|MINOR|TWEAK} - kernel version components
#  Kbuild_VERSION_STRING_CLASSIC - classic representation of version,
find_package(Kbuild)


#TODO:PTZ170106 needs source code for RTAI check FindRTAI.cmake
# hecking dependency style of gcc... gcc3
# checking for RTAI Kconfig file... /usr/src/openairinterface5g/dependencies/rtai/base/arch/x86/defconfig (default)
# checking for module installation directory... ${exec_prefix}/modules
# checking for Linux source tree... 
# configure: error: No Linux kernel tree in /usr/src/linux
# CMakeFiles/ep_rtai.dir/build.make:102 : la recette pour la cible « src/ep_rtai-stamp/ep_rtai-configure » a échouée
# make[3]: *** [src/ep_rtai-stamp/ep_rtai-configure] Erreur 1
# make[3] : on quitte le répertoire « /usr/src/openairinterface5g/bld »
# CMakeFiles/Makefile2:1997 : la recette pour la cible « CMakeFiles/ep_rtai.dir/all » a échouée
# make[2]: *** [CMakeFiles/ep_rtai.dir/all] Erreur 2
# make[2] : on quitte le répertoire « /usr/src/openairinterface5g/bld »
#add_custom_target(  )
#
# target must be what ever cross we want...!
#
#TODO:PTZ161216 in fact would need linux source...!
# --with-linux-dir=/usr/src/linux by default
#  --with-module-dir=${exec_prefix}/modules   /usr/realtime
#  --with-kconfig-file=
# package ... linux-source-4.8 with linux-patch-4.8-rt.patch
#bla bla bla ... very much complex
# DO NOT USE RTAI! but https://rt.wiki.kernel.org
# also use ie. amd64_rt_amd64... on debian
#CONFIG_PREEMPT=y
#CONFIG_PREEMPT_RT_BASE=y
#CONFIG_HAVE_PREEMPT_LAZY=y
#CONFIG_PREEMPT_LAZY=y
#CONFIG_PREEMPT_RT_FULL=y
# # find it!
# # apt-get install linux-source-4.3
# $ tar xaf /usr/src/linux-source-4.3.tar.xz
# The unpacked source tree then will be available in linux-source-4.3 directory.
# 4.2. Rebuilding official Debian kernel packages
# You can build all or selected kernel packages by following these instructions. You may be asked to do this in order to test a potential bug fix.
# 4.2.1. Preparation
# Run the following commands:
# # apt-get install build-essential fakeroot
# # apt-get build-dep linux
# This will install the packages required by the kernel build process.
# $ apt-get source linux
# This will download and unpack the linux source package, making the tree available in the linux-version directory. As always, the revision part of the version of this package (for example, 1 in 3.2.19-1) will determine its patchlevel with respect to the original upstream kernel source.
# $ cd linux-version
# Enter the source directory.
# 4.2.1.1. Disk space requirements
# Building binary packages for a single kernel flavour with debug info enabled (currently true for the 686-pae, amd64, rt-686-pae, rt-amd64 and s390x configurations) requires up to 10 GB space in the package directory and 300 MB in /tmp (or $TMPDIR).
# Building with debug info disabled requires about 2 GB and 25 MB respectively. You can disable debug info by changing the value of debug-info to false in debian/config/arch/defines.
# Building all binary packages for i386 or amd64 currently requires about 20 GB space in the package directory. Other architectures with no debug info or fewer drivers will require less space.
# 4.2.2. Simple patching and building
# The source package includes a script to simplify the process of building with extra patches. You can use this by running commands such as:
# # apt-get install devscripts
# $ bash debian/bin/test-patches ../fix-bug123456.patch ../add-foo-driver.patch
# This script has options to control the flavour, featureset, etc. For a summary of the options, run:
# $ bash debian/bin/test-patches
# You may then need to build the linux-base package as well:
# $ fakeroot make -f debian/rules.real install-linux-base
# However, if you need to change the configuration or make other changes, you should not use this script and should follow the instructions below.
# 4.2.3. Applying patches or configuration changes
# It is possible to apply extra patches to the source before starting the build. In the linux source package, the default (non-featureset) patches are automatically applied in the top level directory.
# The patched source appears in the following directories.
# default source:
# top level
# source with featureset:
# debian/build/source_featureset
# You should apply the extra patches in the appropriate directory. In the linux source package you can use the quilt utility to do this.
# To change the configuration before building, for example for the 686-pae flavour on i386, run the commands:
# $ fakeroot make -f debian/rules.gen setup_i386_none_686-pae
# $ make -C debian/build/build_i386_none_686-pae nconfig
# If the patches or configuration changes alter type definitions for the kernel, you may need to change the ABI name; see Section 5.2.1, �The ABI name�.
# 4.2.4. Building many packages
# To build all possible packages for this architecture, run:
# $ fakeroot debian/rules binary
# To build all architecture-dependent packages, run:
# $ fakeroot debian/rules binary-arch
# To build all architecture-independent packages, run:
# $ fakeroot debian/rules binary-indep
# 4.2.5. Building packages for one flavour
# For example, to build only the binary packages for 686-pae flavour on i386 architecture, use the following commands:
# $ fakeroot debian/rules source
# $ fakeroot make -f debian/rules.gen binary-arch_i386_none_686-pae
# The target in this command has the general form of target_arch_featureset_flavour. Replace the featureset with none if you do not want any of the extra featuresets. This command will build the linux image and kernel headers packages. You may also need the linux-headers-version-common binary package, which can be built using the commands:
# $ fakeroot debian/rules source
# $ fakeroot make -f debian/rules.gen binary-arch_i386_none_real
# The target in this command has the general form of target_arch_featureset_real
# 4.3. Building a development version of the Debian kernel package
# To build a kernel image based on the kernel team's unreleased development version:
# # apt-get install build-essential fakeroot rsync git
# # apt-get build-dep linux
# The last two commands will install the build dependencies required by the kernel build process.
# $ git clone -b dist --single-branch https://anonscm.debian.org/git/kernel/linux.git
# This will check out the Debian packaging. dist is normally the distribution codename such as wheezy or sid (unstable). For the very latest version, usually based on an upstream release candidate, use master. Note that this will download several hundred megabytes of data.
# $ apt-get source -d linux
# This will download the linux upstream source (and the last released Debian patches). Depending on which version you are trying to build, you might need to override APT's version selection or download a tarball from people.debian.org instead.
# $ cd linux
# $ debian/rules orig
# This unpacks the upstream source and merges it with the Debian packaging.
# $ debian/rules debian/control
# This generates a Debian package control file based on the current definitions of the various kernel flavours which can be built.
# $ fakeroot debian/rules target
# Finally, build binary packages as explained in Section 4.2, �Rebuilding official Debian kernel packages�.
# 4.4. Generating orig tarball from newer upstream
# First you must add a changelog entry for the new upstream version. If the new version is a release candidate, change the string -rc to ~rc. (In Debian package versions, a suffix beginning with ~ indicates a pre-release.)
# The 'orig' tarball is generated by the genorig.py script. It takes either a tarball and optional patch from kernel.org, or a git repository. First ensure you have the necessary tools installed:
# # apt-get install unifdef
# Then if you have a tarball, run a command such as:
# $ debian/bin/genorig.py ../linux-4.3.tar.xz ../patch-4.4-rc1.xz
# Or if you have a git repository, pass the name of its directory:
# $ debian/bin/genorig.py ~/src/linux
# Either of these will generate a file such as ../orig/linux_3.5~rc1.orig.tar.xz. You can then combine this tarball with the Debian packaging by running:
# $ debian/rules orig
# 4.5. Building a custom kernel from Debian kernel source
# This section describes the simplest possible procedure to build a custom kernel the "Debian way". It is assumed that user is somewhat familiar with kernel configuration and build process. If that's not the case, it is recommended to consult the kernel documentation and many excellent online resources dedicated to it.
# The easiest way to build a custom kernel (the kernel with the configuration different from the one used in the official packages) from the Debian kernel source is to use the linux-source package and the make deb-pkg target. First, prepare the kernel tree:
# # apt-get install linux-source-4.3
# $ tar xaf /usr/src/linux-source-4.3.tar.xz
# $ cd linux-source-4.3
# The kernel now needs to be configured, that is you have to set the kernel options and select the drivers which are going to be included, either as built-in, or as external modules. The kernel build infrastructure offers a number of targets, which invoke different configuration frontends. For example, one can use console-based menu configuration by invoking the command
# $ make nconfig
# Instead of nconfig one can use config (text-based line-by-line configuration frontend) or xconfig (graphical configuration frontend). It is also possible to reuse your old configuration file by placing it as a .config file in the top-level directory and running one of the configuration targets (if you want to adjust something) or make oldconfig (to keep the same configuration). Note that different frontends may require different additional libraries and utilities to be installed to function properly. For example, the nconfig frontend requires the ncurses library, which at time of writing is provided by the libncurses5-dev package.
# The build will use less disk space if the CONFIG_DEBUG_INFO option is disabled (see Section 4.2.1.1, �Disk space requirements�). Debuginfo is only needed if you plan to use binary object tools like crash, kgdb, and SystemTap on the kernel.
# $ scripts/config --disable DEBUG_INFO
# After the configuration process is finished, the new or updated kernel configuration will be stored in .config file in the top-level directory. The build is started using the commands
# $ make clean
# $ make deb-pkg
# As a result of the build, a custom kernel package linux-image-3.2.19_3.2.19-1_i386.deb (name will reflect the version of the kernel and build number) will be created in the directory one level above the top of the tree. It may be installed using dpkg just as any other package:
# # dpkg -i ../linux-image-3.2.19_3.2.19-1_i386.deb
# This command will unpack the kernel, generate the initrd if necessary (see Chapter 7, Managing the initial ramfs (initramfs) archive for details), and configure the bootloader to make the newly installed kernel the default one. If this command completed without any problems, you can reboot using the
# # shutdown -r now
# command to boot the new kernel.
# For much more information about bootloaders and their configuration please check their documentation, which can be accessed using the commands man lilo, man lilo.conf, man grub, and so on. You can also look for documentation in the /usr/share/doc/package directories, with package being the name of the package involved.
# 4.6. Building a custom kernel from the "pristine" kernel source
# Building a kernel from the "pristine" (also sometimes called "vanilla") kernel source, distributed from www.kernel.org and its mirrors, may be occasionally useful for debugging or in the situations when a newer kernel version is desired. The procedure differs only in obtaining the kernel source: instead of unpacking the kernel source from Debian packages, the "pristine" source is downloaded using your favourite browser or using wget, as follows:
# $ wget https://kernel.org/pub/linux/kernel/v4.x/linux-4.3.tar.xz
# The integrity of the downloaded archive may be verified by fetching the corresponding cryptographic signature
# $ wget https://kernel.org/pub/linux/kernel/v4.x/linux-4.3.tar.sign
# and running this command (gnupg package must be installed):
# $ unxz -c linux-4.3.tar.xz | gpg --verify linux-4.3.tar.sign -
# Successful verification results in output similar to the one below:
# gpg: Signature made Mon 21 May 2012 01:48:14 AM CEST using RSA key ID 00411886
# gpg: Good signature from "Linus Torvalds <torvalds@linux-foundation.org>"
# gpg: WARNING: This key is not certified with a trusted signature!
# gpg:          There is no indication that the signature belongs to the owner.
# Primary key fingerprint: ABAF 11C6 5A29 70B1 30AB  E3C4 79BE 3E43 0041 1886
# After that the archive may be unpacked using
# $ tar xaf linux-4.3.tar.xz
# $ cd linux-4.3
# The unpacked kernel tree (in linux-4.3 now has to be configured. The existing configuration file may be used as a starting point
# $ cp /boot/config-3.2.0-2-686-pae ./.config
# After the configuration with one of the configuration frontends (invoked by make oldconfig, make config, make nconfig, etc) is completed, the build may be started using make deb-pkg target as described above.
# 4.7. Building out-of-tree kernel modules
# Some kernel modules are not included in the upstream or Debian kernel source, but are provided as third-party source packages. For some of the most popular out-of-tree modules, the binary Debian packages with modules built against the stock Debian kernels are provided. For example, if you are running stock Debian kernel 3.2.0-2-686-pae (use the uname -r command to verify the version) from the linux-image-3.2.0-2-686-pae package, and would like to use the squash filesystem, all you need to do is install squashfs-modules-3.2.0-2-686-pae binary package, which provides the neccessary binary kernel modules.
# If you are not so lucky, and there are no binary module packages in the archive, there is a fair chance that the Debian archive contains the packaged source for the kernel modules. Names of such packages typically end in -source, for example squashfs-source, thinkpad-source, rt2x00-source and many others. These packages contain debianized source code of the kernel modules, suitable for building using the module-assistant (or m-a) script from the module-assistant package. Typical sequence to build a custom binary module package, matching a kernel 3.2.0-2-686-pae (as returned by uname -r) from the debianized source consists of the following steps:
# Install a set of kernel headers, matching the kernel for which the modules are going to be built:
# # apt-get install linux-headers-3.2.0-2-686-pae
# Install the package containing the source:
# # apt-get install squashfs-source

# LINUX_VERSION_CODE 

set(with_linux_version ${CMAKE_SYSTEM_VERSION} CACHE STRING "linux target kernel major.minor version [d.dd]")
if(NOT "${with_linux_version}" STREQUAL "${CMAKE_SYSTEM_VERSION}")
  #we are compiling for a different kernel version
  #we are cross compiling indeed
  #
endif()
#;can be a list separated by semi-coma
#TODO:PTZ170112 which (embeded) linux source...
# .could be native 
# .could compete with rpi.
# .find also...on debian
#   linux-source-4.8.tar.xz
#   linux-patch-4.8-rt.patch.xz
string(REGEX MATCH "([0-9]+\\.[0-9]+)(\\.[0-9]+)([.-][a-z0-9]+)"
    _linux_ver
    "${CMAKE_SYSTEM_VERSION}"
)
set(_linux_ver_ma_mi ${CMAKE_MATCH_1})
set(_linux_ver_tw    ${CMAKE_MATCH_2})

#set(LINUX_SOURCE_DIR /usr/src/linux CACHE PATH "linux host sources")
set(LINUX_SOURCE_DIR ${PROJECT_SOURCE_DIR}/dependencies/linux-${_linux_ver_ma_mi} CACHE PATH "linux host sources")

#
# only valid with debian
#

find_file(_linux_source_tar_p
  NAMES linux-source-${_linux_ver_ma_mi}.tar.xz
  #linux-source-${_linux_ver_ma_mi}.tar.gz
  PATHS /usr /usr/local /opt
  PATH_SUFFIXES src
  NO_DEFAULT_PATH
  )
find_file(_linux_patch_rt_p
  NAMES linux-patch-${_linux_ver_ma_mi}-rt.patch.xz
  PATHS /usr /usr/local /opt
  PATH_SUFFIXES src
  NO_DEFAULT_PATH
  )
#if(NOT "${_linux_patch_rt_p}")
set(_linux_patch_rt_p /usr/src/linux-patch-${_linux_ver_ma_mi}-rt.patch)


#config.amd64_rt_amd64.xz
set(_linux_config_rt_p /usr/src/linux-config-${_linux_ver_ma_mi}/config.${_kharch}_rt_${_knarch})

#endif()
#untar linux-image-rt-amd64 linux-headers-4.8.0-2-rt-amd64
#list(append _pkg_build_depends build-essential kernel-package devscripts)
list(APPEND _pkg_deb
  linux-headers-${_linux_ver_ma_mi}-rt-amd64
  linux-source-${_linux_ver_ma_mi}
  build-essential fakeroot
  kernel-package devscripts 
  dkms
  unifdef

  libncurses5-dev
  )
#linux-kbuild-4.0
#linux-image-rt-amd64
#apt-get build-dep linux
#add_custom_command()

add_custom_target(linux_source_untar
  COMMAND tar xaf ${_linux_source_tar_p} -C ${PROJECT_SOURCE_DIR}/dependencies
  BYPRODUCTS ${PROJECT_SOURCE_DIR}/dependencies/linux-${_linux_ver_ma_mi}
  )

add_custom_target(linux_source_patch_xz
  COMMAND test -f ${_linux_patch_rt_p}.xz && /usr/bin/unxz --keep ${_linux_patch_rt_p}.xz || echo
  COMMENT "linux_source :: unxz patch"
  #WORKING_DIRECTORY ${${_prj_nu}_SOURCE_DIR}
  WORKING_DIRECTORY ${LINUX_SOURCE_DIR}
  BYPRODUCTS ${_linux_patch_rt_p}
  VERBATIM
  )

add_custom_target(linux_source_config_xz
  COMMAND test -f ${_linux_config_rt_p}.xz && /usr/bin/unxz --keep ${_linux_config_rt_p}.xz || echo
  COMMENT "linux_source :: unxz config"
  #WORKING_DIRECTORY ${${_prj_nu}_SOURCE_DIR}
  WORKING_DIRECTORY ${LINUX_SOURCE_DIR}
  BYPRODUCTS ${_linux_config_rt_p}
  VERBATIM
  )

 #/usr/src/linux
add_custom_target(src_linux
  #DEPENDS ep_linux_source
  DEPENDS linux_source_untar
  COMMAND ln -vs ${PROJECT_SOURCE_DIR}/dependencies/linux-${_linux_ver_ma_mi} linux
  WORKING_DIRECTORY /usr/src
  BYPRODUCTS /usr/src/linux
  COMMENTS "/usr/src/linux is needed for RTAI. remove it really."
  )

#add_custom_target(
#DEPENDS ${LINUX_SOURCE_DIR} 
#CMAKE_GENERATOR_PLATFORM <platform>
#    Generator-specific platform name
#CMAKE_GENERATOR_TOOLSET <toolset>
#    Generator-specific toolset name

#$ wget https://kernel.org/pub/linux/kernel/v4.x/linux-4.3.tar.xz
#The integrity of the downloaded archive may be verified by fetching the corresponding cryptographic signature
#$ wget https://kernel.org/pub/linux/kernel/v4.x/linux-4.3.tar.sign
#and running this command (gnupg package must be installed):
#$ unxz -c linux-4.3.tar.xz | gpg --verify linux-4.3.tar.sign -

set(_kernel_name oai)
#related to git - tag?
set(_kdeb_pkgver 1)
set(_prj_nl linux_source)

if (_TODOPTZ170127_gona_use_openembedded_poky_instead)
  externalproject_add(ep_linux_source #_deb
    PREFIX ${PROJECT_BINARY_DIR}
    #URL https://kernel.org/pub/linux/kernel/v4.x/linux-${_linux_version_ma_mi}.tar.xz
    URL ${_linux_source_tar_p}
    #DOWNLOAD_COMMAND sudo aptitude install ${_pkg_deb}
    SOURCE_DIR ${LINUX_SOURCE_DIR}
    #PATCH_COMMAND patch --ignore-whitespace --verbose --directory=${LINUX_SOURCE_DIR} --input=${_linux_patch_rt_p}
    UPDATE_COMMAND tar xaf ${_linux_source_tar_p} -C ${PROJECT_SOURCE_DIR}/dependencies
    #--directory=${LINUX_SOURCE_DIR}
    #PATCH_COMMAND  ( patch -p1 --ignore-whitespace --verbose < /usr/bin/unxz --stdout ${_linux_patch_rt_p} )
    #  PATCH_COMMAND test -f ${_linux_patch_rt_p}.xz && /usr/bin/unxz ${_linux_patch_rt_p}.xz

    PATCH_COMMAND patch --strip=1 --ignore-whitespace --verbose --input=${_linux_patch_rt_p}
    #PATCH_VERBATIM
    
    #CONFIGURE_COMMAND test -f ${_linux_config_rt_p}.xz && /usr/bin/unxz --stdout /usr/src/linux/linux-config-${_linux_ver_ma_mi}/config.${_kharch}_rt_${_knarch}.xz > .config
    #CONFIGURE_COMMAND test -f ${_linux_config_rt_p}.xz && /usr/bin/unxz ${_linux_config_rt_p}.xz
    # in /usr/src/openairinterface5g/bld/src/ep_linux_source-build
    CONFIGURE_COMMAND cp -auv ${_linux_config_rt_p} ${LINUX_SOURCE_DIR}/.config
    
    #cd /usr/src/linux/linux-config-4.1
    #unxz config.amd64_rt_amd64.xz
    #cp config.amd64_rt_amd64 /usr/src/linux/.config
    #extra configs for oai...!
    #CONFIGURE_COMMAND scripts/config --disable DEBUG_INFO
    #CONFIG_PREEMPT=y
    #CONFIG_PREEMPT_RT_BASE=y
    #CONFIG_HAVE_PREEMPT_LAZY=y
    #CONFIG_PREEMPT_LAZY=y
    #CONFIG_PREEMPT_RT_FULL=y
    
    #BUILD_COMMAND fakeroot make -f debian/rules.real install-linux-base
    #  $ fakeroot make -f debian/rules.gen setup_i386_none_686-pae
    #$ make -C debian/build/build_i386_none_686-pae nconfig
    #make ARCH=ia64
    #make CROSS_COMPILE=ia64-linux-
# forget the building    
    BUILD_COMMAND make deb-pkg LOCALVERSION=-${_kernel_name} KDEB_PKGVERSION=${_kdeb_pkgver}
    BUILD_COMMAND && fakeroot debian/rules binary-arch
    ##BUILD_COMMAND make deb-pkg
    ##BUILD_DIR ${LINUX_SOURCE_DIR}
    BUILD_IN_SOURCE 1
    #BINARY_DIR 
    #BYPRODUCTS linux-image-4.1.3-rt3-kernel_name_1_amd64.deb
    BUILD_BYPRODUCTS               linux-libc-dev_${_kdeb_pkgver}_${_knarch}.deb
    linux-image-${_linux_ver}-${_kernel_name}_${_kdeb_pkgver}_${_knarch}.deb
    linux-headers-${_linux_ver}-${_kernel_name}_${_kdeb_pkgver}_${_knarch}.deb
    #
    #linux-image-4.1.3-rt3-kernel_name-dbg_1_amd64.deb
    #linux-headers-4.1.3-rt3-kernel_name_1_amd64.deb
    #linux-libc-dev_1_amd64.deb
    
    )

  #sudo /usr/bin/unxz /usr/src/linux-config-4.8/config.amd64_rt_amd64.xz
  #also ExternalProject_Add_StepDependencies(<name> <step> [target1 [target2 [...]]])
  #add_custom_command
  externalproject_add_stepdependencies(ep_linux_source patch linux_source_patch_xz)
  externalproject_add_stepdependencies(ep_linux_source configure linux_source_config_xz)



endif (_TODOPTZ170127_gona_use_openembedded_poky_instead)


#####   #    #  #    #   ####
#    #  #   #   ##  ##  #
#    #  ####    # ## #   ####
#    #  #  #    #    #       #
#    #  #   #   #    #  #    #
#####   #    #  #    #   ####


find_package(dkms)
#TODO: PTZ161017 "os_release" to be detected under cross-compilation (ie. with OS=cygwin,osx )
# so "os_" is the wrong name... need to get the linux version used , dkms would tell us. system_ !!
# default kernel...
#execute_process(COMMAND uname -r
#  OUTPUT_VARIABLE _with_kernel
#  OUTPUT_STRIP_TRAILING_WHITESPACE
#  )


#
# dkms-use.cmake
#
function(dkms_cmake_a_driver_oai_
    _subdir
    _module
    _ver
    _kernelver
    _compiler
    _source_tree
    _dkms_tree
    _install_tree
    )
  
  set(_pck_name oai-${_module}) ##TODO: PTZ191020 here oai or oai-${_module} ?  
  #set(_source_tree  ${CMAKE_CURRENT_BINARY_DIR}/ep_oai_dkms CACHE PATH "dkms source tree")
  #set(_dkms_tree    ${PROJECT_BINARY_DIR}/var/lib/dkms CACHE PATH "dkms tree path")
  #set(_install_tree ${PROJECT_BINARY_DIR}/lib/modules  CACHE PATH "dkms install tree path")
  ##TODO: PTZ161019
  set(${_module}_KO ${_install_tree}/${_kernelver}/updates/dkms/oai-${_module}.ko PARENT_SCOPE)

  string(REGEX REPLACE "^.*-" "" _arch ${_kernelver})

  if(CMAKE_SYSTEM_PROCESSOR STREQUAL ${_arch})
    #we are not cross compiling.
  elseif(CMAKE_SYSTEM_VERSION STREQUAL ${_kernelver})
    #ie. CMAKE_SYSTEM_VERSION 4.7.0-1-amd64 $(kernelver)
    #we are really not cross compiling.
  endif()

  ##TODO: PTZ161019 use ${_dkms_tree}/${_module}/${_ver}/build instead ?
  # _source_tree is a bad name in this context.
  ##TODO: PTZ161019 I gave up? nop...
  #configure_file(${_subdir}/dkms.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/dkms.conf)
  #configure_file(${_subdir}/Makefile.in  ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/Makefile)
  #configure_file(${_subdir}/dkms.conf.in ${_source_tree}/${_pck_name}-${_ver}/dkms.conf)
  #configure_file(${_subdir}/Makefile.in  ${_source_tree}/${_pck_name}-${_ver}/Makefile)
  #file(MAKE_DIRECTORY  ${_source_tree}/${_pck_name}-${_ver}/oai-${_module})
  file(MAKE_DIRECTORY  ${_source_tree}/oai-${_module})
  foreach(_f ${_pck_name}.Kbuild)
    configure_file(${_subdir}/${_f}.in ${_source_tree}/oai-${_module}/${_f})
  endforeach()
  
  file(GLOB _src
    #LIST_DIRECTORIES false
    ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.[ch]
    ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.Kbuild
    )
  foreach(_f ${_src})
    configure_file(${_f} ${_source_tree}/oai-${_module} COPYONLY)
  endforeach()

#   ExternalProject_Add(ep_oai_${_module}_${_kernelver}
#     PREFIX ${PROJECT_BINARY_DIR}
#     SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}
#     #SOURCE_DIR ${_subdir}
#     #SOURCE_DIR ${_pck_name}-${_ver}
#     ##TODO: PTZ161019 not build yet ? DEPENDS Kbuild ${_source_tree}/Makefile ${_source_tree}/dkms.conf
#     #DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/Kbuild
# #
# ##TODO: PTZ161019
# #CMAKE_GENERATOR <gen>
# #Specify generator for native build
# #CMAKE_GENERATOR_PLATFORM <platform>
# #Generator-specific platform name
# #CMAKE_GENERATOR_TOOLSET <toolset>
# #Generator-specific toolset name
# #CMAKE_ARGS <arg>...
# #UPDATE_COMMAND <cmd>...
#     #PTZ161024 have to prefre a copy.. not good for debugging.
#     #DOWNLOAD_COMMAND install --verbose -d ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.{c,h} ${_pck_name}-${_ver}/oai-${_module}
#     #UPDATE_COMMAND install --verbose -d ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.{c,h} ${_pck_name}-${_ver}/oai-${_module}
#     #in SOURCE_DIR
#     UPDATE_COMMAND mkdir -vp ${_source_tree}/${_pck_name}-${_ver}/oai-${_module}
#     UPDATE_COMMAND && install --verbose ${_src} ${_source_tree}/oai-${_module}
#     #UPDATE_COMMAND && cp -auv *.c *.h ${_source_tree}/${_pck_name}-${_ver}/oai-${_module}
#     #CONFIGURE_COMMAND ln -snfv ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir} ${_pck_name}-${_ver}/oai-${_module}
#     CONFIGURE_COMMAND ""
#     ##TODO: PTZ161020
#     BINARY_DIR ${_source_tree}
#     BUILD_COMMAND   ${DKMS_CMD} build ${_pck_name} ${DKMS_CONFIG}
#     INSTALL_COMMAND sudo ${DKMS_CMD} install ${_pck_name} ${DKMS_CONFIG}
#     BUILD_BYPRODUCTS ${${_module}_KO}
#     COMMENT "${_pck_name}.ko for ${_kernel}  done"
#     )
  #add_custom_target(${_module} DEPENDS ep_${_module}_${_kernelver})
  add_custom_target(${_module} DEPENDS ep_oai_${_kernelver})

  ##TODO: PTZ161020 not good, is there an alternative with install()?
  #file(MAKE_DIRECTORY   ${_install_tree}/${_kernelver})
  
endfunction(dkms_cmake_a_driver_oai_
    _subdir
    _module
    _ver
    _kernelver 
    _compiler
    _source_tree
    _dkms_tree
    _install_tree
    )


###

#TODO:PTZ170113 forget RTAI for now!... will gamble with rt-linux.
find_package(RTAI) 
##TODO: PTZ161020 RTAI... will likely not work with LINUX>-3.18.22, 
##TODO: PTZ161020 unlike RTlinux is it a better replacement?
#$ git clone https://github.com/ShabbyX/RTAI.git
if (RTAI_FOUND)
  #is this kernel less than 3.6 ?!
  if (${with_linux_version} VERSION_GREATER "3.5")
    # force it down...
    set(RTAI_FOUND "")
  endif ()
endif (RTAI_FOUND)

add_boolean_option(RTAI "${RTAI_FOUND}" "Use RTAI")
