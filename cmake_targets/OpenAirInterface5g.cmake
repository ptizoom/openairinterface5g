#/*
# * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
# * contributor license agreements.  See the NOTICE file distributed with
# * this work for additional information regarding copyright ownership.
# * The OpenAirInterface Software Alliance licenses this file to You under
# * the OAI Public License, Version 1.0  (the "License"); you may not use this file
# * except in compliance with the License.
# * You may obtain a copy of the License at
# *
# *      http://www.openairinterface.org/?page_id=698
# *
# * Unless required by applicable law or agreed to in writing, software
# * distributed under the License is distributed on an "AS IS" BASIS,
# * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# * See the License for the specific language governing permissions and
# * limitations under the License.
# *-------------------------------------------------------------------------------
# * For more information about the OpenAirInterface (OAI) Software Alliance:
# *      contact@openairinterface.org
# */

# Author: laurent THOMAS, Lionel GAUTHIER

##TODO: PTZ161016 to migrate the whole in subdirectories or root.
#   
#


##############################################################
#    ???!!! TO BE DOCUMENTED OPTIONS !!!???
##############################################################
add_boolean_option(ENABLE_SECURITY         True  "Enable LTE integrity and ciphering between RRC UE and eNB")
add_boolean_option(ENABLE_USE_MME          True  "eNB connected to MME (INTERFACE S1-C), not standalone eNB")
add_boolean_option(NO_RRM                  True  "DO WE HAVE A RADIO RESSOURCE MANAGER: NO")
add_boolean_option(USER_MODE True "when not using kernel primitives")
add_boolean_option(RRC_DEFAULT_RAB_IS_AM False "set the RLC mode to AM for the default bearer")

add_boolean_option(OAI_NW_DRIVER_TYPE_ETHERNET False "????")
add_boolean_option(DISABLE_USE_NAS False "???")
add_boolean_option(DEADLINE_SCHEDULER True "Use the Linux scheduler SCHED_DEADLINE: kernel >= 3.14")
add_boolean_option(CPU_AFFINITY False "Enable CPU Affinity of threads (only valid without deadline scheduler). It is enabled only with >2 CPUs")
add_boolean_option(NAS_ADDRESS_FIX False "specific to oaisim: for nasmesh driver")
add_boolean_option(NAS_NETLINK False "???? Must be True to compile nasmesh driver without rtai")
add_boolean_option(OAISIM False "specific to oaisim")
add_boolean_option(OAI_NW_DRIVER_USE_NETLINK True "????")

add_boolean_option(USE_MME False "this flag is used only one time in lte-softmodem.c")
add_list_string_option(PACKAGE_NAME "NotDefined" "As per attribute name")
add_boolean_option(MESSAGE_CHART_GENERATOR False         "For generating sequence diagrams")
add_boolean_option(MESSAGE_CHART_GENERATOR_RLC_MAC False "trace RLC-MAC exchanges in sequence diagrams")
add_boolean_option(MESSAGE_CHART_GENERATOR_PHY     False "trace some PHY exchanges in sequence diagrams")

add_boolean_option(ENB_AGENT                   True         "enable eNB agent to inteface with a SDN contrller")

########################
# Include order
##########################
add_boolean_option(ENB_MODE True "Swap the include directories between openair2 and openair3" )

##########################
# Emulation options
##########################
add_boolean_option(ENABLE_PGM_TRANSPORT    False "specific to oaisim, emulation through ethernet, reliable multicast")
add_boolean_option(ADDR_CONF               False "specific to oaisim, IP autoconf of user-plane IP interface")
add_boolean_option(OPENAIR_EMU             False "specific to oaisim")
add_boolean_option(OAI_EMU                 False "specific to oaisim")
add_boolean_option(PHY_ABSTRACTION         False "specific to oaisim")

##########################
# SCHEDULING/REAL-TIME/PERF options
##########################
add_boolean_option(ENABLE_USE_CPU_EXECUTION_TIME True "Add data in vcd traces: disable it if perf issues")
add_boolean_option(ENABLE_VCD              True  "always true now, time measurements of proc calls and var displays")
add_boolean_option(ENABLE_VCD_FIFO         True  "time measurements of proc calls and var displays sent to FIFO (one more thread)")
add_boolean_option(HARD_RT                 False "???")
add_boolean_option(LINUX                   False "used in weird memcpy() in pdcp.c ???")
add_boolean_option(LINUX_LIST              False "used only in lists.c: either use OAI implementation of lists or Linux one (should be True, but it is False")
add_boolean_option(LOG_NO_THREAD           True  "Disable thread for log, seems always set to true")
add_boolean_option(OPENAIR_LTE             True "Seems legacy: keep it to true")

##########################
# PHY options
##########################
add_boolean_option(DRIVER2013              True "only relevant for EXMIMO")
add_boolean_option(ENABLE_FXP              True "????")
add_boolean_option(ENABLE_NEW_MULTICAST    False "specific to oaisim")
add_boolean_option(EXMIMO_IOT              True "????")
add_boolean_option(LARGE_SCALE             False "specific to oaisim: defines max eNB=2 and max UE=120")
add_boolean_option(LOCALIZATION            False "???")
add_integer_option(MAX_NUM_CCs             1     "????")
add_boolean_option(MU_RECEIVER             False "????")
add_boolean_option(NEW_FFT                 True "????")
add_boolean_option(OPENAIR1                True "????")
add_boolean_option(PBS_SIM                 False "????")
add_boolean_option(PC_DSP                  True "????")
add_boolean_option(PC_TARGET               True "????")
add_boolean_option(PERFECT_CE              False "????")
add_boolean_option(PHYSIM                  True  "for L1 simulators (dlsim, ulsim, ...)")
add_boolean_option(PHY_CONTEXT             True "not clear: must remain False for dlsim")
add_boolean_option(PHY_EMUL                False "not clear: must remain False for dlsim")
add_boolean_option(PUCCH                   True "????")
add_boolean_option(RANDOM_BF               False "????")
add_boolean_option(SMBV                    False "Rohde&Schwarz SMBV100A vector signal generator")
add_boolean_option(DEBUG_PHY               False "Enable PHY layer debugging options")
add_boolean_option(DEBUG_PHY_PROC          False "Enable debugging of PHY layer procedures")
add_boolean_option(DEBUG_DLSCH             False "Enable debugging of DLSCH physical layer channel")

##########################
# 802.21 options
##########################
add_boolean_option(ENABLE_RAL              False "ENABLE 802.21 INTERFACE")
add_boolean_option(USE_3GPP_ADDR_AS_LINK_ADDR False "As per attribute name")

##########################
# NAS LAYER OPTIONS
##########################
add_boolean_option(ENABLE_NAS_UE_LOGGING   True  "????")
add_boolean_option(NAS_BUILT_IN_UE         True  "UE NAS layer present in this executable")
add_boolean_option(NAS_UE                  True  "NAS UE INSTANCE (<> NAS_MME)")


##########################
# ACCESS STRATUM LAYER2 OPTIONS
##########################
add_boolean_option(MAC_CONTEXT             True  "specific to oaisim")
add_boolean_option(JUMBO_FRAME             True  "ENABLE LARGE SDU in ACCESS STRATUM (larger than common MTU)")

##########################
# RLC LAYER OPTIONS
##########################
add_boolean_option(OPENAIR2                True  "Access Stratum layer 2 built in executable")
add_boolean_option(TRACE_RLC_PAYLOAD       False "Fatal assert in this case")
add_boolean_option(RLC_STOP_ON_LOST_PDU    False "Fatal assert in this case")

add_boolean_option(TRACE_RLC_MUTEX         True  "TRACE for RLC, possible problem in thread scheduling")
add_boolean_option(TRACE_RLC_AM_BO         False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_FREE_SDU   False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_HOLE       False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_PDU        False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_RESEGMENT  False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_RX         False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_RX_DECODE  False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_TX         False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_TX_STATUS  False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_AM_STATUS_CREATION   False "TRACE for RLC AM, TO BE CHANGED IN A MORE GENERAL FLAG")

add_boolean_option(STOP_ON_IP_TRAFFIC_OVERLOAD      False "")
add_boolean_option(TRACE_RLC_UM_DAR        False "TRACE for RLC UM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_UM_DISPLAY_ASCII_DATA  False "TRACE for RLC UM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_UM_PDU        False "TRACE for RLC UM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_UM_RX         False "TRACE for RLC UM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_UM_SEGMENT    False "TRACE for RLC UM, TO BE CHANGED IN A MORE GENERAL FLAG")
add_boolean_option(TRACE_RLC_UM_TX_STATUS  False "TRACE for RLC UM, TO BE CHANGED IN A MORE GENERAL FLAG")


##########################
# PDCP LAYER OPTIONS
##########################
add_boolean_option(PDCP_USE_NETLINK            True  "For eNB, PDCP communicate with a NETLINK socket if connected to network driver, else could use a RT-FIFO")
add_boolean_option(PDCP_USE_NETLINK_QUEUES     True  "When PDCP_USE_NETLINK is true, incoming IP packets are stored in queues")
add_boolean_option(LINK_ENB_PDCP_TO_IP_DRIVER  True  "For eNB, PDCP communicate with a IP driver")
add_boolean_option(LINK_ENB_PDCP_TO_GTPV1U     True  "For eNB, PDCP communicate with GTP-U protocol (eNB<->S-GW)")

##########################
# RRC LAYER OPTIONS
##########################
add_boolean_option(RRC_DEFAULT_RAB_IS_AM       False  "Otherwise it is UM, configure params are actually set in rrc_eNB.c:rrc_eNB_generate_defaultRRCConnectionReconfiguration(...)")


##########################
# S1AP LAYER OPTIONS
##########################
# none

##########################
# PROJECTS (IST, FRENCH COLL., etc)
# SPECIFIC OPTIONS
##########################
add_boolean_option(SPECTRA False "???")
add_boolean_option(MIH_C_MEDIEVAL_EXTENSIONS False "EXTENSIONS TO MIH 802.21 IN CONTEXT OF IST PROJECT CALLED MEDIEVAL")



add_boolean_option(EMOS False "????")
if(${EMOS})
  add_definitions("-D_FILE_OFFSET_BITS=64")
  set(EMOS_LIB gps)
endif(${EMOS})


 # add the binary tree to the search path for include files
#######################################################
# We will find ConfigOAI.h after generation in target directory
include_directories("${OPENAIR_BIN_DIR}")
# add directories to find all include files
# the internal rule is to use generic names such as defs.h
# but to make it uniq name as adding the relative path in the include directtive
# example: #include "RRC/LITE/defs.h"
#find_path (include_dirs_all *.h ${OPENAIR_DIR})
#find_path (include_dirs_all *.h PATHS /usr/include NO_CMAKE_PATH)
#include_directories("${include_dirs_all}")

# Legacy exact order
#PTZ160913 not anymore... fixed this mess ENB/UE common interface by using _COMMON_OAI_ defines, ENB_MODE could also be used...
include_directories(${PROJECT_SOURCE_DIR}/common/oai)

if(ENB_MODE)
  include_directories("${OPENAIR2_DIR}/COMMON")
  include_directories("${OPENAIR2_DIR}/UTIL")
  include_directories("${OPENAIR2_DIR}/UTIL/LOG")
  include_directories("${OPENAIR3_DIR}/COMMON")
  include_directories("${OPENAIR3_DIR}/UTILS")
else()
  include_directories("${OPENAIR3_DIR}/COMMON")
  include_directories("${OPENAIR3_DIR}/UTILS")
  include_directories("${OPENAIR2_DIR}/COMMON")
  include_directories("${OPENAIR2_DIR}/UTIL")
  include_directories("${OPENAIR2_DIR}/UTIL/LOG")
endif()
include_directories("${OPENAIR1_DIR}")
include_directories("${OPENAIR2_DIR}/NAS")
include_directories("${OPENAIR2_DIR}")
include_directories("${OPENAIR2_DIR}/LAYER2/RLC")
include_directories("${OPENAIR2_DIR}/LAYER2/RLC/AM_v9.3.0")
include_directories("${OPENAIR2_DIR}/LAYER2/RLC/UM_v9.3.0")
include_directories("${OPENAIR2_DIR}/LAYER2/RLC/TM_v9.3.0")
include_directories("${OPENAIR2_DIR}/LAYER2/PDCP_v10.1.0")
include_directories("${OPENAIR2_DIR}/RRC/LITE/MESSAGES")
include_directories("${OPENAIR2_DIR}/RRC/LITE")
include_directories("${OPENAIR3_DIR}/RAL-LTE/INTERFACE-802.21/INCLUDE")
include_directories("${OPENAIR3_DIR}/RAL-LTE/LTE_RAL_ENB/INCLUDE")
include_directories("${OPENAIR3_DIR}/RAL-LTE/LTE_RAL_UE/INCLUDE")
include_directories("${OPENAIR_DIR}/common/utils")
include_directories("${OPENAIR_DIR}/common/utils/itti")
include_directories("${OPENAIR3_DIR}/NAS/COMMON")
include_directories("${OPENAIR3_DIR}/NAS/COMMON/API/NETWORK")
include_directories("${OPENAIR3_DIR}/NAS/COMMON/EMM/MSG")
include_directories("${OPENAIR3_DIR}/NAS/COMMON/ESM/MSG")
include_directories("${OPENAIR3_DIR}/NAS/COMMON/IES")
include_directories("${OPENAIR3_DIR}/NAS/COMMON/UTIL")
include_directories("${OPENAIR3_DIR}/SECU")
include_directories("${OPENAIR3_DIR}/SCTP")
include_directories("${OPENAIR3_DIR}/S1AP")
include_directories("${OPENAIR2_DIR}/X2AP")
include_directories("${OPENAIR3_DIR}/UDP")
include_directories("${OPENAIR3_DIR}/GTPV1-U")
include_directories("${OPENAIR_DIR}/targets/COMMON")
include_directories("${OPENAIR_DIR}/targets/ARCH/COMMON")
include_directories("${OPENAIR_DIR}/targets/ARCH/EXMIMO/USERSPACE/LIB/")
include_directories("${OPENAIR_DIR}/targets/ARCH/EXMIMO/DEFS")
include_directories("${OPENAIR2_DIR}/ENB_APP")
include_directories("${OPENAIR2_DIR}/UTIL/OSA")
include_directories("${OPENAIR2_DIR}/UTIL/LFDS/liblfds6.1.1/liblfds611/inc")
include_directories("${OPENAIR2_DIR}/UTIL/MEM")
include_directories("${OPENAIR2_DIR}/UTIL/LISTS")
include_directories("${OPENAIR2_DIR}/UTIL/FIFO")
include_directories("${OPENAIR2_DIR}/UTIL/OCG")
include_directories("${OPENAIR2_DIR}/UTIL/MATH")
include_directories("${OPENAIR2_DIR}/UTIL/TIMER")
include_directories("${OPENAIR2_DIR}/UTIL/OMG")
include_directories("${OPENAIR2_DIR}/UTIL/OTG")
include_directories("${OPENAIR2_DIR}/UTIL/CLI")
include_directories("${OPENAIR2_DIR}/UTIL/OPT")
include_directories("${OPENAIR2_DIR}/UTIL/OMV")
include_directories("${OPENAIR2_DIR}/RRC/LITE/MESSAGES")
include_directories("${OPENAIR3_DIR}/GTPV1-U/nw-gtpv1u/shared")
include_directories("${OPENAIR3_DIR}/GTPV1-U/nw-gtpv1u/include")
include_directories("${OPENAIR_DIR}")

# Hardware dependant options
###################################
add_list1_option(NB_ANTENNAS_RX "2" "Number of antennas in reception" "1" "2" "4")
add_list1_option(NB_ANTENNAS_TX "2" "Number of antennas in transmission" "1" "2" "4")
add_list1_option(NB_ANTENNAS_TXRX "2" "Number of antennas in ????" "1" "2" "4")

add_list2_option(RF_BOARD "EXMIMO" "RF head type" "None" "EXMIMO" "OAI_USRP" "OAI_BLADERF" "CPRIGW" "OAI_LMSSDR")

add_list2_option(TRANSP_PRO "None" "Transport protocol type" "None" "ETHERNET")


#use native cmake method as this package is not in pkg-config
if (${RF_BOARD} STREQUAL "OAI_USRP")
  find_package(Boost REQUIRED)
  include_directories(${LIBBOOST_INCLUDE_DIR})
endif (${RF_BOARD} STREQUAL "OAI_USRP")










#
# libconfig-dev needed by 
#

find_package(libconfig)
check_include_file(libconfig.h HAVE_LIBCONFIG_H)

if (NOT HAVE_LIBCONFIG_H) #OR libconfig_DIR MATCHES NOTFOUND
  #if(DEBIAN_)
  #command
  #apt-get install libconfig-dev
  #else
  if (CYGWIN)
    #https://github.com/libconfig/libconfig.git
    
    set(_lib_rdx config)
    set(_lib_rdx_u LIBCONFIG)
    set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/libconfig)

    #https://sourceforge.net/projects/libconfig/files/latest/download?source=files
    #https://github.com/hyperrealm/libconfig    
    ExternalProject_Add(ep_config
      PREFIX ${PROJECT_BINARY_DIR}
      GIT_REPOSITORY https://github.com/hyperrealm/libconfig.git
      #GIT_REPOSITORY git://koala/sctplib.git
      SOURCE_DIR ${_src_d}
      UPDATE_DISCONNECTED 1
      CONFIGURE_COMMAND ${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared
      BUILD_COMMAND make
      INSTALL_COMMAND make install
      BUILD_BYPRODUCTS libconfig.la libconfig.h ${CMAKE_SHARED_LIBRARY_PREFIX}config-1${CMAKE_SHARED_LIBRARY_SUFFIX}
      STEP_TARGETS  install
      )
    ExternalProject_Add_Step(ep_config autoupdate
      DEPENDEES download
      COMMAND autoupdate --verbose
      WORKING_DIRECTORY ${_src_d}
      )
    ExternalProject_Add_Step(ep_config init
      DEPENDEES autoupdate
      COMMAND autoreconf --force --install --verbose --warnings=all --symlink
      COMMENT "asn1c OAI customised :: being autoreconfed"
      WORKING_DIRECTORY ${_src_d}
      BYPRODUCTS configure Makefile.in config.h.in
      )
    
    #TODO:PTZ170210 installed in ${PROJECT_BINARY_DIR}/lib
    externalproject_get_property(ep_config install_dir binary_dir)
    list(APPEND LIBCONFIG_INCLUDEDIR "${install_dir}/include")
    list(APPEND LIBCONFIG_LIBS
      ${_lib_rdx}
      #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    add_library(${_lib_rdx} STATIC IMPORTED)
    #add_library(${_lib_rdx} SHARED IMPORTED)
    set_property(TARGET ${_lib_rdx}
      PROPERTY
      IMPORTED_LOCATION
      "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    add_dependencies(${_lib_rdx} ep_config)
    set(LIBCONFIG_LIBRARIES  ${_lib_rdx})
    #TODO:PTZ170308 redo a search!
    set(LIBCONFIG_FOUND 1)
  else  (CYGWIN)
    set(LIBCONFIG_LIBRARIES  )
  endif (CYGWIN)
endif (HAVE_LIBCONFIG-NOTFOUND)
#
#
#
#PTZ170210 pkg_search_module(CONFIG libconfig)
#like in cygwin it has not been ported yet
if (NOT "${LIBCONFIG_FOUND}")
   message(WARNING "PACKAGE libconfig is required by binaries such as oaisim: will fail later if this target is built")
   find_library(LIBCONFIG_LIBRARIES config
     PATHS / /usr /usr/local
     PATH_SUFFIXES lib
     )
  find_path(LIBCONFIG_INCLUDEDIR config.h
    PATHS /usr /usr/local
    PATH_SUFFIXES include
    )
endif (NOT "${LIBCONFIG_FOUND}")

include_directories(${LIBCONFIG_INCLUDEDIR})





pkg_search_module(CRYPTO libcrypto REQUIRED)
include_directories(${CRYPTO_INCLUDE_DIRS})

#PTZ170209 OPENPGM_LIBRARIES
#    linked by target "SIMU_ETH" in directory /usr/src/openairinterface5g
#    linked by target "oaisim" in directory /usr/src/openairinterface5g/cmake_targets/oaisim_build_oai
#    linked by target "oaisim_nos1" in directory /usr/src/openairinterface5g/cmake_targets/oaisim_noS1_build_oai
# in /usr/lib/x86_64-linux-gnu/pkgconfig/openpgm-5.2.pc  .. libpgm-dev

pkg_search_module(OPENPGM openpgm-5.2)
if (NOT "${OPENPGM_FOUND}")
  message(WARNING "PACKAGE openpgm-5.{1,2} is required by binaries such as oaisim: will try to build it on the spot or install with apt-get etc ...")
  find_library(OPENPGM_LIBRARIES pgm)
  find_path(OPENPGM_INCLUDE_DIRS pgm.h
    PATH_SUFFIXES pgm
    )
  if (NOT OPENPGM_INCLUDE_DIRS)
    if (CYGWIN)
      message(WARNING "although there is a git@github.com:fd00/yacp.git/libpgm/ for cygwin")

      set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/openpgm)
      #if debian... run apt-get install
      #  cmake -DENABLE_STATIC_LIBS=ON <path to OPENPGM source>
      ExternalProject_Add(ep_openpgm
	PREFIX ${PROJECT_BINARY_DIR}
	GIT_REPOSITORY https://github.com/steve-o/openpgm.git
	#GIT_SUBMODULES ""
	#TODO:PTZ170206 makefile in  ${_src_d}/openpgm/pgm
	SOURCE_DIR "${_src_d}"
	#TODO:PTZ170206 screewy dooby __ep_write_gitclone_script SOURCE_SUBDIR openpgm/pgm
	UPDATE_DISCONNECTED 1
	CONFIGURE_COMMAND ${_src_d}/openpgm/pgm/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared
	BUILD_COMMAND make
	INSTALL_COMMAND make install
	BUILD_BYPRODUCTS libpgm.la pgm/pgm.h ${CMAKE_SHARED_LIBRARY_PREFIX}pgm-0${CMAKE_SHARED_LIBRARY_SUFFIX}
	STEP_TARGETS  install
	)
      ExternalProject_Add_Step(ep_openpgm autoupdate
         DEPENDEES download
         COMMAND autoupdate --verbose
         WORKING_DIRECTORY ${_src_d}/openpgm/pgm
         )
      ExternalProject_Add_Step(ep_openpgm init
         DEPENDEES autoupdate
         COMMAND autoreconf --force --install --verbose --warnings=all --symlink
         COMMENT "openpgm  :: being autoreconfed for OAI"
         WORKING_DIRECTORY ${_src_d}/openpgm/pgm
         BYPRODUCTS configure Makefile.in config.h.in
         )
       #TODO:PTZ170210 installed in ${PROJECT_BINARY_DIR}/lib
       set(_lib_rdx pgm)
       externalproject_get_property(ep_openpgm install_dir binary_dir)
       #TODO:PTZ170209 un-hardcode this pgm-5.2 sometimes
       list(APPEND OPENPGM_INCLUDE_DIRS "${install_dir}/include/pgm-5.2")
       list(APPEND OPENPGM_LIBS
	 ${_lib_rdx}
	 #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
	 )
       add_library(${_lib_rdx} STATIC IMPORTED)
       #add_library(${_lib_rdx} SHARED IMPORTED)
       set_property(TARGET ${_lib_rdx}
	 PROPERTY
	 IMPORTED_LOCATION
	 "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}"
	 #"${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
	 #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
	 )
       message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
       add_dependencies(${_lib_rdx} ep_openpgm)
       set(OPENPGM_LIBRARIES  ${_lib_rdx})
       #set(OPENPGM_LIBRARIES  /usr/src/openairinterface5g/bld/lib/lib${_lib_rdx}.a)
      
    else (CYGWIN)
      #TODO:PTZ170308 redo a search!
      set(ENABLE_PGM_TRANSPORT False)
      add_definitions("-UENABLE_PGM_TRANSPORT")
      set(OPENPGM_LIBRARIES  )  
    endif (CYGWIN)
  endif (NOT OPENPGM_INCLUDEDIR)

else (NOT "${OPENPGM_FOUND}")
endif (NOT "${OPENPGM_FOUND}")
#PTZ170316 OPENPGM_CFLAGS OPENPGM_LDFLAGS could also be used
include_directories(${OPENPGM_INCLUDE_DIRS})


pkg_search_module(NETTLE nettle)
if(NOT ${NETTLE_FOUND})
  message("PACKAGE nettle not found: some targets will fail")
else()
  include_directories(${NETTLE_INCLUDE_DIRS})
endif()

pkg_search_module(XPM xpm)
if(NOT ${XPM_FOUND})
  message("PACKAGE xpm not found: some targets will fail")
else()
  include_directories(${XPM_INCLUDE_DIRS})
endif()

# Atlas is required by some packages, but not found in pkg-config
pkg_search_module(LAPACK lapack-atlas)
pkg_search_module(BLAS   blas-atlas)
pkg_search_module(ATLAS  atlas)
if((NOT ${LAPACK_FOUND}) OR (NOT ${BLAS_FOUND}))
  find_library(ATLAS_LIBRARIES lapack cblas atlas lapack_atlas
    HINTS / /usr /usr/local
    PATH_SUFFIXES lib lib/atlas-base/atlas
    )
  message(WARNING "No Atlas package found; ATLAS_LIBRARIES=${ATLAS_LIBRARIES}")

else ()
  find_library(ATLAS_LIBRARIES lapack_atlas
    HINTS / /usr /usr/local
    PATH_SUFFIXES lib lib/atlas-base lib/atlas-base/atlas
    )
endif ()

if (NOT BLAS_LIBRARIES)
  find_library(BLAS_LIBRARIES cblas blas
    HINTS / /usr /usr/local
    PATH_SUFFIXES lib lib/atlas-base/atlas
    )
  message(WARNING "No Blas package found;but found BLAS_LIBRARIES=${BLAS_LIBRARIES}")
endif (NOT BLAS_LIBRARIES)
if (NOT LAPACK_LIBRARIES)
  find_library(LAPACK_LIBRARIES lapack atlas lapack_atlas
    HINTS / /usr /usr/local
    PATH_SUFFIXES lib lib/atlas-base/atlas
    )
  message(WARNING "No Atlas package found; LAPACK_LIBRARIES=${LAPACK_LIBRARIES}")
endif (NOT LAPACK_LIBRARIES)
if (ATLAS_LIBRARIES MATCHES NOTFOUND)
  set(ATLAS_LIBRARIES ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})
else ()
  list(APPEND ATLAS_LIBRARIES ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})
endif ()
find_path(ATLAS_INCLUDE_DIRS cblas.h clapack.h
  PATHS /usr /usr/local    
  PATH_SUFFIXES include include/atlas
  )
list(APPEND ATLAS_INCLUDE_DIRS ${BLAS_INCLUDE_DIRS} ${LAPACK_INCLUDE_DIRS})

# Atlas is required by some packages, but not found in pkg-config
if(NOT ${ATLAS_INCLUDE_DIRS})
  message(WARNING "No Blas/Atlas libs found, some targets will fail:ATLAS_LIBRARIES=${ATLAS_LIBRARIES}")
  list(APPEND ATLAS_INCLUDE_DIRS /usr/include/atlas)
endif()
include_directories(${ATLAS_INCLUDE_DIRS})

pkg_search_module(NETTLE nettle)
if(NOT ${NETTLE_FOUND})
  message("PACKAGE nettle not found: some targets will fail")
else()
  include_directories(${NETTLE_INCLUDE_DIRS})
endif()

pkg_search_module(XPM xpm)
if(NOT ${XPM_FOUND})
  message("PACKAGE xpm not found: some targets will fail")
else()
  include_directories(${XPM_INCLUDE_DIRS})
endif()

pkg_check_modules(XFORMS forms)

# if (NOT ${XFORMS_FOUND})
#   find_library(XFORMS_LIBRARIES forms)
#   if(${XFORMS_LIBRARIES} MATCHES "forms")
#     set(XFORMS_FOUND true)
#     set(XFORMS_INCLUDE_DIRS )
#   else()
#     set(XFORMS_LIBRARIES forms)
#   endif()
# endif()

if(${XFORMS_FOUND})
  set(XFORMS_SOURCE
    #forms.h
    ${OPENAIR1_DIR}/PHY/TOOLS/lte_phy_scope.c
    )
  set(XFORMS_SOURCE_SOFTMODEM
    #forms.h
    ${OPENAIR_TARGETS}/RT/USER/stats.c
    )
  set(XFORMS_LIBRARIES forms)
  #include_directories ("/usr/include/X11")
  include(${XFORMS_INCLUDE_DIRS})
  
else(${XFORMS_FOUND})
  ##TODO: PTZ160825 mais je ne cromprends pas!
  find_library(XFORMS_LIBRARIES forms
    PATHS /usr/local /usr
    PATH_SUFFIXES lib
  )
  find_file(XFORMS_h forms.h
    PATHS /usr/local /usr
    PATH_SUFFIXES include 
  )
  set(XFORMS_SOURCE
    ${XFORMS_h}
    ${OPENAIR1_DIR}/PHY/TOOLS/lte_phy_scope.c
    )
  set(XFORMS_SOURCE_SOFTMODEM
    ${XFORMS_h}
    ${OPENAIR_TARGETS}/RT/USER/stats.c
    )
  #set(XFORMS_LIBRARIES forms)
endif (${XFORMS_FOUND})

message(SATUS "forms ${XFORMS_LIBRARIES},${XFORMS_SOURCE}  are quite required")

#####


#############################
# ASN.1 grammar C code generation & dependencies
################################
# A difficulty: asn1c generates C code of a un-predictable list of files
# so, generate the c from asn1c once at cmake run time
# So, if someone modify the asn.1 source file in such as way that it will create
# (so creating new asn.1 objects instead of modifying the object attributes)
# New C code source file, cmake must be re-run (instead of re-running make only)
#############

set(ASN1C_DIR ${OPENAIR_CMAKE})

include(Asn1c)

#### tools to pre-procecess  asn1c files
# TODO:PTZ160811 put into asn1c-config.cmake
#could also use env{ASN1C_CMD}=${ASN1C_INSTALL_DIR}/bin/asn1c

set(asn1c_call ASN1C_CMD="${ASN1C_CMD}" "${OPENAIR_CMAKE}/tools/generate_asn1")
set(fix_asn1c_call "${OPENAIR_CMAKE}/tools/fix_asn1")

set(asn1_generated_dir ${OPENAIR_BIN_DIR})

set(protoc_call "${OPENAIR_CMAKE}/tools/generate_protobuf")
set(protobuf_generated_dir ${OPENAIR_BIN_DIR})

#
#  LFDS
#
# Make lfds as a own source code (even if it is a outside library)
# For better intergration with compilation flags & structure of cmake
###################################################################
set(lfds ${OPENAIR2_DIR}/UTIL/LFDS/liblfds6.1.1/liblfds611/src/)
file(GLOB lfds_queue ${lfds}/lfds611_queue/*.c)
file(GLOB lfds_ring ${lfds}/lfds611_ringbuffer/*.c)
file(GLOB lfds_slist ${lfds}/lfds611_slist/*.c)
file(GLOB lfds_stack ${lfds}/lfds611_stack/*.c)
file(GLOB lfds_freelist ${lfds}/lfds611_freelist/*.c)

include_directories(${lfds})
add_library(LFDS SHARED
  ${lfds_queue} ${lfds_ring} ${lfds_slist} ${lfds_stack} ${lfds_freelist}
  ${lfds}/lfds611_liblfds/lfds611_liblfds_abstraction_test_helpers.c
  ${lfds}/lfds611_liblfds/lfds611_liblfds_aligned_free.c
  ${lfds}/lfds611_liblfds/lfds611_liblfds_aligned_malloc.c
  ${lfds}/lfds611_abstraction/lfds611_abstraction_free.c
  ${lfds}/lfds611_abstraction/lfds611_abstraction_malloc.c
)

###
#include T directory even if the T is off because T macros are in the code
#no matter what
# T_IDs.h is generated here
###
#PTZ170125 before itti! T_cache is needed in itti
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/common/utils/T)

include_directories(
  #"${OPENAIR_DIR}/common/utils/T"
  "${PROJECT_BINARY_DIR}/common/utils/T"
  )

#
# ITTI
#
#####################################################
add_boolean_option(ENABLE_ITTI True "ITTI is internal messaging, should remain enabled for most targets")
set(ITTI_DIR ${OPENAIR_DIR}/common/utils/itti)
if(${ENABLE_ITTI})
  #add_subdirectory(${OPENAIR_DIR}/common/utils/itti)
  set(GTPU_need_ITTI ${OPENAIR3_DIR}/GTPV1-U/gtpv1u_eNB.c)

  add_library(ITTI SHARED
    # add .h files if depend on (this one is generated)
    ${ITTI_DIR}/intertask_interface.h
    ${ITTI_DIR}/intertask_interface.c
    ${ITTI_DIR}/intertask_interface_dump.c
    ${ITTI_DIR}/backtrace.c
    ${ITTI_DIR}/memory_pools.c
    ${ITTI_DIR}/signals.c
    ${ITTI_DIR}/timer.c
    #PTZ170125 just noticed T_cache ... needs to be linked?!
    ${T_SOURCE}
    #${GTPU_need_ITTI}
  )
  set(ITTI_LIB ITTI)
  add_dependencies(ITTI rrc_srcs generate_T ${EPOLL_TARGETS})

if (NOT "${HAVE_SYS_EPOLL_H}" AND "${CYGWIN}")
  add_dependencies(ITTI ep_sys_epoll)
endif (NOT "${HAVE_SYS_EPOLL_H}" AND "${CYGWIN}")

  target_link_libraries(ITTI
    LFDS
    ${EPOLL_LIBS}
    )
  
endif (${ENABLE_ITTI})


# Hack on a test of asn1c version (already dirty)
add_definitions(-DASN1_MINIMUM_VERSION=924)

#### RRC/S1AP/X2C common includes.
include_directories(
  ${PROJECT_SOURCE_DIR}
  ${PROJECT_BINARY_DIR}/share/oai
)


# RRC
######
# need asn1_generated_dir
#find_package(asn1c)
#
#set(RRC_ASN1C_SOURCE_DIR  ${OPENAIR2_DIR}/RRC/LITE/MESSAGES)
set(_prj_rdx  oai_rrc)
set(OAI_RRC_SOURCE_DIR  ${PROJECT_SOURCE_DIR}/openair2/RRC/LITE)

add_boolean_option(RRC_MSG_PRINT False "print RRC messages")
set(RRC_GRAMMAR_DIR  ${OAI_RRC_SOURCE_DIR}/MESSAGES/asn1c/ASN1_files)
add_list2_option(RRC_ASN1_VERSION "Rel10" "ASN.1 version of RRC interface" "Rel8" "Rel10" "CBA")
string(TOLOWER ${RRC_ASN1_VERSION} rel_t)
if (${RRC_ASN1_VERSION} STREQUAL "Rel8")
  set (RRC_GRAMMAR ${RRC_GRAMMAR_DIR}/EUTRA-RRC-Definitions-86.asn)
elseif (${RRC_ASN1_VERSION} STREQUAL "CBA")
  set (RRC_GRAMMAR ${RRC_GRAMMAR_DIR}/EUTRA-RRC-Definitions-a20-lola.asn)
else()
  set (RRC_GRAMMAR ${RRC_GRAMMAR_DIR}/EUTRA-RRC-Definitions-a20.asn)
endif  (${RRC_ASN1_VERSION} STREQUAL "Rel8")

add_subdirectory(${OAI_RRC_SOURCE_DIR})

include_directories(${OAI_RRC_INCLUDE_DIRS})

message(STATUS oai::OAI_RRC_ASN1C_INCLUDE_DIRS=
  ${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}.../asn1_constants.h
  " -OAI_RRC_INCLUDE_DIRS-" ${OAI_RRC_INCLUDE_DIRS}
  " --sources>=${OAI_RRC_ASN1C_SOURCES}"
  " --sources>=${OAI_RRC_ASN1C_SRCS}"
  )
if(NOT OAI_RRC_ASN1C_SOURCES OR NOT OAI_RRC_LIBRARIES)
  message(FATAL_ERROR "empty lib, sources, parent_scope , it DID NOT WORK")
endif()

#TODO: PTZ160821 23
###also done in sub-directory, but does not seem to work in this one ? latter?!
## only RRC_LIB  is exported

set_source_files_properties(
  ${OAI_RRC_ASN1C_SRCS}
  PROPERTIES
  GENERATED true
  )

add_custom_target(rrc_srcs
  DEPENDS ep_oai_rrc_asn1c
  SOURCES ${OAI_RRC_ASN1C_SRCS}
  )



#
# S1AP
# Same limitation as described in RRC: unknown generated file list
# so we generate it at cmake time
##############

set(_prj_rdx  oai_s1ap)
set(OAI_S1AP_SOURCE_DIR ${PROJECT_SOURCE_DIR}/openair3/S1AP)
#set(S1AP_DIR ${OPENAIR3_DIR}/S1AP)

add_list1_option(S1AP_VERSION R10 "S1AP Asn.1 grammar version" R8 R9 R10)
string(TOLOWER ${S1AP_VERSION} rel_t)
if (${S1AP_VERSION} STREQUAL "R10")
  set (ASN1RELDIR R10.5)
  add_definitions("-DUPDATE_RELEASE_9 -DUPDATE_RELEASE_10")
elseif (${S1AP_VERSION} STREQUAL "R9")
  set (ASN1RELDIR R9.8)
  add_definitions("-DUPDATE_RELEASE_9")
else(${S1AP_VERSION} STREQUAL "R8")
  set (ASN1RELDIR R8.10)
endif(${S1AP_VERSION} STREQUAL "R10")

add_subdirectory("${OAI_S1AP_SOURCE_DIR}")

include_directories(${OAI_S1AP_INCLUDE_DIRS})

message(STATUS oai::OAI_S1AP_ASN1C_INCLUDE_DIRS=
  ${OAI_S1AP_ASN1C_INSTALL_INCLUDE_DIR}.../asn1_constants.h
  " -OAI_S1AP_INCLUDE_DIRS-" ${OAI_S1AP_INCLUDE_DIRS}
  " --sources>=${OAI_S1AP_ASN1C_SOURCES}"
  " --sources>=${OAI_S1AP_ASN1C_SRCS}"
  )
if(NOT OAI_S1AP_ASN1C_SOURCES)
  message(FATAL_ERROR "empty sources, parent_scope DID NOT WORK")
endif()

#TODO: PTZ160821 
###also done in sub-directory, but does not seem to work in this one ? latter?!
## only S1AP_LIB  is expoerted
set_source_files_properties(#${OAI_S1AP_ASN1C_SOURCES} nok
  ${OAI_S1AP_ASN1C_SRCS}
  PROPERTIES
  GENERATED true
  )

#X2AP
# Same limitation as described in RRC/S1AP: unknown generated file list
# so we generate it at cmake time
##############
set(_prj_rdx  oai_x2ap)
set(OAI_X2AP_SOURCE_DIR   ${PROJECT_SOURCE_DIR}/openair2/X2AP)
#set(X2AP_DIR ${OPENAIR2_DIR}/X2AP)

add_list1_option(X2AP_VERSION R11 "X2AP Asn.1 grammar version" R10 R11)
string(TOLOWER ${X2AP_VERSION} rel_t)
if (${X2AP_VERSION} STREQUAL "R11")
  set (ASN1RELDIR R11.2)
elseif (${X2AP_VERSION} STREQUAL "R10")
  set (ASN1RELDIR R.UNKNOWN)
endif(${X2AP_VERSION} STREQUAL "R11")

add_subdirectory("${OAI_X2AP_SOURCE_DIR}")
include_directories(${OAI_X2AP_INCLUDE_DIRS})

message(STATUS oai::OAI_X2AP_ASN1C_INCLUDE_DIRS=
  ${OAI_X2AP_ASN1C_INSTALL_INCLUDE_DIR}.../asn1_constants.h
  " -OAI_X2AP_INCLUDE_DIRS-" ${OAI_X2AP_INCLUDE_DIRS}
  " --sources>=${OAI_X2AP_ASN1C_SOURCES}"
  " --sources>=${OAI_X2AP_ASN1C_SRCS}"
  )
if(NOT OAI_X2AP_ASN1C_SOURCES)
  message(FATAL_ERROR "empty sources, parent_scope DID NOT WORK")
endif()

#TODO: PTZ160821 
###also done in sub-directory, but does not seem to work in this one ? latter?!
## only X2AP_LIB  is exported
set_source_files_properties(
  ${OAI_X2AP_ASN1C_SRCS}
  PROPERTIES
  GENERATED true
  )


####RRC/S1AP/X2C common includes.
include_directories(
  ${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}
  ${OAI_S1AP_ASN1C_INSTALL_INCLUDE_DIR}
  ${OAI_X2AP_ASN1C_INSTALL_INCLUDE_DIR}
  )
get_directory_property(CONF_INCLUDE_DIRS INCLUDE_DIRECTORIES)

#
# include RF devices / transport protocols library modules
######################################################################
#
######???! 


add_subdirectory(${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lms7002m lms7002m)
add_subdirectory(${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR lmsSDR)
add_subdirectory(${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/Si5351C Si5351C)

include_directories("${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB")
include_directories ("${OPENAIR_TARGETS}/ARCH/EXMIMO/DEFS")
#set (option_HWEXMIMOLIB_lib "-l ")
set(HWLIB_EXMIMO_SOURCE 
  ${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB/openair0_lib.c
#  ${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB/gain_control.c
  )
add_library(oai_exmimodevif MODULE ${HWLIB_EXMIMO_SOURCE} )


###UHD USRP Hardware Driver (UHD™) Software
#TODO:PTZ170307 https://github.com/EttusResearch/uhd.git
check_include_file(uhd/utils/thread_priority.hpp HAVE_UHD)

if (HAVE_UHD-NOTFOUND)
  if (CYGWIN)
    set(_src_d ${PROJECT_SOURCE_DIR}/dependencies/uhd)
    #if debian... run apt-get install
    #  cmake -DENABLE_STATIC_LIBS=ON <path to UHD source>
    ExternalProject_Add(ep_uhd
      PREFIX ${PROJECT_BINARY_DIR}
      GIT_REPOSITORY https://github.com/EttusResearch/uhd.git
      #with submodule fpga-src
      SOURCE_DIR ${_src_d}
      UPDATE_DISCONNECTED 1
      CONFIGURE_COMMAND ${_src_d}/configure --prefix=${PROJECT_BINARY_DIR} --enable-shared
      BUILD_COMMAND make
      INSTALL_COMMAND make install
      BUILD_BYPRODUCTS libuhd.la uhd.h ${CMAKE_SHARED_LIBRARY_PREFIX}uhd-0${CMAKE_SHARED_LIBRARY_SUFFIX}
      STEP_TARGETS  install
      )
    ExternalProject_Add_Step(ep_uhd autoupdate
      DEPENDEES download
      COMMAND autoupdate --verbose
      WORKING_DIRECTORY ${_src_d}
      )
    ExternalProject_Add_Step(ep_uhd init
      DEPENDEES autoupdate
      COMMAND autoreconf --force --install --verbose --warnings=all --symlink
      COMMENT "asn1c OAI customised :: being autoreconfed"
      WORKING_DIRECTORY ${_src_d}
      BYPRODUCTS configure Makefile.in config.h.in
      )
    #TODO:PTZ170206 set HAVE_LIBUHD and relevant libraries
    #TODO:PTZ170210 installed in ${PROJECT_BINARY_DIR}/lib
    set(_lib_rdx uhd)
    externalproject_get_property(ep_uhd install_dir binary_dir)
    list(APPEND UHD_INCLUDEDIR "${install_dir}/include")
    list(APPEND UHD_LIBS
      ${_lib_rdx}
      #"${install_dir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}up${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    add_library(${_lib_rdx} STATIC IMPORTED)
    #add_library(${_lib_rdx} SHARED IMPORTED)
    set_property(TARGET ${_lib_rdx}
      PROPERTY
      IMPORTED_LOCATION
      "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}.dll${CMAKE_STATIC_LIBRARY_SUFFIX}"
      #"${binary_dir}/src/.libs/${CMAKE_SHARED_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_SHARED_LIBRARY_SUFFIX}"
      )
    message("${_lib_rdx} in ${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${_lib_rdx}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    add_dependencies(${_lib_rdx} ep_uhd)
    set(UHD_LIBRARIES  ${_lib_rdx})
    #set(UHD_LIBRARIES  /usr/src/openairinterface5g/bld/lib/lib${_lib_rdx}.a)
  endif (CYGWIN)
endif (HAVE_LIBUHD-NOTFOUND)
#(CYGWIN AND NOT "${HAVE_LIBSCTP}")



if (HAVE_UHD)
  include_directories("${OPENAIR_TARGETS}/ARCH/USRP/USERSPACE/LIB")
  set (option_HWUSRPLIB_lib "-l uhd")
  set(HWLIB_USRP_SOURCE 
    ${OPENAIR_TARGETS}/ARCH/USRP/USERSPACE/LIB/usrp_lib.cpp
    )
  add_library(oai_usrpdevif MODULE ${HWLIB_USRP_SOURCE} )

endif (HAVE_UHD)


#if (${RF_BOARD} STREQUAL "OAI_BLADERF")
# in libbladerf-dev
pkg_search_module(BLADERF libbladeRF)
check_include_file(libbladeRF.h  HAVE_BLADERF_H)

if (${RF_BOARD} STREQUAL "OAI_BLADERF" AND BLADERF-NOTFOUND)
  #libbladeRF.h?
  set(RF_BOARD NONE)
endif (${RF_BOARD} STREQUAL "OAI_BLADERF" AND BLADERF-NOTFOUND)

if (BLADERF-FOUND)

  include_directories(${BLADERF_INCLUDE_DIRS})
  include_directories("${OPENAIR_TARGETS}/ARCH/BLADERF/USERSPACE/LIB")
  set(option_HWBLADERFLIB_lib ${BLADERF_LIBRARIES})


  set(HWLIB_BLADERF_SOURCE 
    ${OPENAIR_TARGETS}/ARCH/BLADERF/USERSPACE/LIB/bladerf_lib.c
    )
  add_library(oai_bladerfdevif MODULE ${HWLIB_BLADERF_SOURCE})
  target_link_libraries(oai_bladerfdevif ${BLADERF_LIBRARIES})

  # (${RF_BOARD} STREQUAL "OAI_LMSSDR")
  ###LMSSDR
  include_directories("${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB"
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lms7002m
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/Si5351C
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/LMS_StreamBoard/
    )
  set (option_HWLMSSDRLIB_lib "-l LMS_SDR -l LMS7002M -l Si5351C")
  set(HWLIB_LMSSDR_SOURCE
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lms7002m/LMS7002M.h
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/Si5351C/Si5351C.h
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lms_lib.cpp
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/LMS_SDR.h
    ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/LMS_StreamBoard/LMS_StreamBoard.h
    )
  add_library(oai_lmssdrdevif MODULE ${HWLIB_LMSSDR_SOURCE})

endif (BLADERF-FOUND)

###TPLIB
include_directories("${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB")
set(TPLIB_ETHERNET_SOURCE 
  ${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB/ethernet_lib.c
  ${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB/eth_udp.c
  ${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB/eth_raw.c
  )
add_library(oai_eth_transpro MODULE ${TPLIB_ETHERNET_SOURCE} )

#
# RF devices / transport protocols settings
######################################################################
if (${RF_BOARD} STREQUAL "EXMIMO")
  set(DRIVER2013)
  include_directories ("${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB/")
  include_directories ("${OPENAIR_TARGETS}/ARCH/EXMIMO/DEFS/")
  set(HW_SOURCE ${HW_SOURCE}
    ${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB/openair0_lib.c)
  #    ${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB/gain_control.c)
  set(option_HW_lib "-rdynamic -ldl")

elseif (${RF_BOARD} STREQUAL "OAI_USRP")

  #TODO:PTZ170307 need uhd from...https://github.com/Nuand/bladeRF
  # uhd/utils/thread_priority.hpp #need HAVE_UHD
  
  include_directories("${OPENAIR_TARGETS}/ARCH/USRP/USERSPACE/LIB/")
  include_directories("/opt/include/uhd")
  set(HW_SOURCE ${HW_SOURCE}
    ${OPENAIR_TARGETS}/ARCH/USRP/USERSPACE/LIB/usrp_lib.cpp)
  LINK_DIRECTORIES("/opt/lib")
  set(option_HW_lib "-luhd -rdynamic -ldl")

elseif (${RF_BOARD} STREQUAL "OAI_BLADERF")
  include_directories("${OPENAIR_TARGETS}/ARCH/BLADERF/USERSPACE/LIB/")
  include_directories("${OPENAIR2_DIR}/UTIL/LOG")
  include_directories(${BLADERF_INCLUDE_DIRS})
  set(HW_SOURCE ${HW_SOURCE}
    ${OPENAIR_TARGETS}/ARCH/BLADERF/USERSPACE/LIB/bladerf_lib.c
    )
  LINK_DIRECTORIES("/usr/lib/x86_64-linux-gnu")
  set(option_HW_lib ${BLADERF_LIBRARIES} "-rdynamic -ldl")

elseif (${RF_BOARD} STREQUAL "OAI_LMSSDR")
  include_directories("${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB")
  include_directories("${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR")
  include_directories("${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lms7002m")
  include_directories("${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/Si5351C")
  include_directories("${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lmsSDR/LMS_StreamBoard")
  LINK_DIRECTORIES("/usr/lib/x86_64-linux-gnu")
  LINK_DIRECTORIES("${CMAKE_CURRENT_BINARY_DIR}/lmsSDR")
  LINK_DIRECTORIES("${CMAKE_CURRENT_BINARY_DIR}/lms7002m")
  LINK_DIRECTORIES("${CMAKE_CURRENT_BINARY_DIR}/Si5351C")
  set(HW_SOURCE ${HW_SOURCE} ${OPENAIR_TARGETS}/ARCH/LMSSDR/USERSPACE/LIB/lms_lib.cpp)
  set(option_HW_lib "-lLMS_SDR -lLMS7002M -lSi5351C -rdynamic -ldl")

elseif (${RF_BOARD} STREQUAL "CPRIGW")
  set(HW_SOURCE ${HW_SOURCE}
    ${OPENAIR_TARGETS}/ARCH/CPRIGW/USERSPACE/LIB/cprigw_lib.c
    )
  include_directories("${OPENAIR_TARGETS}/ARCH/CPRIGW/USERSPACE/LIB/")
  set(option_HW_lib "-rdynamic dl")
  
endif (${RF_BOARD} STREQUAL "EXMIMO")

#TODO PTZ170216 not yet useable in CYGWIN
if (${TRANSP_PRO} STREQUAL "ETHERNET")
  if (CYGWIN)
    #set(TRANSP_PRO NONE)
    message(WARNING "TRANSP_PRO is ETHERNET, but not yet supported under CYGWIN:"
      "ARCH/ETHERNET/USERSPACE/LIB is bypassed")
    
  else (CYGWIN)
  include_directories ("${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB")
  set(TRANSPORT_SOURCE ${TRANSPORT_SOURCE}
    ${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB/ethernet_lib.c
    ${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB/eth_udp.c
    ${OPENAIR_TARGETS}/ARCH/ETHERNET/USERSPACE/LIB/eth_raw.c
    )
  set(option_TP_lib "-rdynamic -ldl")
  endif (CYGWIN)
  
endif (${TRANSP_PRO} STREQUAL "ETHERNET")
##########################################################

include_directories ("${OPENAIR_TARGETS}/ARCH/COMMON")

Message("DEADLINE_SCHEDULER flag  is ${DEADLINE_SCHEDULER}")
Message("CPU_Affinity flag is ${CPU_AFFINITY}")


# Utilities Library
################
add_library(HASHTABLE SHARED
  ${OPENAIR_DIR}/common/utils/hashtable/hashtable.c
  ${OPENAIR_DIR}/common/utils/hashtable/obj_hashtable.c
)
include_directories(${OPENAIR_DIR}/common/utils/hashtable)

if (MESSAGE_CHART_GENERATOR)
  add_library(MSC  
    ${OPENAIR_DIR}/common/utils/msc/msc.c
  )  
  set(MSC_LIB MSC)
endif()
include_directories(${OPENAIR_DIR}/common/utils/msc)

set(UTIL_SRC
  #max_xface
  ${OPENAIR2_DIR}/LAYER2/MAC/extern.h

  ${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}/asn1_constants.h
  
  ${OPENAIR2_DIR}/UTIL/CLI/cli.c
  ${OPENAIR2_DIR}/UTIL/CLI/cli_cmd.c
  ${OPENAIR2_DIR}/UTIL/CLI/cli_server.c
  ${OPENAIR2_DIR}/UTIL/FIFO/pad_list.c
  ${OPENAIR2_DIR}/UTIL/LISTS/list.c
  ${OPENAIR2_DIR}/UTIL/LISTS/list2.c
  ${OPENAIR2_DIR}/UTIL/LOG/log.c
  ${OPENAIR2_DIR}/UTIL/LOG/vcd_signal_dumper.c
  ${OPENAIR2_DIR}/UTIL/MATH/oml.c
  ${OPENAIR2_DIR}/UTIL/MEM/mem_block.c
  ${OPENAIR2_DIR}/UTIL/OCG/OCG.c
  ${OPENAIR2_DIR}/UTIL/OCG/OCG_create_dir.c
  ${OPENAIR2_DIR}/UTIL/OCG/OCG_detect_file.c
  ${OPENAIR2_DIR}/UTIL/OCG/OCG_generate_report.c
  ${OPENAIR2_DIR}/UTIL/OCG/OCG_parse_filename.c
  ${OPENAIR2_DIR}/UTIL/OCG/OCG_parse_XML.c
  ${OPENAIR2_DIR}/UTIL/OCG/OCG_save_XML.c
  ${OPENAIR2_DIR}/UTIL/OMG/common.c
  ${OPENAIR2_DIR}/UTIL/OMG/grid.c
  ${OPENAIR2_DIR}/UTIL/OMG/job.c
  ${OPENAIR2_DIR}/UTIL/OMG/mobility_parser.c
  ${OPENAIR2_DIR}/UTIL/OMG/omg.c
  #${OPENAIR2_DIR}/UTIL/OMG/omg_hashtable.c
  ${OPENAIR2_DIR}/UTIL/OMG/rwalk.c
  ${OPENAIR2_DIR}/UTIL/OMG/rwp.c
  ${OPENAIR2_DIR}/UTIL/OMG/static.c
  ${OPENAIR2_DIR}/UTIL/OMG/steadystaterwp.c
  ${OPENAIR2_DIR}/UTIL/OMG/trace.c
  ${OPENAIR2_DIR}/UTIL/OMG/trace_hashtable.c
  ${OPENAIR2_DIR}/UTIL/OPT/probe.c
  ${OPENAIR2_DIR}/UTIL/OTG/otg_tx.c
  ${OPENAIR2_DIR}/UTIL/OTG/otg.c
  ${OPENAIR2_DIR}/UTIL/OTG/otg_kpi.c
  ${OPENAIR2_DIR}/UTIL/OTG/otg_models.c
  ${OPENAIR2_DIR}/UTIL/OTG/otg_form.c
  ${OPENAIR2_DIR}/UTIL/OTG/otg_rx.c
  ##TODO: PTZ160824quite needed
  #${OPENAIR2_DIR}/UTIL/OMG/sumo.c

  )

#PTZ170130 still required un shared, because of dependencies cycles below
add_library(UTIL ${UTIL_SRC})
#not done...#TODO: PTZ160824 changes my mind...
add_dependencies(UTIL rrc_srcs generate_T)
#add_dependencies(UTIL oai_rrc_regen)
set_target_properties(UTIL
  PROPERTIES
  LINKER_LANGUAGE C
  )
target_link_libraries(UTIL
  ${XFORMS_LIBRARIES}
  ${LIBXML2_LIBRARIES}
  SCHED_LIB
  HASHTABLE
  )

#set(OMG_SUMO_SRC
#  ${OPENAIR2_DIR}/UTIL/OMG/client_traci_OMG.c
#  ${OPENAIR2_DIR}/UTIL/OMG/id_manager.c
#  ${OPENAIR2_DIR}/UTIL/OMG/sumo.c
#  ${OPENAIR2_DIR}/UTIL/OMG/socket_traci_OMG.c
#  ${OPENAIR2_DIR}/UTIL/OMG/storage_traci_OMG.c
#  )
#add_library(OMG_SUMO ${OMG_SUMO_SRC})

set(SECU_OSA_SRC
  ${OPENAIR2_DIR}/UTIL/OSA/osa_key_deriver.c
  ${OPENAIR2_DIR}/UTIL/OSA/osa_rijndael.c
  ${OPENAIR2_DIR}/UTIL/OSA/osa_snow3g.c
  ${OPENAIR2_DIR}/UTIL/OSA/osa_stream_eea.c
  ${OPENAIR2_DIR}/UTIL/OSA/osa_stream_eia.c
  )
add_library(SECU_OSA SHARED ${SECU_OSA_SRC})
target_link_libraries(SECU_OSA
  ${NETTLE_LIBRARIES}
  ${OPENSSL_LIBRARIES}
  #PTZ170204 needed T_cache, freelist_head...
  ${ITTI_LIB}
)

set(SECU_CN_SRC
  ${OPENAIR3_DIR}/SECU/kdf.c
  ${OPENAIR3_DIR}/SECU/rijndael.c
  ${OPENAIR3_DIR}/SECU/snow3g.c
  ${OPENAIR3_DIR}/SECU/key_nas_deriver.c
  ${OPENAIR3_DIR}/SECU/nas_stream_eea1.c
  ${OPENAIR3_DIR}/SECU/nas_stream_eia1.c
  ${OPENAIR3_DIR}/SECU/nas_stream_eea2.c
  ${OPENAIR3_DIR}/SECU/nas_stream_eia2.c
  )
add_library(SECU_CN SHARED ${SECU_CN_SRC})

target_link_libraries(SECU_CN
  "${NETTLE_LIBRARIES}" #PTZ170306 nettle_ this and that
  ${OPENSSL_LIBRARIES} #needs CMAC_...
  )

# Scheduler
################################"
#PTZ161015 file(GLOB SCHED_SRC ${OPENAIR1_DIR}/SCHED/*.c)

set(SCHED_SRC 
  ${OPENAIR1_DIR}/SCHED/phy_procedures_lte_eNb.c
  ${OPENAIR1_DIR}/SCHED/phy_procedures_lte_ue.c
  ${OPENAIR1_DIR}/SCHED/phy_procedures_lte_common.c
  ${OPENAIR1_DIR}/SCHED/phy_mac_stub.c
  ${OPENAIR1_DIR}/SCHED/pucch_pc.c
  ${OPENAIR1_DIR}/SCHED/pusch_pc.c
  )

#PTZ170130 still required un shared, because of dependencies cycles below
#TODO: PTZ160922 ie. dlsch_coding set in PHY is required by SCHED_LIB,
# but loop since PHY depends of L2 ...UTIL...SCHED_LIB
add_library(SCHED_LIB
  ${SCHED_SRC}
  )
target_link_libraries(SCHED_LIB
  PHY
  )


# Layer 1
#############################
set(PHY_SRC
  # depend on code generation from asn1c
  ${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}/asn1_constants.h
  # actual source
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pss.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/sss.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pilots.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pilots_mbsfn.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dlsch_coding.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dlsch_modulation.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dlsch_demodulation.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dlsch_llr_computation.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/power_control.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dlsch_decoding.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dlsch_scrambling.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dci_tools.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/uci_tools.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/lte_mcs.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pbch.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/dci.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/phich.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pcfich.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pucch.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/prach.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pmch.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/pch.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/group_hopping.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/srs_modulation.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/drs_modulation.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/ulsch_modulation.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/ulsch_demodulation.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/ulsch_coding.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/ulsch_decoding.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/rar_tools.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/print_stats.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/initial_sync.c
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/if4_tools.c
  #${PROJECT_SOURCE_DIR}/targets/ARCH/ETHERNET/USERSPACE/LIB/if_defs.h
  ${OPENAIR1_DIR}/PHY/LTE_TRANSPORT/if5_tools.c
  ${OPENAIR1_DIR}/PHY/MODULATION/ofdm_mod.c
  ${OPENAIR1_DIR}/PHY/MODULATION/slot_fep.c
  ${OPENAIR1_DIR}/PHY/MODULATION/slot_fep_mbsfn.c
  ${OPENAIR1_DIR}/PHY/MODULATION/slot_fep_ul.c
  ${OPENAIR1_DIR}/PHY/MODULATION/ul_7_5_kHz.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/freq_equalization.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_sync_time.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_sync_timefreq.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_adjust_sync.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_dl_channel_estimation.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_dl_mbsfn_channel_estimation.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_ul_channel_estimation.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_est_freq_offset.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_ue_measurements.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/lte_eNB_measurements.c
  ${OPENAIR1_DIR}/PHY/LTE_ESTIMATION/adjust_gain.c
  ${OPENAIR1_DIR}/PHY/LTE_REFSIG/lte_dl_cell_spec.c
  ${OPENAIR1_DIR}/PHY/LTE_REFSIG/lte_dl_uespec.c
  ${OPENAIR1_DIR}/PHY/LTE_REFSIG/lte_gold.c
  ${OPENAIR1_DIR}/PHY/LTE_REFSIG/lte_gold_mbsfn.c
  ${OPENAIR1_DIR}/PHY/LTE_REFSIG/lte_dl_mbsfn.c
  ${OPENAIR1_DIR}/PHY/LTE_REFSIG/lte_ul_ref.c
  ${OPENAIR1_DIR}/PHY/CODING/lte_segmentation.c
  ${OPENAIR1_DIR}/PHY/CODING/ccoding_byte.c
  ${OPENAIR1_DIR}/PHY/CODING/ccoding_byte_lte.c
  ${OPENAIR1_DIR}/PHY/CODING/3gpplte_sse.c
  ${OPENAIR1_DIR}/PHY/CODING/crc_byte.c
  ${OPENAIR1_DIR}/PHY/CODING/3gpplte_turbo_decoder_sse_8bit.c
  ${OPENAIR1_DIR}/PHY/CODING/3gpplte_turbo_decoder_sse_16bit.c
  ${OPENAIR1_DIR}/PHY/CODING/3gpplte_turbo_decoder_avx2_16bit.c
  ${OPENAIR1_DIR}/PHY/CODING/lte_rate_matching.c
  ${OPENAIR1_DIR}/PHY/CODING/rate_matching.c
  ${OPENAIR1_DIR}/PHY/CODING/viterbi.c
  ${OPENAIR1_DIR}/PHY/CODING/viterbi_lte.c
  ${OPENAIR1_DIR}/PHY/INIT/lte_init.c
  ${OPENAIR1_DIR}/PHY/INIT/lte_parms.c
  ${OPENAIR1_DIR}/PHY/INIT/lte_param_init.c
  ${OPENAIR1_DIR}/PHY/TOOLS/file_output.c
  ${OPENAIR1_DIR}/PHY/TOOLS/lte_dfts.c
  ${OPENAIR1_DIR}/PHY/TOOLS/log2_approx.c
  ${OPENAIR1_DIR}/PHY/TOOLS/cmult_sv.c
  ${OPENAIR1_DIR}/PHY/TOOLS/cmult_vv.c
  ${OPENAIR1_DIR}/PHY/TOOLS/cdot_prod.c
  ${OPENAIR1_DIR}/PHY/TOOLS/signal_energy.c
  ${OPENAIR1_DIR}/PHY/TOOLS/dB_routines.c
  ${OPENAIR1_DIR}/PHY/TOOLS/sqrt.c
  ${OPENAIR1_DIR}/PHY/TOOLS/time_meas.c
  ${OPENAIR1_DIR}/PHY/TOOLS/lut.c


  #in L2 library?!
  #find_UE_id()
  #${OPENAIR2_DIR}/LAYER2/MAC/eNB_scheduler_primitives.c
  ${OPENAIR2_DIR}/LAYER2/MAC/proto.h

  #mac_UE_get_rrc_status mac_eNB_get_rrc_status
  #  ${OPENAIR2_DIR}/RRC/L2_INTERFACE/openair_rrc_L2_interface.c
  ${OPENAIR2_DIR}/RRC/L2_INTERFACE/openair_rrc_L2_interface.h
  )
if (${SMBV})
  set(PHY_SRC "${PHY_SRC} ${OPENAIR1_DIR}/PHY/TOOLS/smbv.c")
endif  (${SMBV})

if(0)
#set_source_files_properties( /usr/src/openairinterface5g/bld/share/oai/rrc/asn1_constants.h
#     PROPERTIES
#    GENERATED true
#    ) 
  set_source_files_properties(${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}/asn1_constants.h
    PROPERTIES
    GENERATED true
    )
# ok.!
  set_source_files_properties(${OAI_RRC_ASN1C_SOURCES}
    PROPERTIES
    GENERATED true
    )
 set_source_files_properties(${OAI_RRC_ASN1C_SRCS}
  PROPERTIES
  GENERATED true
  #DEPENDS ep_oai_s1ap_asn1c
  ) 
  #nok
endif()

#TODO: PTZ160825 has to be static because of extern rules really not done properly.
#  "SIMU" of type SHARED_LIBRARY
#    depends on "PHY" (weak)
#~~    depends on "L2" (weak)
#  "PHY" of type SHARED_LIBRARY
#    depends on "L2" (weak)
#    depends on "SIMU" (weak)
#  "L2" of type SHARED_LIBRARY
#    depends on "SIMU" (weak)
#    depends on "PHY" (weak)

add_library(PHY #SHARED
  ${PHY_SRC}
  )
#TODO: PTZ160824
if(1)
  add_dependencies(PHY rrc_srcs)
else()
add_dependencies(PHY oai_rrc_regen)
#add_dependencies(UTIL oai_rrc_regen)
endif()

if (CYGWIN)
  #PTZ170218 needed to find a pseudo netinet/if_ether.h 
  add_dependencies(PHY
    ep_tcptrace)
  
endif (CYGWIN)

set_target_properties(PHY
  PROPERTIES
  LINKER_LANGUAGE CXX
  )

#PTZ170316 L2 not needed in dlsim... actually is...
#target_link_libraries(PHY L2)

target_link_libraries(PHY SIMU) #PTZ170322 
#libPHY.a(pbch.c.o)Â : Dans la fonction Â«Â rx_pbch_emulÂ Â»Â :
#/usr/src/openairinterface5g/openair1/PHY/LTE_TRANSPORT/pbch.c:1061Â : rÃ©fÃ©rence indÃ©finie vers Â«Â pbch_blerÂ Â»
#/usr/src/openairinterface5g/openair1/PHY/LTE_TRANSPORT/pbch.c:1070Â : rÃ©fÃ©rence indÃ©finie vers Â«Â uniformrandomÂ Â»


#Layer 2 library
#####################
set(MAC_DIR ${OPENAIR2_DIR}/LAYER2/MAC)
set(RLC_DIR ${OPENAIR2_DIR}/LAYER2/RLC)
set(RLC_UM_DIR ${OPENAIR2_DIR}/LAYER2/RLC/UM_v9.3.0)
set(RLC_AM_DIR ${OPENAIR2_DIR}/LAYER2/RLC/AM_v9.3.0)
set(RLC_TM_DIR ${OPENAIR2_DIR}/LAYER2/RLC/TM_v9.3.0)
set(RRC_DIR ${OPENAIR2_DIR}/RRC/LITE)
set(PDCP_DIR  ${OPENAIR2_DIR}/LAYER2/PDCP_v10.1.0)
set(L2_SRC
  ${OPENAIR2_DIR}/LAYER2/openair2_proc.c
  ${PDCP_DIR}/pdcp.c
  ${PDCP_DIR}/pdcp_fifo.c
  ${PDCP_DIR}/pdcp_sequence_manager.c
  ${PDCP_DIR}/pdcp_primitives.c
  ${PDCP_DIR}/pdcp_util.c
  ${PDCP_DIR}/pdcp_security.c
  ${PDCP_DIR}/pdcp_netlink.c
  ${RLC_AM_DIR}/rlc_am.c
  ${RLC_AM_DIR}/rlc_am_init.c
  ${RLC_AM_DIR}/rlc_am_timer_poll_retransmit.c
  ${RLC_AM_DIR}/rlc_am_timer_reordering.c
  ${RLC_AM_DIR}/rlc_am_timer_status_prohibit.c
  ${RLC_AM_DIR}/rlc_am_segment.c
  ${RLC_AM_DIR}/rlc_am_segments_holes.c
  ${RLC_AM_DIR}/rlc_am_in_sdu.c
  ${RLC_AM_DIR}/rlc_am_receiver.c
  ${RLC_AM_DIR}/rlc_am_retransmit.c
  ${RLC_AM_DIR}/rlc_am_windows.c
  ${RLC_AM_DIR}/rlc_am_rx_list.c
  ${RLC_AM_DIR}/rlc_am_reassembly.c
  ${RLC_AM_DIR}/rlc_am_status_report.c
  ${RLC_TM_DIR}/rlc_tm.c
  ${RLC_TM_DIR}/rlc_tm_init.c
  ${RLC_UM_DIR}/rlc_um.c
  ${RLC_UM_DIR}/rlc_um_fsm.c
  ${RLC_UM_DIR}/rlc_um_control_primitives.c
  ${RLC_UM_DIR}/rlc_um_segment.c
  ${RLC_UM_DIR}/rlc_um_reassembly.c
  ${RLC_UM_DIR}/rlc_um_receiver.c
  ${RLC_UM_DIR}/rlc_um_dar.c
  ${RLC_DIR}/rlc_mac.c
  ${RLC_DIR}/rlc.c
  ${RLC_DIR}/rlc_rrc.c
  ${RLC_DIR}/rlc_mpls.c
  ${RRC_DIR}/rrc_UE.c
  ${RRC_DIR}/rrc_eNB.c
  ${RRC_DIR}/rrc_eNB_S1AP.c
  ${RRC_DIR}/rrc_eNB_UE_context.c
  ${RRC_DIR}/rrc_common.c
  ${RRC_DIR}/L2_interface.c
  )
set (MAC_SRC
  ${MAC_DIR}/lte_transport_init.c
  ${MAC_DIR}/main.c
  ${MAC_DIR}/ue_procedures.c
  ${MAC_DIR}/ra_procedures.c
  ${MAC_DIR}/l1_helpers.c
  ${MAC_DIR}/rar_tools.c
  ${MAC_DIR}/eNB_scheduler.c
  ${MAC_DIR}/eNB_scheduler_dlsch.c
  ${MAC_DIR}/eNB_scheduler_ulsch.c
  ${MAC_DIR}/eNB_scheduler_mch.c
  ${MAC_DIR}/eNB_scheduler_bch.c
  ${MAC_DIR}/eNB_scheduler_primitives.c
  ${MAC_DIR}/eNB_scheduler_RA.c
  ${MAC_DIR}/pre_processor.c
  ${MAC_DIR}/config.c

  
  ${MAC_DIR}/vars.h

  )

set (ENB_APP_SRC
  ${OPENAIR2_DIR}/ENB_APP/enb_app.c
  ${OPENAIR2_DIR}/ENB_APP/enb_config.c
  )

add_library(L2 #SHARED
  ${L2_SRC}
  ${MAC_SRC}
  ${ENB_APP_SRC}
  )
#  ${OPENAIR2_DIR}/RRC/L2_INTERFACE/openair_rrc_L2_interface.c)
target_link_libraries(L2
  # "RRC_LIB" that is not in the export set. ${OAI_RRC_LIBRARIES}
  UTIL
  #SCHED_LIB->PHY
  #PTZ170210 not sure but RRC_LIB cannot be found ... ${OAI_RRC_LIBRARIES}
  #SIMU #->blas, phy
  SECU_OSA
  #HASHTABLE
  ${OAI_S1AP_LIBRARIES} #if(ENABLE_USE_MME) need  s1ap_generate_eNB_id !
  GTPV1U

  #PTZ170317 really directly needed... check other cycles...?
  # really needed I put it back, but cannot:
  # CMake Error: install(EXPORT "openairinterface-targets" ...) includes target "L2" which requires target "RRC_LIB" that is not in the export set.
  # ${OAI_RRC_LIBRARIES}

  ${LIBCONFIG_LIBRARIES}
  SIMU_ETH #->util
  )

# L3 Libs
##########################

set(RAL_LTE_DIR ${OPENAIR3_DIR}/RAL-LTE/)
if (${ENABLE_RAL})
  set(RAL_LTE_SRC
    ${RRC_DIR}/rrc_UE_ral.c
    ${RRC_DIR}/rrc_eNB_ral.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_action.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_main.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_mih_msg.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_parameters.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_process.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_rrc_msg.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_subscribe.c
    ${RAL_LTE_DIR}LTE_RAL_ENB/SRC/lteRALenb_thresholds.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_action.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_main.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_mih_msg.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_parameters.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_process.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_rrc_msg.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_subscribe.c
    ${RAL_LTE_DIR}LTE_RAL_UE/SRC/lteRALue_thresholds.c
    )
  add_library(RAL ${RAL_LTE_SRC})
  set(RAL_LIB RAL)
endif()

if(${MIH_C_MEDIEVAL_EXTENSIONS})
  set(MIH_SRC
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_header_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_msg_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_primitive_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_F1_basic_data_types_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_F2_general_data_types_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_F3_data_types_for_address_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_F4_data_types_for_links_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_F9_data_types_for_qos_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_F13_data_types_for_information_elements_codec.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_L2_type_values_for_tlv_encoding.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_Medieval_extensions.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C_bit_buffer.c
    ${RAL_LTE_DIR}INTERFACE-802.21/C/MIH_C.c
    )
  add_library(MIH ${MIH_SRC})
  set(MIH_LIB MIH)
endif()

# CN libs
##########################

add_library(CN_UTILS
  ${OPENAIR3_DIR}/UTILS/conversions.c
  ${OPENAIR3_DIR}/UTILS/enum_string.c
  ${OPENAIR3_DIR}/UTILS/log.c
  ${OPENAIR3_DIR}/UTILS/mcc_mnc_itu.c
  )

set(GTPV1U_DIR ${OPENAIR3_DIR}/GTPV1-U)
set (GTPV1U_SRC
  ${RRC_DIR}/rrc_eNB_GTPV1U.c
  ${GTPV1U_DIR}/nw-gtpv1u/src/NwGtpv1uTunnelEndPoint.c
  ${GTPV1U_DIR}/nw-gtpv1u/src/NwGtpv1uTrxn.c
  ${GTPV1U_DIR}/nw-gtpv1u/src/NwGtpv1uMsg.c
  ${GTPV1U_DIR}/nw-gtpv1u/src/NwGtpv1u.c
  ${GTPV1U_DIR}/gtpv1u_teid_pool.c
)
add_library(GTPV1U #PTZ170210 SHARED cannot since lots of cycles RRC_LIB-L2-GTPV1VU
  ${GTPV1U_SRC}
  )
target_link_libraries(GTPV1U
  ${ITTI_LIB} #PTZ170306 need T_cache T_freelist_head T_active itti_malloc
  # ${OAI_RRC_LIBRARIES}   #PTZ170206 need rrc_eNB_get_ue_context eNB_rrc_inst
#CMakeFiles/nvram.dir/usr/src/openairinterface5g/openair3/GTPV1-U/gtpv1u_eNB.c.oÂ : Dans la fonction Â«Â gtpv1u_eNB_process_stack_reqÂ Â»Â :
#/usr/src/openairinterface5g/openair3/GTPV1-U/gtpv1u_eNB.c:341Â : rÃ©fÃ©rence indÃ©finie vers Â«Â pdcp_data_reqÂ Â»
#CMakeFiles/nvram.dir/usr/src/openairinterface5g/openair3/GTPV1-U/gtpv1u_eNB.c.oÂ : Dans la fonction Â«Â gtpv1u_eNB_initÂ Â»Â :
#/usr/src/openairinterface5g/openair3/GTPV1-U/gtpv1u_eNB.c:910Â : rÃ©fÃ©rence indÃ©finie vers Â«Â enb_config_getÂ Â»
  SIMU
  )

set(SCTP_SRC
  ${OPENAIR3_DIR}/SCTP/sctp_common.c
  ${OPENAIR3_DIR}/SCTP/sctp_eNB_task.c
  ${OPENAIR3_DIR}/SCTP/sctp_eNB_itti_messaging.c
)
add_library(SCTP_CLIENT SHARED ${SCTP_SRC})
target_link_libraries(SCTP_CLIENT ${SCTP_LIBRARIES})

add_library(UDP SHARED ${OPENAIR3_DIR}/UDP/udp_eNB_task.c)
target_link_libraries(UDP
  ${ITTI_LIB} #PTZ170306 need T_cache T_freelist_head T_active itti_malloc
  )

set(NAS_SRC ${OPENAIR3_DIR}/NAS/)
set(libnas_api_OBJS
  ${NAS_SRC}COMMON/API/NETWORK/as_message.c
  ${NAS_SRC}COMMON/API/NETWORK/nas_message.c
  ${NAS_SRC}COMMON/API/NETWORK/network_api.c
  )
  
set(libnas_emm_msg_OBJS
  ${NAS_SRC}COMMON/EMM/MSG/AttachAccept.c
  ${NAS_SRC}COMMON/EMM/MSG/AttachComplete.c
  ${NAS_SRC}COMMON/EMM/MSG/AttachReject.c
  ${NAS_SRC}COMMON/EMM/MSG/AttachRequest.c
  ${NAS_SRC}COMMON/EMM/MSG/AuthenticationFailure.c
  ${NAS_SRC}COMMON/EMM/MSG/AuthenticationReject.c
  ${NAS_SRC}COMMON/EMM/MSG/AuthenticationRequest.c
  ${NAS_SRC}COMMON/EMM/MSG/AuthenticationResponse.c
  ${NAS_SRC}COMMON/EMM/MSG/CsServiceNotification.c
  ${NAS_SRC}COMMON/EMM/MSG/DetachAccept.c
  ${NAS_SRC}COMMON/EMM/MSG/DetachRequest.c
  ${NAS_SRC}COMMON/EMM/MSG/DownlinkNasTransport.c
  ${NAS_SRC}COMMON/EMM/MSG/EmmInformation.c
  ${NAS_SRC}COMMON/EMM/MSG/emm_msg.c
  ${NAS_SRC}COMMON/EMM/MSG/EmmStatus.c
  ${NAS_SRC}COMMON/EMM/MSG/ExtendedServiceRequest.c
  ${NAS_SRC}COMMON/EMM/MSG/GutiReallocationCommand.c
  ${NAS_SRC}COMMON/EMM/MSG/GutiReallocationComplete.c
  ${NAS_SRC}COMMON/EMM/MSG/IdentityRequest.c
  ${NAS_SRC}COMMON/EMM/MSG/IdentityResponse.c
  ${NAS_SRC}COMMON/EMM/MSG/SecurityModeCommand.c
  ${NAS_SRC}COMMON/EMM/MSG/SecurityModeComplete.c
  ${NAS_SRC}COMMON/EMM/MSG/SecurityModeReject.c
  ${NAS_SRC}COMMON/EMM/MSG/ServiceReject.c
  ${NAS_SRC}COMMON/EMM/MSG/ServiceRequest.c
  ${NAS_SRC}COMMON/EMM/MSG/TrackingAreaUpdateAccept.c
  ${NAS_SRC}COMMON/EMM/MSG/TrackingAreaUpdateComplete.c
  ${NAS_SRC}COMMON/EMM/MSG/TrackingAreaUpdateReject.c
  ${NAS_SRC}COMMON/EMM/MSG/TrackingAreaUpdateRequest.c
  ${NAS_SRC}COMMON/EMM/MSG/UplinkNasTransport.c
)
  
set(libnas_esm_msg_OBJS
  ${NAS_SRC}COMMON/ESM/MSG/ActivateDedicatedEpsBearerContextAccept.c
  ${NAS_SRC}COMMON/ESM/MSG/ActivateDedicatedEpsBearerContextReject.c
  ${NAS_SRC}COMMON/ESM/MSG/ActivateDedicatedEpsBearerContextRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/ActivateDefaultEpsBearerContextAccept.c
  ${NAS_SRC}COMMON/ESM/MSG/ActivateDefaultEpsBearerContextReject.c
  ${NAS_SRC}COMMON/ESM/MSG/ActivateDefaultEpsBearerContextRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/BearerResourceAllocationReject.c
  ${NAS_SRC}COMMON/ESM/MSG/BearerResourceAllocationRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/BearerResourceModificationReject.c
  ${NAS_SRC}COMMON/ESM/MSG/BearerResourceModificationRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/DeactivateEpsBearerContextAccept.c
  ${NAS_SRC}COMMON/ESM/MSG/DeactivateEpsBearerContextRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/EsmInformationRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/EsmInformationResponse.c
  ${NAS_SRC}COMMON/ESM/MSG/esm_msg.c
  ${NAS_SRC}COMMON/ESM/MSG/EsmStatus.c
  ${NAS_SRC}COMMON/ESM/MSG/ModifyEpsBearerContextAccept.c
  ${NAS_SRC}COMMON/ESM/MSG/ModifyEpsBearerContextReject.c
  ${NAS_SRC}COMMON/ESM/MSG/ModifyEpsBearerContextRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/PdnConnectivityReject.c
  ${NAS_SRC}COMMON/ESM/MSG/PdnConnectivityRequest.c
  ${NAS_SRC}COMMON/ESM/MSG/PdnDisconnectReject.c
  ${NAS_SRC}COMMON/ESM/MSG/PdnDisconnectRequest.c
)

set(libnas_ies_OBJS
  ${NAS_SRC}COMMON/IES/AccessPointName.c
  ${NAS_SRC}COMMON/IES/AdditionalUpdateResult.c
  ${NAS_SRC}COMMON/IES/AdditionalUpdateType.c
  ${NAS_SRC}COMMON/IES/ApnAggregateMaximumBitRate.c
  ${NAS_SRC}COMMON/IES/AuthenticationFailureParameter.c
  ${NAS_SRC}COMMON/IES/AuthenticationParameterAutn.c
  ${NAS_SRC}COMMON/IES/AuthenticationParameterRand.c
  ${NAS_SRC}COMMON/IES/AuthenticationResponseParameter.c
  ${NAS_SRC}COMMON/IES/CipheringKeySequenceNumber.c
  ${NAS_SRC}COMMON/IES/Cli.c
  ${NAS_SRC}COMMON/IES/CsfbResponse.c
  ${NAS_SRC}COMMON/IES/DaylightSavingTime.c
  ${NAS_SRC}COMMON/IES/DetachType.c
  ${NAS_SRC}COMMON/IES/DrxParameter.c
  ${NAS_SRC}COMMON/IES/EmergencyNumberList.c
  ${NAS_SRC}COMMON/IES/EmmCause.c
  ${NAS_SRC}COMMON/IES/EpsAttachResult.c
  ${NAS_SRC}COMMON/IES/EpsAttachType.c
  ${NAS_SRC}COMMON/IES/EpsBearerContextStatus.c
  ${NAS_SRC}COMMON/IES/EpsBearerIdentity.c
  ${NAS_SRC}COMMON/IES/EpsMobileIdentity.c
  ${NAS_SRC}COMMON/IES/EpsNetworkFeatureSupport.c
  ${NAS_SRC}COMMON/IES/EpsQualityOfService.c
  ${NAS_SRC}COMMON/IES/EpsUpdateResult.c
  ${NAS_SRC}COMMON/IES/EpsUpdateType.c
  ${NAS_SRC}COMMON/IES/EsmCause.c
  ${NAS_SRC}COMMON/IES/EsmInformationTransferFlag.c
  ${NAS_SRC}COMMON/IES/EsmMessageContainer.c
  ${NAS_SRC}COMMON/IES/GprsTimer.c
  ${NAS_SRC}COMMON/IES/GutiType.c
  ${NAS_SRC}COMMON/IES/IdentityType2.c
  ${NAS_SRC}COMMON/IES/ImeisvRequest.c
  ${NAS_SRC}COMMON/IES/KsiAndSequenceNumber.c
  ${NAS_SRC}COMMON/IES/LcsClientIdentity.c
  ${NAS_SRC}COMMON/IES/LcsIndicator.c
  ${NAS_SRC}COMMON/IES/LinkedEpsBearerIdentity.c
  ${NAS_SRC}COMMON/IES/LlcServiceAccessPointIdentifier.c
  ${NAS_SRC}COMMON/IES/LocationAreaIdentification.c
  ${NAS_SRC}COMMON/IES/MessageType.c
  ${NAS_SRC}COMMON/IES/MobileIdentity.c
  ${NAS_SRC}COMMON/IES/MobileStationClassmark2.c
  ${NAS_SRC}COMMON/IES/MobileStationClassmark3.c
  ${NAS_SRC}COMMON/IES/MsNetworkCapability.c
  ${NAS_SRC}COMMON/IES/MsNetworkFeatureSupport.c
  ${NAS_SRC}COMMON/IES/NasKeySetIdentifier.c
  ${NAS_SRC}COMMON/IES/NasMessageContainer.c
  ${NAS_SRC}COMMON/IES/NasRequestType.c
  ${NAS_SRC}COMMON/IES/NasSecurityAlgorithms.c
  ${NAS_SRC}COMMON/IES/NetworkName.c
  ${NAS_SRC}COMMON/IES/Nonce.c
  ${NAS_SRC}COMMON/IES/PacketFlowIdentifier.c
  ${NAS_SRC}COMMON/IES/PagingIdentity.c
  ${NAS_SRC}COMMON/IES/PdnAddress.c
  ${NAS_SRC}COMMON/IES/PdnType.c
  ${NAS_SRC}COMMON/IES/PlmnList.c
  ${NAS_SRC}COMMON/IES/ProcedureTransactionIdentity.c
  ${NAS_SRC}COMMON/IES/ProtocolConfigurationOptions.c
  ${NAS_SRC}COMMON/IES/ProtocolDiscriminator.c
  ${NAS_SRC}COMMON/IES/PTmsiSignature.c
  ${NAS_SRC}COMMON/IES/QualityOfService.c
  ${NAS_SRC}COMMON/IES/RadioPriority.c
  ${NAS_SRC}COMMON/IES/SecurityHeaderType.c
  ${NAS_SRC}COMMON/IES/ServiceType.c
  ${NAS_SRC}COMMON/IES/ShortMac.c
  ${NAS_SRC}COMMON/IES/SsCode.c
  ${NAS_SRC}COMMON/IES/SupportedCodecList.c
  ${NAS_SRC}COMMON/IES/TimeZoneAndTime.c
  ${NAS_SRC}COMMON/IES/TimeZone.c
  ${NAS_SRC}COMMON/IES/TmsiStatus.c
  ${NAS_SRC}COMMON/IES/TrackingAreaIdentity.c
  ${NAS_SRC}COMMON/IES/TrackingAreaIdentityList.c
  ${NAS_SRC}COMMON/IES/TrafficFlowAggregateDescription.c
  ${NAS_SRC}COMMON/IES/TrafficFlowTemplate.c
  ${NAS_SRC}COMMON/IES/TransactionIdentifier.c
  ${NAS_SRC}COMMON/IES/UeNetworkCapability.c
  ${NAS_SRC}COMMON/IES/UeRadioCapabilityInformationUpdateNeeded.c
  ${NAS_SRC}COMMON/IES/UeSecurityCapability.c
  ${NAS_SRC}COMMON/IES/VoiceDomainPreferenceAndUeUsageSetting.c
)

set (libnas_utils_OBJS
  ${NAS_SRC}COMMON/UTIL/device.c
  ${NAS_SRC}COMMON/UTIL/memory.c
#PTZ161016 T_active,... needed by memory...
  ${T_SOURCE}
  
  ${NAS_SRC}COMMON/UTIL/nas_log.c
  ${NAS_SRC}COMMON/UTIL/nas_timer.c
  ${NAS_SRC}COMMON/UTIL/socket.c
  ${NAS_SRC}COMMON/UTIL/stty.c
  ${NAS_SRC}COMMON/UTIL/TLVEncoder.c
  ${NAS_SRC}COMMON/UTIL/TLVDecoder.c
  ${NAS_SRC}COMMON/UTIL/OctetString.c
)

if(NAS_UE) 
  set(libnas_ue_api_OBJS
    ${NAS_SRC}UE/API/USER/at_command.c
    ${NAS_SRC}UE/API/USER/at_error.c
    ${NAS_SRC}UE/API/USER/at_response.c
    ${NAS_SRC}UE/API/USER/user_api.c
    ${NAS_SRC}UE/API/USER/user_indication.c
    ${NAS_SRC}UE/API/USIM/aka_functions.c
    ${NAS_SRC}UE/API/USIM/usim_api.c
  )
  set(libnas_ue_emm_OBJS
    ${NAS_SRC}UE/EMM/Attach.c
    ${NAS_SRC}UE/EMM/Authentication.c
    ${NAS_SRC}UE/EMM/Detach.c
    ${NAS_SRC}UE/EMM/emm_main.c
    ${NAS_SRC}UE/EMM/EmmStatusHdl.c
    ${NAS_SRC}UE/EMM/Identification.c
    ${NAS_SRC}UE/EMM/IdleMode.c
    ${NAS_SRC}UE/EMM/LowerLayer.c
    ${NAS_SRC}UE/EMM/SecurityModeControl.c
    ${NAS_SRC}UE/EMM/ServiceRequestHdl.c
    ${NAS_SRC}UE/EMM/TrackingAreaUpdate.c
  )
  set(libnas_ue_emm_sap_OBJS
    ${NAS_SRC}UE/EMM/SAP/emm_as.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredAttachNeeded.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredAttemptingToAttach.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregistered.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredInitiated.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredLimitedService.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredNoCellAvailable.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredNoImsi.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredNormalService.c
    ${NAS_SRC}UE/EMM/SAP/EmmDeregisteredPlmnSearch.c
    ${NAS_SRC}UE/EMM/SAP/emm_esm.c
    ${NAS_SRC}UE/EMM/SAP/emm_fsm.c
    ${NAS_SRC}UE/EMM/SAP/EmmNull.c
    ${NAS_SRC}UE/EMM/SAP/emm_recv.c
    ${NAS_SRC}UE/EMM/SAP/emm_reg.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredAttemptingToUpdate.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegistered.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredImsiDetachInitiated.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredInitiated.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredLimitedService.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredNoCellAvailable.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredNormalService.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredPlmnSearch.c
    ${NAS_SRC}UE/EMM/SAP/EmmRegisteredUpdateNeeded.c
    ${NAS_SRC}UE/EMM/SAP/emm_sap.c
    ${NAS_SRC}UE/EMM/SAP/emm_send.c
    ${NAS_SRC}UE/EMM/SAP/EmmServiceRequestInitiated.c
    ${NAS_SRC}UE/EMM/SAP/EmmTrackingAreaUpdatingInitiated.c
  )
  set (libnas_ue_esm_OBJS
    ${NAS_SRC}UE/ESM/DedicatedEpsBearerContextActivation.c
    ${NAS_SRC}UE/ESM/DefaultEpsBearerContextActivation.c
    ${NAS_SRC}UE/ESM/EpsBearerContextDeactivation.c
    ${NAS_SRC}UE/ESM/esm_ebr.c
    ${NAS_SRC}UE/ESM/esm_ebr_context.c
    ${NAS_SRC}UE/ESM/esm_ip.c
    ${NAS_SRC}UE/ESM/esm_main.c
    ${NAS_SRC}UE/ESM/esm_pt.c
    ${NAS_SRC}UE/ESM/EsmStatusHdl.c
    ${NAS_SRC}UE/ESM/PdnConnectivity.c
    ${NAS_SRC}UE/ESM/PdnDisconnect.c
  )
  set(libnas_ue_esm_sap_OBJS
    ${NAS_SRC}UE/ESM/SAP/esm_recv.c
    ${NAS_SRC}UE/ESM/SAP/esm_send.c
    ${NAS_SRC}UE/ESM/SAP/esm_sap.c
  )
  add_library(LIB_NAS_UE
    ${NAS_SRC}UE/nas_itti_messaging.c
    ${NAS_SRC}UE/nas_network.c
    ${NAS_SRC}UE/nas_parser.c
    ${NAS_SRC}UE/nas_proc.c
    ${NAS_SRC}UE/nas_user.c
    ${libnas_api_OBJS}
    ${libnas_ue_api_OBJS}
    ${libnas_emm_msg_OBJS}
    ${libnas_esm_msg_OBJS}
    ${libnas_ies_OBJS}
    ${libnas_utils_OBJS}
    ${libnas_ue_emm_OBJS}
    ${libnas_ue_emm_sap_OBJS}
    ${libnas_ue_esm_OBJS}
    ${libnas_ue_esm_sap_OBJS}
    )
  add_dependencies(LIB_NAS_UE generate_T)
  set(NAS_UE_LIB LIB_NAS_UE)

  include_directories(${NAS_SRC}UE)
  include_directories(${NAS_SRC}UE/API/USER)
  include_directories(${NAS_SRC}UE/API/USIM)
  include_directories(${NAS_SRC}UE/EMM)
  include_directories(${NAS_SRC}UE/EMM/SAP)
  include_directories(${NAS_SRC}UE/ESM)
  include_directories(${NAS_SRC}UE/ESM/SAP)
endif()



# Simulation library
##########################
add_library(SIMU #STATIC
  ${OPENAIR1_DIR}/SIMULATION/TOOLS/random_channel.c
  ${OPENAIR1_DIR}/SIMULATION/TOOLS/rangen_double.c
  ${OPENAIR1_DIR}/SIMULATION/TOOLS/taus.c
  ${OPENAIR1_DIR}/SIMULATION/TOOLS/multipath_channel.c
  ${OPENAIR1_DIR}/SIMULATION/TOOLS/abstraction.c
  ${OPENAIR1_DIR}/SIMULATION/TOOLS/multipath_tv_channel.c
  ${OPENAIR1_DIR}/SIMULATION/RF/rf.c
  ${OPENAIR1_DIR}/SIMULATION/RF/dac.c
  ${OPENAIR1_DIR}/SIMULATION/RF/adc.c
)
target_link_libraries(SIMU
  ${BLAS_LIBRARIES}
  #PTZ170129 needs PHY signal_energy
  #PTZ170317-->PHY conflict by requesting MAC in dlsim...
  )
add_dependencies(SIMU generate_T rrc_srcs)

#PTZ170210 has to be static because cyclics L2,UTIL, SCHED PHY SIMU SIMU_ETH.
add_library(SIMU_ETH STATIC
${OPENAIR1_DIR}/SIMULATION/ETH_TRANSPORT/netlink_init.c
${OPENAIR1_DIR}/SIMULATION/ETH_TRANSPORT/multicast_link.c
${OPENAIR1_DIR}/SIMULATION/ETH_TRANSPORT/socket.c
${OPENAIR1_DIR}/SIMULATION/ETH_TRANSPORT/bypass_session_layer.c
${OPENAIR1_DIR}/SIMULATION/ETH_TRANSPORT/emu_transport.c
${OPENAIR1_DIR}/SIMULATION/ETH_TRANSPORT/pgm_link.c
#TODO PTZ170209 
${T_SOURCE}
)
target_link_libraries(SIMU_ETH
  ${OPENPGM_LIBRARIES}
  #util_log
  UTIL
  )
add_library(OPENAIR0_LIB SHARED
  ${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB/openair0_lib.c
)

#################################
# add executables for operation
#################################

# lte-softmodem is both eNB and UE implementation
###################################################
add_subdirectory(targets/RT)

# lte-softmodem-nos1 is both eNB and UE implementation
###################################################
add_subdirectory(cmake_targets/lte_noS1_build_oai)


# USIM process
#################
#
#  NAS_SIM_TOOLS
#
add_subdirectory(cmake_targets/nas_sim_tools)


###################################"
# Addexecutables for tests
####################################
##TODO:PTZ161002  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets/oaisim_build_oai)
##TODO:PTZ161002  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets/oaisim_mme_build_oai)
##TODO:PTZ161002  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets/oaisim_noS1_build_oai)
##TODO:PTZ161002  add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/cmake_targets/s1c_mme_test)


# A all in one network simulator
################
add_subdirectory(cmake_targets/oaisim_build_oai)
add_subdirectory(cmake_targets/oaisim_noS1_build_oai)



# Unitary tests for each piece of L1: example, mbmssim is MBMS L1 simulator
#####################################

foreach(myExe dlsim ulsim pbchsim scansim mbmssim pdcchsim pucchsim prachsim syncsim)
  add_executable(${myExe}
    ${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}/asn1_constants.h
    ${PROJECT_BINARY_DIR}/messages_xml.h
#    ${OPENAIR_BIN_DIR}/messages_xml.h
    ${OPENAIR1_DIR}/SIMULATION/LTE_PHY/${myExe}.c
    ${XFORMS_SOURCE}
    ${T_SOURCE}
    
    ${GTPU_need_ITTI}    
    )
  target_link_libraries(${myExe}
    #-Wl,--start-group
    #PTZ170316 ${OAI_RRC_LIBRARIES} #ie. needs do_RRCConnectionRequest, also dups get_dci_sdu ?
    SIMU
    #SCHED_LIB
    PHY  ##PTZ170316 PHY_ofdm_mod needed by dlsim
    UTIL ##PTZ170316 logInit ... in UTIL
      ##PTZ170316
      #GTPU_need_ITTI sources  gtpv1u_eNB.c needs nwGtpv1uMsgGetTpdu in NwGtpv1uMsg.c. GTPV1U
      # which could have been in ITTI_LIB also
        GTPV1U
        ## pdcp.c pdcp_data_req is also requested ? ${L2_SRC} in L2 then....
        ## enb_config.c for enb_config_get ${ENB_APP_SRC} in L2
        L2 #===>asn1_msg.c RRC_LITE
          ${OAI_RRC_LIBRARIES} #PTZ170328 really sorts out the lot.
        
    LFDS ${ITTI_LIB}
    #-Wl,--end-group
    Threads::Threads
    #pthread
    m
    ${LIBCONFIG_LIBRARIES}
    ${ATLAS_LIBRARIES}
    ${XFORMS_LIBRARIES}
    ${T_LIB}
    )
  set_target_properties(${myExe}
    PROPERTIES
    EXPORT openairinterface-targets
    LINKER_LANGUAGE C
    )
  #if (${T_TRACER})
  add_dependencies(${myExe} generate_T)
  #endif (${T_TRACER})
endforeach(myExe)

#add_dependencies(T_IDs.h generate_T)

add_subdirectory(cmake_targets/epc_test)

#unitary tests for Core NEtwork pieces
#################################
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/targets/TEST/PDCP)

foreach(myExe s1ap
    secu_knas_encrypt_eia1
    secu_kenb
    aes128_ctr_encrypt
    aes128_ctr_decrypt
    secu_knas_encrypt_eea2
    secu_knas secu_knas_encrypt_eea1
    kdf
    aes128_cmac_encrypt
    secu_knas_encrypt_eia2
    )
  add_executable(test_${myExe}
    #TODO:PTZ161016 used to be test_util.c ?! but removed lately because of a duplicate
    #   in targets/TEST/PDCP/test_util.h
    ${OPENAIR3_DIR}/TEST/test_secu_util.c
    ${OPENAIR3_DIR}/TEST/test_${myExe}.c
    #
    ##TODO: #PTZ161016 bring back an test_util
    #${OPENAIR3_DIR}/TEST/oaisim_mme_test_s1c_scenario.c
    #${OPENAIR3_DIR}/TEST/oaisim_mme_test_s1c_scenario1.c
    #
    ${OPENAIR2_DIR}/ENB_APP/enb_config.h

    ${TRANSPORT_SOURCE}
    ${XFORMS_SOURCE}
    ${T_SOURCE}

    ${GTPU_need_ITTI}
    #L2
    #libL2.so : référence indéfinie vers « nas_nlh_rx »
    ${OPENAIR1_DIR}/SIMULATION/ETH_TRANSPORT/netlink_init.c
    )
  target_link_libraries(test_${myExe}
    #pthread  #
    Threads::Threads
    ${T_LIB} #rt
    m crypt
    ${CRYPTO_LIBRARIES}
    ${OPENSSL_LIBRARIES}
    ${NETTLE_LIBRARIES}
    ${LIBCONFIG_LIBRARIES}
    ${ITTI_LIB}   
#    -Wl,--start-group
    SECU_CN UTIL LFDS
    #S1AP_LIB S1AP_ENB GTPV1U SECU_CN SECU_OSA UTIL HASHTABLE SCTP_CLIENT UDP SCHED_LIB

#
#L2 ${MSC_LIB} ${RAL_LIB} ${NAS_UE_LIB}
    #${MIH_LIB}
    #    -Wl,--end-group
    ${OAI_S1AP_LIBRARIES}

    #L2 needs ...
    L2 ${OAI_RRC_LIBRARIES}

#    SIMU
#    SIMU # <--\
#    PHY
#libPHY.a(pbch.c.o)Â : Dans la fonction Â«Â rx_pbch_emulÂ Â»Â :
#/usr/src/openairinterface5g/openair1/PHY/LTE_TRANSPORT/pbch.c:1062Â : rÃ©fÃ©rence indÃ©finie vers Â«Â pbch_blerÂ Â»
#/usr/src/openairinterface5g/openair1/PHY/LTE_TRANSPORT/pbch.c:1071Â : rÃ©fÃ©rence indÃ©finie vers Â«Â uniformrandomÂ Â»
#in abstraction.c...
#here conbine for get uniformrandom ticked off.
SIMU
PHY
UTIL
GTPV1U
#L2 ${OAI_RRC_LIBRARIES}
#L2 SIMU GTPV1U SECU_OSA
S1AP_ENB
S1AP_LIB
SIMU_ETH
#UTIL
SCHED_LIB
#PHY
#HASHTABLE
#ITTI
LFDS

    )
endforeach(myExe)

# to be added
#../targets/TEST/PDCP/test_pdcp.c
#../targets/TEST/PDCP/with_rlc/test_pdcp_rlc.c
#
#
# Makefile in targets/TEST/PDCP 
#####TODO: PTZ161010
#add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/targets/TEST/PDCP)


#ensure that the T header files are generated before targets depending on them
#if (${T_TRACER})
  #add_dependencies(lte-softmodem generate_T)
  #add_dependencies(lte-softmodem-nos1 generate_T)
  #add_dependencies(rrh_gw generate_T)
  #add_dependencies(oaisim generate_T)
  #add_dependencies(oaisim_nos1 generate_T)
  #add_dependencies(dlsim generate_T)
  #add_dependencies(ulsim generate_T)
  #add_dependencies(pbchsim generate_T)
  #add_dependencies(scansim generate_T)
  #add_dependencies(mbmssim generate_T)
  #add_dependencies(pdcchsim generate_T)
  #add_dependencies(pucchsim generate_T)
  #add_dependencies(prachsim generate_T)
  #add_dependencies(syncsim generate_T)
#endif (${T_TRACER})

##################################################
# Generated specific cases is not regular code
###############################################
##################""
# itti symbolic debug print require to generate a specific include file
# like messages_xml.h
########################################

# retrieve the compiler options to send it to gccxml
get_directory_property( DirDefs COMPILE_DEFINITIONS )
foreach( d ${DirDefs} )
    list(APPEND itti_compiler_options "-D${d}")
endforeach()
get_directory_property( DirDefs INCLUDE_DIRECTORIES )
foreach( d ${DirDefs} )
    list(APPEND itti_compiler_options "-I${d}")
endforeach()

# castxml doesn't work with c11 (gcc 5 default)
# force castxml and clang compilation with gnu89 standard
# we can't use cXX standard as pthread_rwlock_t is gnu standard
##TODO: PTZ160917 do not believe it !! list(APPEND itti_compiler_options "-std=gnu89")
#PTZ161006
set (ITTI_H ${ITTI_DIR}/intertask_interface_types.h)
add_custom_command (
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/messages.xml  
  COMMAND gccxml ${itti_compiler_options} -fxml=${CMAKE_CURRENT_BINARY_DIR}/messages.xml ${ITTI_H}
  DEPENDS ${S1AP_OAI_generated} ${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}/asn1_constants.h
  )

add_custom_command (
  OUTPUT  ${CMAKE_CURRENT_BINARY_DIR}/messages_xml.h
  COMMAND sed -e "'s/ *//;s/\"/\\\\\"/g;s/^/\"/;s/$$/\\\\n\"/'" ${CMAKE_CURRENT_BINARY_DIR}/messages.xml > ${OPENAIR_BIN_DIR}/messages_xml.h
  DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/messages.xml ${OAI_RRC_ASN1C_INSTALL_INCLUDE_DIR}/asn1_constants.h
  )

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/messages_xml.h
  EXPORT openairinterface-targets
  DESTINATION "${INSTALL_INCLUDE_DIR}" COMPONENT dev
  )
add_custom_target (
  msgs_xml_gen
  DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/messages_xml.h
  )


#TODO: PTZ161018 obsoleted, ko to move inside its directory.
# Exmimo board drivers
#########################
list(APPEND openair_rf_src module_main.c irq.c fileops.c exmimo_fw.c)
message(WARNING "TODO: PTZ161018 use dkms_make_driver make_driver(openair_rf ${OPENAIR_TARGETS}/ARCH/EXMIMO/DRIVER/eurecom ${openair_rf_src})
")
add_executable(updatefw
  ${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/OAI_FW_INIT/updatefw.c
)

# OCTAVE tools
###############
set(OCT_INCL -I${OPENAIR_TARGETS}/ARCH/EXMIMO/DEFS -I${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/LIB -I${OPENAIR_TARGETS}/ARCH/COMMON)
set(OCT_LIBS -L${CMAKE_CURRENT_BINARY_DIR} -lm -lOPENAIR0_LIB) 
set(OCT_FLAGS -DEXMIMO)
set(OCT_DIR ${OPENAIR_TARGETS}/ARCH/EXMIMO/USERSPACE/OCTAVE)
set(OCT_FILES
  oarf_config_exmimo.oct
  oarf_config_exmimo.oct 
  oarf_get_frame.oct 
  oarf_stop.oct 
  oarf_send_frame.oct 
  oarf_get_num_detected_cards.oct 
  oarf_stop_without_reset.oct
)

foreach(file IN ITEMS ${OCT_FILES})
  string(REGEX REPLACE "oct *$" "cc" src ${file})
  add_custom_command(
    OUTPUT ${file}
    DEPENDS ${OCT_DIR}/${src} OPENAIR0_LIB
    COMMAND mkoctfile
    ARGS ${OCT_FLAGS} ${OCT_INCL} ${OCT_LIBS} 
    ARGS -o ${file} ${OCT_DIR}/${src}
    COMMENT "Generating ${file}"
    VERBATIM
  )
endforeach(file)

ADD_CUSTOM_TARGET(oarf
   DEPENDS ${OCT_FILES}
)
