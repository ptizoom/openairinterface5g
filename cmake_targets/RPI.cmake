#
#PTZ170106 has followed https://blog.kitware.com/cross-compiling-for-raspberry-pi/
#
#find toolchain
#
#    mkdir -p  ~/src/RaspberryPi/toolchain
#    cd ~/src/RaspberryPi/toolchain
#    wget http://crosstool-ng.org/download/crosstool-ng/crosstool-ng-1.17.0.tar.bz2
#    tar xjf crosstool-ng-1.17.0.tar.bz2
#    cd crosstool-ng-1.17.0
#
#cd ~/src/RaspberryPi/toolchain/crosstool-ng-1.17.0
#mkdir -p ~/local/crosstool-ng
#./configure –prefix=/home/ibanez/local/crosstool-ng
#
#     sudo aptitude install bison cvs flex gperf texinfo automake libtool 
# then we can do
#     make
#     make install
# and add to the PATH the bin directory where crosstool-ng was installed:
#     export PATH=$PATH:/home/ibanez/local/crosstool-ng/bin/
# In some cases, it might be necessary to unset the LD_LIBRARY_PATH,
# to prevent the toolchain from grabbing other shared libraries from the host machine:
#     unset LD_LIBRARY_PATH
#




	

#git clone https://github.com/raspberrypi/tools.git

#if (CMAKE_CROSSCOMPILING)

set(CMAKE_SYSTEM_NAME RPI)
set(CMAKE_RPI_NDK ${CMAKE_SOURCE_DIR}/rpi)

set(CMAKE_SYSTEM_VERSION 21) # API level
set(CMAKE_RPI_ARCH_ABI arm64-v8a)
set(CMAKE_RPI_NDK /path/to/android-ndk)
##set(CMAKE_RPI_STL_TYPE gnustl_static)
##set(CMAKE_RPI_STANDALONE_TOOLCHAIN /path/to/rpi-toolchain)
##<ndk>/platforms/android-<api>/arch-<arch>






if (0)

SET(CMAKE_SYSTEM_NAME Linux)
#like Linux+Android!?
#SET(CMAKE_SYSTEM_NAME RaspberryPi)
##PTZ170108
SET(CMAKE_SYSTEM_VERSION 1)

set(CMAKE_SYSTEM_PROCESSOR arm)
set(CMAKE_SYSROOT /home/devel/rasp-pi-rootfs)
set(CMAKE_STAGING_PREFIX /home/devel/stage)

# Specify the cross compiler
set(_tools /home/devel/gcc-4.7-linaro-rpi-gnueabihf)
set(CMAKE_C_COMPILER ${_tools}/bin/arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER ${_tools}/bin/arm-linux-gnueabihf-g++)
SET(CMAKE_C_COMPILER $ENV{HOME}/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc)
SET(CMAKE_CXX_COMPILER $ENV{HOME}/rpi/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-g++)
SET(CMAKE_C_COMPILER   /opt/eldk-2007-01-19/usr/bin/ppc_74xx-gcc)
SET(CMAKE_CXX_COMPILER /opt/eldk-2007-01-19/usr/bin/ppc_74xx-g++)



set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

#
# Where is the target environment
#
SET(CMAKE_FIND_ROOT_PATH  /opt/eldk-2007-01-19/ppc_74xx /home/alex/eldk-ppc74xx-inst)
SET(CMAKE_FIND_ROOT_PATH $ENV{HOME}/rpi/rootfs)

SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")
SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")
SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")

# Search for programs only in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# Search for libraries and headers only in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

#If you want to use VideoCore IV-related libraries, you may also consider adding the following commands to the toolchain file 2:
#INCLUDE_DIRECTORIES($ENV{HOME}/rpi/rootfs/opt/vc/include)
#INCLUDE_DIRECTORIES($ENV{HOME}/rpi/rootfs/opt/vc/include/interface/vcos/pthreads)
#INCLUDE_DIRECTORIES($ENV{HOME}/rpi/rootfs/opt/vc/include/interface/vmcs_host/linux)


#~/src$ cd build
#~/src/build$ cmake -DCMAKE_TOOLCHAIN_FILE=~/Toolchain-eldk-ppc74xx.cmake ..

endif(0)
