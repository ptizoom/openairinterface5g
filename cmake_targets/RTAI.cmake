#
#
#
project(RTAI C CC)

#https://cmake.org/Wiki/CMake_Cross_Compiling

#add_custom_target(  )
#
# target must be what ever cross we want...!
#
#TODO:PTZ161216 in fact would need linux source...!
# --with-linux-dir=/usr/src/linux by default
#  --with-module-dir=${exec_prefix}/modules   /usr/realtime
#  --with-kconfig-file=
# package ... linux-source-4.8 with linux-patch-4.8-rt.patch
#bla bla bla ... very much complex
# DO NOT USE RTAI! but https://rt.wiki.kernel.org
# also use ie. amd64_rt_amd64... on debian
#CONFIG_PREEMPT=y
#CONFIG_PREEMPT_RT_BASE=y
#CONFIG_HAVE_PREEMPT_LAZY=y
#CONFIG_PREEMPT_LAZY=y
#CONFIG_PREEMPT_RT_FULL=y
# # find it!
# # apt-get install linux-source-4.3
# $ tar xaf /usr/src/linux-source-4.3.tar.xz
# The unpacked source tree then will be available in linux-source-4.3 directory.
# 4.2. Rebuilding official Debian kernel packages
# You can build all or selected kernel packages by following these instructions. You may be asked to do this in order to test a potential bug fix.
# 4.2.1. Preparation
# Run the following commands:
# # apt-get install build-essential fakeroot
# # apt-get build-dep linux
# This will install the packages required by the kernel build process.
# $ apt-get source linux
# This will download and unpack the linux source package, making the tree available in the linux-version directory. As always, the revision part of the version of this package (for example, 1 in 3.2.19-1) will determine its patchlevel with respect to the original upstream kernel source.
# $ cd linux-version
# Enter the source directory.
# 4.2.1.1. Disk space requirements
# Building binary packages for a single kernel flavour with debug info enabled (currently true for the 686-pae, amd64, rt-686-pae, rt-amd64 and s390x configurations) requires up to 10 GB space in the package directory and 300 MB in /tmp (or $TMPDIR).
# Building with debug info disabled requires about 2 GB and 25 MB respectively. You can disable debug info by changing the value of debug-info to false in debian/config/arch/defines.
# Building all binary packages for i386 or amd64 currently requires about 20 GB space in the package directory. Other architectures with no debug info or fewer drivers will require less space.
# 4.2.2. Simple patching and building
# The source package includes a script to simplify the process of building with extra patches. You can use this by running commands such as:
# # apt-get install devscripts
# $ bash debian/bin/test-patches ../fix-bug123456.patch ../add-foo-driver.patch
# This script has options to control the flavour, featureset, etc. For a summary of the options, run:
# $ bash debian/bin/test-patches
# You may then need to build the linux-base package as well:
# $ fakeroot make -f debian/rules.real install-linux-base
# However, if you need to change the configuration or make other changes, you should not use this script and should follow the instructions below.
# 4.2.3. Applying patches or configuration changes
# It is possible to apply extra patches to the source before starting the build. In the linux source package, the default (non-featureset) patches are automatically applied in the top level directory.
# The patched source appears in the following directories.
# default source:
# top level
# source with featureset:
# debian/build/source_featureset
# You should apply the extra patches in the appropriate directory. In the linux source package you can use the quilt utility to do this.
# To change the configuration before building, for example for the 686-pae flavour on i386, run the commands:
# $ fakeroot make -f debian/rules.gen setup_i386_none_686-pae
# $ make -C debian/build/build_i386_none_686-pae nconfig
# If the patches or configuration changes alter type definitions for the kernel, you may need to change the ABI name; see Section 5.2.1, �The ABI name�.
# 4.2.4. Building many packages
# To build all possible packages for this architecture, run:
# $ fakeroot debian/rules binary
# To build all architecture-dependent packages, run:
# $ fakeroot debian/rules binary-arch
# To build all architecture-independent packages, run:
# $ fakeroot debian/rules binary-indep
# 4.2.5. Building packages for one flavour
# For example, to build only the binary packages for 686-pae flavour on i386 architecture, use the following commands:
# $ fakeroot debian/rules source
# $ fakeroot make -f debian/rules.gen binary-arch_i386_none_686-pae
# The target in this command has the general form of target_arch_featureset_flavour. Replace the featureset with none if you do not want any of the extra featuresets. This command will build the linux image and kernel headers packages. You may also need the linux-headers-version-common binary package, which can be built using the commands:
# $ fakeroot debian/rules source
# $ fakeroot make -f debian/rules.gen binary-arch_i386_none_real
# The target in this command has the general form of target_arch_featureset_real
# 4.3. Building a development version of the Debian kernel package
# To build a kernel image based on the kernel team's unreleased development version:
# # apt-get install build-essential fakeroot rsync git
# # apt-get build-dep linux
# The last two commands will install the build dependencies required by the kernel build process.
# $ git clone -b dist --single-branch https://anonscm.debian.org/git/kernel/linux.git
# This will check out the Debian packaging. dist is normally the distribution codename such as wheezy or sid (unstable). For the very latest version, usually based on an upstream release candidate, use master. Note that this will download several hundred megabytes of data.
# $ apt-get source -d linux
# This will download the linux upstream source (and the last released Debian patches). Depending on which version you are trying to build, you might need to override APT's version selection or download a tarball from people.debian.org instead.
# $ cd linux
# $ debian/rules orig
# This unpacks the upstream source and merges it with the Debian packaging.
# $ debian/rules debian/control
# This generates a Debian package control file based on the current definitions of the various kernel flavours which can be built.
# $ fakeroot debian/rules target
# Finally, build binary packages as explained in Section 4.2, �Rebuilding official Debian kernel packages�.
# 4.4. Generating orig tarball from newer upstream
# First you must add a changelog entry for the new upstream version. If the new version is a release candidate, change the string -rc to ~rc. (In Debian package versions, a suffix beginning with ~ indicates a pre-release.)
# The 'orig' tarball is generated by the genorig.py script. It takes either a tarball and optional patch from kernel.org, or a git repository. First ensure you have the necessary tools installed:
# # apt-get install unifdef
# Then if you have a tarball, run a command such as:
# $ debian/bin/genorig.py ../linux-4.3.tar.xz ../patch-4.4-rc1.xz
# Or if you have a git repository, pass the name of its directory:

# $ debian/bin/genorig.py ~/src/linux
# Either of these will generate a file such as ../orig/linux_3.5~rc1.orig.tar.xz. You can then combine this tarball with the Debian packaging by running:

# $ debian/rules orig
# 4.5. Building a custom kernel from Debian kernel source

# This section describes the simplest possible procedure to build a custom kernel the "Debian way". It is assumed that user is somewhat familiar with kernel configuration and build process. If that's not the case, it is recommended to consult the kernel documentation and many excellent online resources dedicated to it.

# The easiest way to build a custom kernel (the kernel with the configuration different from the one used in the official packages) from the Debian kernel source is to use the linux-source package and the make deb-pkg target. First, prepare the kernel tree:

# # apt-get install linux-source-4.3
# $ tar xaf /usr/src/linux-source-4.3.tar.xz
# $ cd linux-source-4.3
# The kernel now needs to be configured, that is you have to set the kernel options and select the drivers which are going to be included, either as built-in, or as external modules. The kernel build infrastructure offers a number of targets, which invoke different configuration frontends. For example, one can use console-based menu configuration by invoking the command

# $ make nconfig
# Instead of nconfig one can use config (text-based line-by-line configuration frontend) or xconfig (graphical configuration frontend). It is also possible to reuse your old configuration file by placing it as a .config file in the top-level directory and running one of the configuration targets (if you want to adjust something) or make oldconfig (to keep the same configuration). Note that different frontends may require different additional libraries and utilities to be installed to function properly. For example, the nconfig frontend requires the ncurses library, which at time of writing is provided by the libncurses5-dev package.
# The build will use less disk space if the CONFIG_DEBUG_INFO option is disabled (see Section 4.2.1.1, �Disk space requirements�). Debuginfo is only needed if you plan to use binary object tools like crash, kgdb, and SystemTap on the kernel.
# $ scripts/config --disable DEBUG_INFO
# After the configuration process is finished, the new or updated kernel configuration will be stored in .config file in the top-level directory. The build is started using the commands
# $ make clean
# $ make deb-pkg
# As a result of the build, a custom kernel package linux-image-3.2.19_3.2.19-1_i386.deb (name will reflect the version of the kernel and build number) will be created in the directory one level above the top of the tree. It may be installed using dpkg just as any other package:
# # dpkg -i ../linux-image-3.2.19_3.2.19-1_i386.deb
# This command will unpack the kernel, generate the initrd if necessary (see Chapter 7, Managing the initial ramfs (initramfs) archive for details), and configure the bootloader to make the newly installed kernel the default one. If this command completed without any problems, you can reboot using the
# # shutdown -r now
# command to boot the new kernel.
# For much more information about bootloaders and their configuration please check their documentation, which can be accessed using the commands man lilo, man lilo.conf, man grub, and so on. You can also look for documentation in the /usr/share/doc/package directories, with package being the name of the package involved.
# 4.6. Building a custom kernel from the "pristine" kernel source
# Building a kernel from the "pristine" (also sometimes called "vanilla") kernel source, distributed from www.kernel.org and its mirrors, may be occasionally useful for debugging or in the situations when a newer kernel version is desired. The procedure differs only in obtaining the kernel source: instead of unpacking the kernel source from Debian packages, the "pristine" source is downloaded using your favourite browser or using wget, as follows:
# $ wget https://kernel.org/pub/linux/kernel/v4.x/linux-4.3.tar.xz
# The integrity of the downloaded archive may be verified by fetching the corresponding cryptographic signature
# $ wget https://kernel.org/pub/linux/kernel/v4.x/linux-4.3.tar.sign
# and running this command (gnupg package must be installed):
# $ unxz -c linux-4.3.tar.xz | gpg --verify linux-4.3.tar.sign -
# Successful verification results in output similar to the one below:
# gpg: Signature made Mon 21 May 2012 01:48:14 AM CEST using RSA key ID 00411886
# gpg: Good signature from "Linus Torvalds <torvalds@linux-foundation.org>"
# gpg: WARNING: This key is not certified with a trusted signature!
# gpg:          There is no indication that the signature belongs to the owner.
# Primary key fingerprint: ABAF 11C6 5A29 70B1 30AB  E3C4 79BE 3E43 0041 1886
# After that the archive may be unpacked using
# $ tar xaf linux-4.3.tar.xz
# $ cd linux-4.3
# The unpacked kernel tree (in linux-4.3 now has to be configured. The existing configuration file may be used as a starting point
# $ cp /boot/config-3.2.0-2-686-pae ./.config
# After the configuration with one of the configuration frontends (invoked by make oldconfig, make config, make nconfig, etc) is completed, the build may be started using make deb-pkg target as described above.
# 4.7. Building out-of-tree kernel modules
# Some kernel modules are not included in the upstream or Debian kernel source, but are provided as third-party source packages. For some of the most popular out-of-tree modules, the binary Debian packages with modules built against the stock Debian kernels are provided. For example, if you are running stock Debian kernel 3.2.0-2-686-pae (use the uname -r command to verify the version) from the linux-image-3.2.0-2-686-pae package, and would like to use the squash filesystem, all you need to do is install squashfs-modules-3.2.0-2-686-pae binary package, which provides the neccessary binary kernel modules.
# If you are not so lucky, and there are no binary module packages in the archive, there is a fair chance that the Debian archive contains the packaged source for the kernel modules. Names of such packages typically end in -source, for example squashfs-source, thinkpad-source, rt2x00-source and many others. These packages contain debianized source code of the kernel modules, suitable for building using the module-assistant (or m-a) script from the module-assistant package. Typical sequence to build a custom binary module package, matching a kernel 3.2.0-2-686-pae (as returned by uname -r) from the debianized source consists of the following steps:
# Install a set of kernel headers, matching the kernel for which the modules are going to be built:
# # apt-get install linux-headers-3.2.0-2-686-pae
# Install the package containing the source:
# # apt-get install squashfs-source

