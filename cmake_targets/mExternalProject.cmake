#modified ExternalProject module...
#cmake_policy(SET CMP0017 NEW)
#include system...
include(ExternalProject)

function(__ep_write_gitclone_script
    script_filename
    source_dir
    git_EXECUTABLE
    git_repository
    git_tag
    git_remote_name
    git_submodules
    git_shallow
    src_name
    work_dir
    gitclone_infofile
    gitclone_stampfile
    tls_verify
    )
  if(NOT GIT_VERSION_STRING VERSION_LESS 1.7.10)
    set(git_clone_shallow_options "--depth 1 --no-single-branch")
  else()
    set(git_clone_shallow_options "--depth 1")
  endif()
  file(WRITE ${script_filename}
"if(\"${git_tag}\" STREQUAL \"\")
  message(FATAL_ERROR \"Tag for git checkout should not be empty.\")
endif()

set(run 0)

message(STATUS \"in ${work_dir}...with ${gitclone_stampfile} using top prj ${PROJECT_SOURCE_DIR} src_name ${src_name} \")

if(\"${gitclone_infofile}\" IS_NEWER_THAN \"${gitclone_stampfile}\")
  set(run 1)
endif()

if(NOT run)
  message(STATUS \"Avoiding repeated git clone, stamp file is up to date: '${gitclone_stampfile}'\")
  return()
endif()

#PTZ160810 no ways
if(0)
execute_process(
  COMMAND \${CMAKE_COMMAND} -E remove_directory \"${source_dir}\"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR \"Failed to remove directory: '${source_dir}'\")
endif()
endif(0)

set(git_options)

# disable cert checking if explicitly told not to do it
set(tls_verify \"${tls_verify}\")
if(NOT \"x${tls_verify}\" STREQUAL \"x\" AND NOT tls_verify)
  list(APPEND git_options
    -c http.sslVerify=false)
endif()

set(git_clone_options)

set(git_shallow \"${git_shallow}\")
if(git_shallow)
  list(APPEND git_clone_options ${git_clone_shallow_options})
endif()


#PTZ161002
  set(top_source_dir \"${PROJECT_SOURCE_DIR}\")
  file(RELATIVE_PATH submod_name_p \"\${top_source_dir}\" \"${source_dir}\")
  while(submod_name_p MATCHES \"^../\")
    string(APPEND top_source_dir \"/..\")
    file(RELATIVE_PATH submod_name_p \"\${top_source_dir}\" \"${source_dir}\")
  endwhile()
  string(CONCAT submod_name_p \"./\" \${submod_name_p})


#PTZ160810
if(NOT EXISTS \"${source_dir}/.git\")

#is it already a submodule
  if(NOT (\"${git_tag}\" STREQUAL \"master\"))
     set(git_branch \"-b ${git_tag}\")
  endif()
  if(NOT (\"${git_remote_name}\" STREQUAL \"origin\"))
     set(git_name \"--name ${git_remote_name}/${src_name}\")
  else()
     set(git_name \"--name ${src_name}\")
  endif()
    execute_process(
      COMMAND \"${git_EXECUTABLE}\" submodule add \${git_branch} \${git_name} -- \"${git_repository}\" \"\${submod_name_p}\"
      WORKING_DIRECTORY \${top_source_dir}
      RESULT_VARIABLE error_code
    )
  if(error_code)
     message(STATUS \"FAILED COMMAND::${git_EXECUTABLE} submodule add \${git_branch} \${git_name} ${git_repository} \${submod_name_p}\")
  endif(error_code)

# try the clone 3 times incase there is an odd git clone issue
set(error_code 1)
set(number_of_tries 0)
while(error_code AND number_of_tries LESS 3)
  execute_process(
      COMMAND \"${git_EXECUTABLE}\" submodule update --init -- \"\${submod_name_p}\"
      WORKING_DIRECTORY  \${top_source_dir}
##PTZ160810    COMMAND \"${git_EXECUTABLE}\" \${git_options} clone \${git_clone_options} --origin \"${git_remote_name}\" \"${git_repository}\" \"${src_name}\"
##PTZ160810    WORKING_DIRECTORY \"${work_dir}\"
    RESULT_VARIABLE error_code
    )
  math(EXPR number_of_tries \"\${number_of_tries} + 1\")
endwhile()
if(number_of_tries GREATER 1)
  message(STATUS \"Had to git clone more than once:
          \${number_of_tries} times.\")
endif()
  endif(NOT EXISTS \"${source_dir}/.git\")

  if((error_code MATCHES \"did not contain\") AND EXISTS \"${source_dir}/.git\")
      # trying direct to checkout anyway
      execute_process(
        COMMAND \"${git_EXECUTABLE}\" checkout ${git_tag} -- .
        WORKING_DIRECTORY \"${work_dir}/${src_name}\"
        RESULT_VARIABLE error_code
      )
  endif()

  if(error_code OR NOT EXISTS \"${source_dir}/.git\")
    message(FATAL_ERROR \"Failed to update submodule \${submod_name_p} in \${PROJECT_SOURCE_DIR} for repository: '${git_repository}'\")
  endif()



if(0) #PTZ161002
execute_process(
  COMMAND \"${git_EXECUTABLE}\" \${git_options} checkout ${git_tag}
  WORKING_DIRECTORY \"${work_dir}/${src_name}\"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR \"Failed to checkout tag: '${git_tag}'\")
endif()
endif(0)

#
# sub sub modules
#
if(NOT \"${git_submodules}\" STREQUAL \"\") #PTZ161002
execute_process(
  COMMAND \"${git_EXECUTABLE}\" \${git_options} submodule init -- ${git_submodules}
  WORKING_DIRECTORY \"${work_dir}/${src_name}\"
  RESULT_VARIABLE error_code
  )
if(error_code)
  #message(FATAL_ERROR \"Failed to init submodules in: '${work_dir}/${src_name}'\")
  message(FATAL_ERROR \"Failed to init submodules '${git_submodules}' in: '${work_dir}/${src_name}'\")
endif()

execute_process(
  COMMAND \"${git_EXECUTABLE}\" \${git_options} submodule update --recursive --init -- ${git_submodules}
  WORKING_DIRECTORY \"${work_dir}/${src_name}\"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR \"Failed to update submodules in: '${work_dir}/${src_name}'\")
endif()
endif(NOT \"${git_submodules}\" STREQUAL \"\") #PTZ161002


# Complete success, update the script-last-run stamp file:
#
if(NOT EXISTS \"${source_dir}/.git\")
  message(FATAL_ERROR \"Failed to create ${source_dir}/.git\")
  set(error_code 1)
endif()

execute_process(
  COMMAND \${CMAKE_COMMAND} -E copy
    \"${gitclone_infofile}\"
    \"${gitclone_stampfile}\"
  WORKING_DIRECTORY \"${work_dir}/${src_name}\"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR \"Failed to copy script-last-run stamp file: '${gitclone_stampfile}'\")
endif()

"
)

endfunction()

function(_ep_add_update_command name)
  ExternalProject_Get_Property(${name} source_dir tmp_dir)

  get_property(cmd_set TARGET ${name} PROPERTY _EP_UPDATE_COMMAND SET)
  get_property(cmd TARGET ${name} PROPERTY _EP_UPDATE_COMMAND)
  get_property(cvs_repository TARGET ${name} PROPERTY _EP_CVS_REPOSITORY)
  get_property(svn_repository TARGET ${name} PROPERTY _EP_SVN_REPOSITORY)
  get_property(git_repository TARGET ${name} PROPERTY _EP_GIT_REPOSITORY)
  get_property(hg_repository  TARGET ${name} PROPERTY _EP_HG_REPOSITORY )
  get_property(update_disconnected_set TARGET ${name} PROPERTY _EP_UPDATE_DISCONNECTED SET)
  if(update_disconnected_set)
    get_property(update_disconnected TARGET ${name} PROPERTY _EP_UPDATE_DISCONNECTED)
  else()
    get_property(update_disconnected DIRECTORY PROPERTY EP_UPDATE_DISCONNECTED)
  endif()

  set(work_dir)
  set(comment)
  set(always)

  if(cmd_set)
    set(work_dir ${source_dir})
    if(NOT "x${cmd}" STREQUAL "x")
      set(always 1)
    endif()
  elseif(cvs_repository)
    if(NOT CVS_EXECUTABLE)
      message(FATAL_ERROR "error: could not find cvs for update of ${name}")
    endif()
    set(work_dir ${source_dir})
    set(comment "Performing update step (CVS update) for '${name}'")
    get_property(cvs_tag TARGET ${name} PROPERTY _EP_CVS_TAG)
    set(cmd ${CVS_EXECUTABLE} -d ${cvs_repository} -q up -dP ${cvs_tag})
    set(always 1)
  elseif(svn_repository)
    if(NOT Subversion_SVN_EXECUTABLE)
      message(FATAL_ERROR "error: could not find svn for update of ${name}")
    endif()
    set(work_dir ${source_dir})
    set(comment "Performing update step (SVN update) for '${name}'")
    get_property(svn_revision TARGET ${name} PROPERTY _EP_SVN_REVISION)
    get_property(svn_username TARGET ${name} PROPERTY _EP_SVN_USERNAME)
    get_property(svn_password TARGET ${name} PROPERTY _EP_SVN_PASSWORD)
    get_property(svn_trust_cert TARGET ${name} PROPERTY _EP_SVN_TRUST_CERT)
    set(svn_user_pw_args "")
    if(DEFINED svn_username)
      set(svn_user_pw_args ${svn_user_pw_args} "--username=${svn_username}")
    endif()
    if(DEFINED svn_password)
      set(svn_user_pw_args ${svn_user_pw_args} "--password=${svn_password}")
    endif()
    if(svn_trust_cert)
      set(svn_trust_cert_args --trust-server-cert)
    endif()
    set(cmd ${Subversion_SVN_EXECUTABLE} up ${svn_revision}
      --non-interactive ${svn_trust_cert_args} ${svn_user_pw_args})
    set(always 1)
  elseif(git_repository)
    unset(CMAKE_MODULE_PATH) # Use CMake builtin find module
    find_package(Git QUIET)
    if(NOT GIT_EXECUTABLE)
      message(FATAL_ERROR "error: could not find git for fetch of ${name}")
    endif()
    set(work_dir ${source_dir})
    set(comment "Performing update step for '${name}'")
    get_property(git_tag TARGET ${name} PROPERTY _EP_GIT_TAG)
    if(NOT git_tag)
      set(git_tag "master")
    endif()
    get_property(git_remote_name TARGET ${name} PROPERTY _EP_GIT_REMOTE_NAME)
    if(NOT git_remote_name)
      set(git_remote_name "origin")
    endif()
    get_property(git_submodules TARGET ${name} PROPERTY _EP_GIT_SUBMODULES)
    _ep_write_gitupdate_script(${tmp_dir}/${name}-gitupdate.cmake
      ${GIT_EXECUTABLE} ${git_tag} ${git_remote_name} "${git_submodules}" ${git_repository} ${work_dir}
      )
    set(cmd ${CMAKE_COMMAND} -P ${tmp_dir}/${name}-gitupdate.cmake)
    set(always 1)
  elseif(hg_repository)
    if(NOT HG_EXECUTABLE)
      message(FATAL_ERROR "error: could not find hg for pull of ${name}")
    endif()
    set(work_dir ${source_dir})
    set(comment "Performing update step (hg pull) for '${name}'")
    get_property(hg_tag TARGET ${name} PROPERTY _EP_HG_TAG)
    if(NOT hg_tag)
      set(hg_tag "tip")
    endif()
    if("${HG_VERSION_STRING}" STREQUAL "2.1")
      message(WARNING "Mercurial 2.1 does not distinguish an empty pull from a failed pull:
 http://mercurial.selenic.com/wiki/UpgradeNotes#A2.1.1:_revert_pull_return_code_change.2C_compile_issue_on_OS_X
 http://thread.gmane.org/gmane.comp.version-control.mercurial.devel/47656
Update to Mercurial >= 2.1.1.
")
    endif()
    set(cmd ${HG_EXECUTABLE} pull
      COMMAND ${HG_EXECUTABLE} update ${hg_tag}
      )
    set(always 1)
  endif()

  get_property(log TARGET ${name} PROPERTY _EP_LOG_UPDATE)
  if(log)
    set(log LOG 1)
  else()
    set(log "")
  endif()

  get_property(uses_terminal TARGET ${name} PROPERTY
    _EP_USES_TERMINAL_UPDATE)
  if(uses_terminal)
    set(uses_terminal USES_TERMINAL 1)
  else()
    set(uses_terminal "")
  endif()

  ExternalProject_Add_Step(${name} update
    COMMENT ${comment}
    COMMAND ${cmd}
    ALWAYS ${always}
    EXCLUDE_FROM_MAIN ${update_disconnected}
    WORKING_DIRECTORY ${work_dir}
    DEPENDEES download
    ${log}
    ${uses_terminal}
    )

  if(always AND update_disconnected)
    _ep_get_step_stampfile(${name} skip-update skip-update_stamp_file)
    string(REPLACE "Performing" "Skipping" comment "${comment}")
    ExternalProject_Add_Step(${name} skip-update
      COMMENT ${comment}
      #ALWAYS 1
      #EXCLUDE_FROM_MAIN 1
      WORKING_DIRECTORY ${work_dir}
      DEPENDEES download
      ${log}
      ${uses_terminal}
    )
    set_property(SOURCE ${skip-update_stamp_file} PROPERTY SYMBOLIC 1)
  endif()

endfunction()


#PTZ160810 BEWARE 
# COMMAND \${CMAKE_COMMAND} -E remove_directory \"${source_dir}\"
# is done and you would loose the whole .git directory/file
# when _ep_write_gitclone_script is called... this is not good.
#
#    _ep_write_gitclone_script(
#      ${tmp_dir}/${name}-gitclone.cmake
#      ${source_dir}
#      ${GIT_EXECUTABLE}
#      ${git_repository}
#      ${git_tag}  
#      ${git_remote_name}   PTZ160901 ????git_remote_name missing in cmake-3.3.2/
#     "${git_submodules}"
#      "${git_shallow}"  PTZ161002 ???? missing in cmake-3.6.2/
#      ${src_name}
#      ${work_dir}
#      ${stamp_dir}/${name}-gitinfo.txt
#      ${stamp_dir}/${name}-gitclone-lastrun.txt
#      "${tls_verify}" PTZ161002 ????  missing in cmake-3.6.2/
#      )
if(${CMAKE_VERSION} VERSION_LESS 3.4.0)

function(_ep_write_gitclone_script script_filename source_dir git_EXECUTABLE git_repository git_tag

git_submodules src_name work_dir gitclone_infofile gitclone_stampfile)

__ep_write_gitclone_script(
 ${script_filename}
 ${source_dir}
 ${git_EXECUTABLE}
 ${git_repository}
 ${git_tag}
 ""
 "${git_submodules}"
 ""
 ${src_name}
 ${work_dir}
 ${gitclone_infofile}
 ${gitclone_stampfile}
 ""
)
endfunction()



else()
  function(_ep_write_gitclone_script
      script_filename source_dir
      git_EXECUTABLE git_repository git_tag
      git_remote_name
      git_submodules
      git_shallow
      src_name
      work_dir
      gitclone_infofile gitclone_stampfile
      tls_verify
      )
message(STATUS " calling    __ep_write_gitclone_script
      ${script_filename}
      ${source_dir}
      ${git_EXECUTABLE}
      ${git_repository}
      ${git_tag}
      ${git_remote_name}
      ${git_submodules}
      ${git_shallow}
      ${src_name}
      ${work_dir}
      ${gitclone_infofile}
      ${gitclone_stampfile}
      ${tls_verify}
      ")
 
    __ep_write_gitclone_script(
      ${script_filename}
      ${source_dir}
      ${git_EXECUTABLE}
      ${git_repository}
      ${git_tag}
      ${git_remote_name}
      "${git_submodules}"
      "${git_shallow}"
      ${src_name}
      ${work_dir}
      ${gitclone_infofile}
      ${gitclone_stampfile}
      "${tls_verify}"
      )
  endfunction()
endif()




#3.6.2 !

function(__ep_write_gitclone_script_
    script_filename source_dir git_EXECUTABLE git_repository git_tag git_remote_name
    git_submodules src_name work_dir gitclone_infofile gitclone_stampfile
    )
  file(WRITE ${script_filename}
"
set(run 0)
message(STATUS \"in ${work_dir}...with ${gitclone_stampfile} using top prj ${PROJECT_SOURCE_DIR} src_name ${src_name} \")
if((\"${gitclone_infofile}\" IS_NEWER_THAN \"${gitclone_stampfile}\")
  OR NOT EXISTS \"${source_dir}/.git\")
  set(run 1)
endif()

if(NOT run)
  message(STATUS \"Avoiding repeated git clone, stamp file is up to date: '${gitclone_stampfile}'\")
  return()
endif()

#PTZ160810 no ways
if(0)
execute_process(
  COMMAND \${CMAKE_COMMAND} -E remove_directory \"${source_dir}\"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR \"Failed to remove directory: '${source_dir}'\")
endif()
endif()

  set(top_source_dir \"${PROJECT_SOURCE_DIR}\")
  file(RELATIVE_PATH submod_name_p \"\${top_source_dir}\" \"${source_dir}\")
  while(submod_name_p MATCHES \"^../\")
    string(APPEND top_source_dir \"/..\")
    file(RELATIVE_PATH submod_name_p \"\${top_source_dir}\" \"${source_dir}\")
  endwhile()
  string(CONCAT submod_name_p \"./\" \${submod_name_p})

if(NOT EXISTS \"${source_dir}/.git\")

#is it already a submodule
  if(NOT (\"${git_tag}\" STREQUAL \"master\"))
     set(git_branch \"-b ${git_tag}\")
  endif()
  if(NOT (\"${git_remote_name}\" STREQUAL \"origin\"))
     set(git_name \"--name ${git_remote_name}/${src_name}\")
  else()
     set(git_name \"--name ${src_name}\")
  endif()
    execute_process(
      COMMAND \"${git_EXECUTABLE}\" submodule add \${git_branch} \${git_name} -- \"${git_repository}\" \"\${submod_name_p}\"
      WORKING_DIRECTORY \${top_source_dir}
      RESULT_VARIABLE error_code
    )
  if(error_code)
     message(STATUS \"FAILED COMMAND::${git_EXECUTABLE} submodule add \${git_branch} \${git_name} ${git_repository} \${submod_name_p}\")
  endif()

  if (0)
  #init anyway
  execute_process(
    COMMAND \"${git_EXECUTABLE}\" submodule init -- \"\${submod_name_p}\"
    WORKING_DIRECTORY  \${top_source_dir}
    RESULT_VARIABLE error_code
  )
  if(error_code)
    message(FATAL_ERROR \"Failed to init submodule \${submod_name_p} in \${top_source_dir}\")
  endif()
  endif(0)

  # try the clone 3 times incase there is an odd git clone issue
  set(error_code 1)
  set(number_of_tries 0)
  while(error_code AND number_of_tries LESS 3)
    execute_process(
      COMMAND \"${git_EXECUTABLE}\" submodule update --init -- \"\${submod_name_p}\"
      WORKING_DIRECTORY  \${top_source_dir}
      RESULT_VARIABLE error_code
    )
    math(EXPR number_of_tries \"\${number_of_tries} + 1\")
  endwhile()
  if(number_of_tries GREATER 1)
    message(STATUS \"Had to ${git_EXECUTABLE} submodule update \${submod_name_p} more than once: \${number_of_tries} times.\")
  endif()
  endif(NOT EXISTS \"${source_dir}/.git\")

  if((error_code MATCHES \"did not contain\") AND EXISTS \"${source_dir}/.git\")
      # trying direct to checkout anyway
      execute_process(
        COMMAND \"${git_EXECUTABLE}\" checkout ${git_tag} -- .
        WORKING_DIRECTORY \"${work_dir}/${src_name}\"
        RESULT_VARIABLE error_code
      )
  endif()

  if(error_code OR NOT EXISTS \"${source_dir}/.git\")
    message(FATAL_ERROR \"Failed to update submodule \${submod_name_p} in \${PROJECT_SOURCE_DIR} for repository: '${git_repository}'\")
  endif()

#
# sub sub modules
#
if(NOT \"${git_submodules}\" STREQUAL \"\")
  execute_process(
    COMMAND \"${git_EXECUTABLE}\" submodule init -- ${git_submodules}
    WORKING_DIRECTORY \"${work_dir}/${src_name}\"
    RESULT_VARIABLE error_code
  )
  if(error_code)
    message(FATAL_ERROR \"Failed to init submodules '${git_submodules}' in: '${work_dir}/${src_name}'\")
  endif()

  execute_process(
    COMMAND \"${git_EXECUTABLE}\" submodule update --recursive -- ${git_submodules}
    WORKING_DIRECTORY \"${work_dir}/${src_name}\"
    RESULT_VARIABLE error_code
  )
  if(error_code)
    message(FATAL_ERROR \"Failed to update submodules in: '${work_dir}/${src_name}'\")
  endif()
endif()


# Complete success, update the script-last-run stamp file:
#
if(NOT EXISTS \"${source_dir}/.git\")
  message(FATAL_ERROR \"Failed to create ${source_dir}/.git\")
  set(error_code 1)
endif()

execute_process(
  COMMAND \${CMAKE_COMMAND} -E copy
    \"${gitclone_infofile}\"
    \"${gitclone_stampfile}\"
  WORKING_DIRECTORY \"${work_dir}/${src_name}\"
  RESULT_VARIABLE error_code
  )
if(error_code)
  message(FATAL_ERROR \"Failed to copy script-last-run stamp file: '${gitclone_stampfile}'\")
endif()

"
)
endfunction()
