#/*
# * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
# * contributor license agreements.  See the NOTICE file distributed with
# * this work for additional information regarding copyright ownership.
# * The OpenAirInterface Software Alliance licenses this file to You under
# * the OAI Public License, Version 1.0  (the "License"); you may not use this file
# * except in compliance with the License.
# * You may obtain a copy of the License at
# *
# *      http://www.openairinterface.org/?page_id=698
# *
# * Unless required by applicable law or agreed to in writing, software
# * distributed under the License is distributed on an "AS IS" BASIS,
# * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# * See the License for the specific language governing permissions and
# * limitations under the License.
# *-------------------------------------------------------------------------------
# * For more information about the OpenAirInterface (OAI) Software Alliance:
# *      contact@openairinterface.org
# */


###########################################
# macros to define options as there is numerous options in oai
################################################
macro(add_option name val helpstr)
  if(DEFINED ${name})
    set(value ${${name}})
  else(DEFINED ${name})
    set(value ${val})
  endif()
  set(${name} ${value} CACHE STRING "${helpstr}")
  add_definitions("-D${name}=${value}")
endmacro(add_option)

macro(add_boolean_option name val helpstr)
  if(DEFINED ${name})
    set(value ${${name}})
  else(DEFINED ${name})
    set(value ${val})
  endif()
  set(${name} ${value} CACHE STRING "${helpstr}")
  set_property(CACHE ${name} PROPERTY TYPE BOOL)
  if (${value})
    add_definitions("-D${name}")
  endif (${value})
endmacro(add_boolean_option)

macro(add_integer_option name val helpstr)
  if(DEFINED ${name})
    set(value ${${name}})
  else(DEFINED ${name})
    set(value ${val})
  endif()
  set(${name} ${value} CACHE STRING "${helpstr}")
  add_definitions("-D${name}=${value}")
endmacro(add_integer_option)

macro(add_list1_option name val helpstr)
  if(DEFINED ${name})
    set(value ${${name}})
  else(DEFINED ${name})
    set(value ${val})
  endif()
  set(${name} ${value} CACHE STRING "${helpstr}")
  set_property(CACHE ${name} PROPERTY STRINGS ${ARGN})
  if(NOT "${value}" STREQUAL "False")
    add_definitions("-D${name}=${value}")
  endif()
endmacro(add_list1_option)

macro(add_list2_option name val helpstr)
  if(DEFINED ${name})
    set(value ${${name}})
  else(DEFINED ${name})
    set(value ${val})
  endif()
  set(${name} ${value} CACHE STRING "${helpstr}")
  set_property(CACHE ${name} PROPERTY STRINGS ${ARGN})
  if(NOT "${value}" STREQUAL "False")
    add_definitions("-D${value}=1")
  endif()
endmacro(add_list2_option)

macro(add_list_string_option name val helpstr)
  if(DEFINED ${name})
    set(value ${${name}})
  else(DEFINED ${name})
    set(value ${val})
  endif()
  set(${name} ${value} CACHE STRING "${helpstr}")
  set_property(CACHE ${name} PROPERTY STRINGS ${ARGN})
  if(NOT "${value}" STREQUAL "False")
    add_definitions("-D${name}=\"${value}\"")
  endif()
endmacro(add_list_string_option)


#
#
#
#
function(dkms_make_driver_
    _subdir
    _module
    _ver
    _kernelver 
    _compiler
    )
  
  set(_pck_name oai-${_module}) ##TODO: PTZ191020 here oai or oai-${_module} ?  
  set(_source_tree  ${PROJECT_BINARY_DIR}/src/ep_oai_${_module}_${_kernelver}-build)
  set(_dkms_tree    ${PROJECT_BINARY_DIR}/var/lib/dkms)
  set(_install_tree ${PROJECT_BINARY_DIR}/lib/modules)
  set(${_module}_KO ${_install_tree}/${_kernelver}/updates/dkms/oai-${_module}.ko PARENT_SCOPE)

  string(REGEX REPLACE "^.*-" "" _arch ${_kernelver})

  if(CMAKE_SYSTEM_PROCESSOR STREQUAL ${_arch})
    #we are not cross compiling.
  elseif(CMAKE_SYSTEM_VERSION STREQUAL ${_kernelver})
    #ie. CMAKE_SYSTEM_VERSION 4.7.0-1-amd64 $(kernelver)
    #we are really not cross compiling.
  endif()

  ##TODO: PTZ161018 less efficient inside this function...
  set(_cppflags "")
  get_directory_property(_c COMPILE_DEFINITIONS)
  list(REMOVE_ITEM _c "USER_MODE")
  list(FIND  _c RTAI _with_RTAI)
  if(_with_RTAI)
    list(APPEND _c "NAS_NETLINK")
  else()
    list(REMOVE_ITEM _c "NAS_NETLINK")
  endif()
  foreach(_d ${_c})
    string(CONCAT _cppflags ${_cppflags} " -D" ${_d})
  endforeach()
  set(_cflags "")
  get_directory_property(_c INCLUDE_DIRECTORIES)
  foreach(_d ${_c})
    string(CONCAT _cflags ${_cflags} " -I" ${_d})
  endforeach()


  ##TODO: PTZ161019 use ${_dkms_tree}/${_module}/${_ver}/build instead ?
  # _source_tree is a bad name in this context.
  ##TODO: PTZ161019 I gave up? nop...
  #configure_file(${_subdir}/dkms.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/dkms.conf)
  #configure_file(${_subdir}/Makefile.in  ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/Makefile)
  #configure_file(${_subdir}/dkms.conf.in ${_source_tree}/${_pck_name}-${_ver}/dkms.conf)
  #configure_file(${_subdir}/Makefile.in  ${_source_tree}/${_pck_name}-${_ver}/Makefile)
  file(MAKE_DIRECTORY  ${_source_tree}/${_pck_name}-${_ver})
  foreach(_f dkms.conf Makefile Kbuild ${_pck_name}.Kbuild)
    configure_file(${_subdir}/${_f}.in  ${_source_tree}/${_pck_name}-${_ver}/${_f})
  endforeach()
  set(DKMS_CMD dkms CACHE STRING "dkms command")

  set(DKMS_CONFIG
    ##TODO: PTZ161018 could be needed with cross compiling!
    #--kernelsourcedir _root_/lib/modules/$kernelver/build
    #--verbose
    --sourcetree  ${_source_tree}  
    --dkmstree    ${_dkms_tree}
    --installtree ${_install_tree}
    ##TODO:PTZ161019 instead --dkmsframework ${_source_tree}/framework.conf
    #--arch ${_arch}
    #-c ${_source_tree}/dkms.conf
    -k ${_kernelver}
    -v ${_ver}
    )

  ExternalProject_Add(ep_oai_${_module}_${_kernelver}
    PREFIX ${PROJECT_BINARY_DIR}
    SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}
    ##TODO: PTZ161019 not build yet ? DEPENDS Kbuild ${_source_tree}/Makefile ${_source_tree}/dkms.conf
    #DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/Kbuild
#
##TODO: PTZ161019
#CMAKE_GENERATOR <gen>
#Specify generator for native build
#CMAKE_GENERATOR_PLATFORM <platform>
#Generator-specific platform name
#CMAKE_GENERATOR_TOOLSET <toolset>
#Generator-specific toolset name
#CMAKE_ARGS <arg>...
#
    UPDATE_COMMAND ""
#    CONFIGURE_COMMAND ln -snfv ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir} ${_source_tree}/${_module}-${_ver}
#    # move makefile and dkms.conf
#    && install -v ${_source_tree}/dkms.conf ${_source_tree}/Makefile ${_dkms_tree}/${_module}/${_ver}/build
    #command executed in ${_source_tree}/
    # move makefile and dkms.conf
    #&& install -v dkms.conf Makefile ${_dkms_tree}/${_module}/${_ver}/build
    CONFIGURE_COMMAND ln -snfv ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir} ${_pck_name}-${_ver}/oai-${_module}

    ##TODO: PTZ161019 BINARY_DIR ${_dkms_tree}/${_module}/${_ver}/build ?
    ##TODO: PTZ161019 BINARY_DIR ${_dkms_tree}/${_module}/${_ver} would be better for this stage !?
    ##TODO: PTZ161020 BINARY_DIR ${_source_tree}
    BUILD_COMMAND     ${DKMS_CMD} build   ${_pck_name} ${DKMS_CONFIG}

    ##TODO: PTZ161020 Error! You must be root to use this command.
    INSTALL_COMMAND   sudo ${DKMS_CMD} install ${_pck_name} ${DKMS_CONFIG}

    BUILD_BYPRODUCTS ${${_module}_KO}
    COMMENT "${_pck_name}.ko for ${_kernel}  done"
    )
  
  add_custom_target(${_module} DEPENDS ep_oai_${_module}_${_kernelver})


  ##TODO: PTZ161020 not good, is there an alternative with install()?
  file(MAKE_DIRECTORY   ${_install_tree}/${_kernelver})

  
endfunction(dkms_make_driver_
    _subdir
    _module
    _ver
    _kernelver 
    _compiler
    )




function(dkms_cmake_a_driver
    _subdir
    _module
    _ver
    _kernelver
    _compiler
    _source_tree
    _dkms_tree
    _install_tree
    )
  
  set(_pck_name oai-${_module}) ##TODO: PTZ191020 here oai or oai-${_module} ?  
  #set(_source_tree  ${CMAKE_CURRENT_BINARY_DIR}/ep_oai_dkms CACHE PATH "dkms source tree")
  #set(_dkms_tree    ${PROJECT_BINARY_DIR}/var/lib/dkms CACHE PATH "dkms tree path")
  #set(_install_tree ${PROJECT_BINARY_DIR}/lib/modules  CACHE PATH "dkms install tree path")
  ##TODO: PTZ161019
  set(${_module}_KO ${_install_tree}/${_kernelver}/updates/dkms/oai-${_module}.ko PARENT_SCOPE)

  string(REGEX REPLACE "^.*-" "" _arch ${_kernelver})

  if(CMAKE_SYSTEM_PROCESSOR STREQUAL ${_arch})
    #we are not cross compiling.
  elseif(CMAKE_SYSTEM_VERSION STREQUAL ${_kernelver})
    #ie. CMAKE_SYSTEM_VERSION 4.7.0-1-amd64 $(kernelver)
    #we are really not cross compiling.
  endif()

  ##TODO: PTZ161019 use ${_dkms_tree}/${_module}/${_ver}/build instead ?
  # _source_tree is a bad name in this context.
  ##TODO: PTZ161019 I gave up? nop...
  #configure_file(${_subdir}/dkms.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/dkms.conf)
  #configure_file(${_subdir}/Makefile.in  ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/Makefile)
  #configure_file(${_subdir}/dkms.conf.in ${_source_tree}/${_pck_name}-${_ver}/dkms.conf)
  #configure_file(${_subdir}/Makefile.in  ${_source_tree}/${_pck_name}-${_ver}/Makefile)
  #file(MAKE_DIRECTORY  ${_source_tree}/${_pck_name}-${_ver}/oai-${_module})
  file(MAKE_DIRECTORY  ${_source_tree}/oai-${_module})
  foreach(_f ${_pck_name}.Kbuild)
    configure_file(${_subdir}/${_f}.in ${_source_tree}/oai-${_module}/${_f})
  endforeach()
  
  file(GLOB _src
    #LIST_DIRECTORIES false
    ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.[ch]
    ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.Kbuild
    )
  foreach(_f ${_src})
    configure_file(${_f} ${_source_tree}/oai-${_module} COPYONLY)
  endforeach()

#   ExternalProject_Add(ep_oai_${_module}_${_kernelver}
#     PREFIX ${PROJECT_BINARY_DIR}
#     SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}
#     #SOURCE_DIR ${_subdir}
#     #SOURCE_DIR ${_pck_name}-${_ver}
#     ##TODO: PTZ161019 not build yet ? DEPENDS Kbuild ${_source_tree}/Makefile ${_source_tree}/dkms.conf
#     #DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/Kbuild
# #
# ##TODO: PTZ161019
# #CMAKE_GENERATOR <gen>
# #Specify generator for native build
# #CMAKE_GENERATOR_PLATFORM <platform>
# #Generator-specific platform name
# #CMAKE_GENERATOR_TOOLSET <toolset>
# #Generator-specific toolset name
# #CMAKE_ARGS <arg>...
# #UPDATE_COMMAND <cmd>...
#     #PTZ161024 have to prefre a copy.. not good for debugging.
#     #DOWNLOAD_COMMAND install --verbose -d ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.{c,h} ${_pck_name}-${_ver}/oai-${_module}
#     #UPDATE_COMMAND install --verbose -d ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir}/*.{c,h} ${_pck_name}-${_ver}/oai-${_module}
#     #in SOURCE_DIR
#     UPDATE_COMMAND mkdir -vp ${_source_tree}/${_pck_name}-${_ver}/oai-${_module}
#     UPDATE_COMMAND && install --verbose ${_src} ${_source_tree}/oai-${_module}
#     #UPDATE_COMMAND && cp -auv *.c *.h ${_source_tree}/${_pck_name}-${_ver}/oai-${_module}
#     #CONFIGURE_COMMAND ln -snfv ${CMAKE_CURRENT_SOURCE_DIR}/${_subdir} ${_pck_name}-${_ver}/oai-${_module}
#     CONFIGURE_COMMAND ""
#     ##TODO: PTZ161020
#     BINARY_DIR ${_source_tree}
#     BUILD_COMMAND   ${DKMS_CMD} build ${_pck_name} ${DKMS_CONFIG}
#     INSTALL_COMMAND sudo ${DKMS_CMD} install ${_pck_name} ${DKMS_CONFIG}
#     BUILD_BYPRODUCTS ${${_module}_KO}
#     COMMENT "${_pck_name}.ko for ${_kernel}  done"
#     )
  #add_custom_target(${_module} DEPENDS ep_${_module}_${_kernelver})
  add_custom_target(${_module} DEPENDS ep_oai_${_kernelver})

  ##TODO: PTZ161020 not good, is there an alternative with install()?
  #file(MAKE_DIRECTORY   ${_install_tree}/${_kernelver})
  
endfunction(dkms_cmake_a_driver
    _subdir
    _module
    _ver
    _kernelver 
    _compiler
    _source_tree
    _dkms_tree
    _install_tree
    )


###
