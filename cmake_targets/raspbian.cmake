
#http://archive.raspbian.org/
#Your /etc/apt/sources.list file should look as follows:
#deb http://archive.raspbian.org/raspbian wheezy main contrib non-free rpi
#deb-src http://archive.raspbian.org/raspbian wheezy main contrib non-free rpi
#
#When using apt-get, you will will want to install the Raspbian public key in your apt-get keyring, it should already be there if you used debootstrap from the Raspbian repository but if you need to add it manually this can be done with following command:
#
#wget http://archive.raspbian.org/raspbian.public.key -O - | sudo apt-key add -

#list of packages
#http://archive.raspbian.org/raspbian/dists/wheezy/main/binary-armhf/Packages
