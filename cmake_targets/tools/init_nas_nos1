#!/bin/bash
#/*
# * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
# * contributor license agreements.  See the NOTICE file distributed with
# * this work for additional information regarding copyright ownership.
# * The OpenAirInterface Software Alliance licenses this file to You under
# * the OAI Public License, Version 1.0  (the "License"); you may not use this file
# * except in compliance with the License.
# * You may obtain a copy of the License at
# *
# *      http://www.openairinterface.org/?page_id=698
# *
# * Unless required by applicable law or agreed to in writing, software
# * distributed under the License is distributed on an "AS IS" BASIS,
# * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# * See the License for the specific language governing permissions and
# * limitations under the License.
# *-------------------------------------------------------------------------------
# * For more information about the OpenAirInterface (OAI) Software Alliance:
# *      contact@openairinterface.org
# */

set -x

# file init_nas_nos1
# brief loads the nasmesh module and sets up the radio bearers (used to provide ip interface without S1 interface)
# author Florian Kaltenberger
#
#######################################
ORIGIN_PATH=$PWD
THIS_SCRIPT_PATH=$(dirname $(readlink -f $0))
OPENAIR_DIR=${OPENAIR_DIR:-${PROJECT_SOURCE_DIR}}
OPENAIR_DIR=${OPENAIR_DIR:-$(echo "${THIS_SCRIPT_PATH}" | sed 's,/cmake_targets.*,,g')}
PROJECT_BINARY_DIR=${PROJECT_BINARY_DIR:-${OPENAIR_DIR}/bld}
OPENAIR_BUILD_d=${PROJECT_BINARY_DIR}
if [ -f ${OPENAIR_DIR}/cmake_targets/tools/build_helper ]
then
    source ${OPENAIR_DIR}/cmake_targets/tools/build_helper
    source ${OPENAIR_DIR}/cmake_targets/tools/test_helper
fi
## TODO: PTZ161021 now in build_helper.bash
load_module() {
  local mod_name=${1##*/}
  mod_name=${mod_name%.*}
  if awk "/$mod_name/ {found=1 ;exit} END {if (found!=1) exit 1}" /proc/modules
    then
      echo "module $mod_name already loaded: I remove it first"
      sudo rmmod $mod_name
  fi
  echo loading $mod_name
  sudo insmod $1
}

## TODO: PTZ161021 other archs...
_karch=$(uname -r)
_m_ko_p=$OPENAIR_DIR/targets/bin/nasmesh.ko
[ -f ${_m_ko_p} ] || _m_ko_p=${OPENAIR_BUILD_d}/lib/modules/${_karch}/driver/net/wireless/oai-nasmesh.ko
[ -f ${_m_ko_p} ] || _m_ko_p=${OPENAIR_BUILD_d}/lib/modules/${_karch}/updates/dkms/oai-nasmesh.ko
[ -f ${_m_ko_p} ] || _m_ko_p=${OPENAIR_BUILD_d}/var/lib/dkms/oai-nasmesh/1-PTZ160803_x86_emu/${_karch}/x86_64/module/oai-nasmesh.ko

load_module ${_m_ko_p}

_rb_tool_p=${OPENAIR_DIR}/targets/bin/rb_tool
[ -x ${_rb_tool_p} ] || _rb_tool_p=${OPENAIR_BUILD_d}/openair2/rb_tool
[ -x ${_rb_tool_p} ] || _rb_tool_p=${OPENAIR_BUILD_d}/bin/rb_tool
[ -x ${_rb_tool_p} ] || _rb_tool_p=${OPENAIR_BUILD_d}/bin/nas_mesh_rb_tool

if [ "$1" = "eNB" ]; then 
     echo "bring up oai0 interface for enb"
     sudo ifconfig oai0 10.0.1.1 netmask 255.255.255.0 broadcast 10.0.1.255
      ${_rb_tool_p} -a -c0 -i0 -z0 -s 10.0.1.1 -t 10.0.1.9 -r 1
else
    if [ "$1" = "UE" ]; then 
       echo "bring up oai0 interface for UE"
       sudo ifconfig oai0 10.0.1.9 netmask 255.255.255.0 broadcast 10.0.1.255
        ${_rb_tool_p} -a -c0 -i0 -z0 -s 10.0.1.9 -t 10.0.1.1 -r 1
    fi
fi
