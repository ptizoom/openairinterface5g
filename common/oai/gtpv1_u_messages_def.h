/*******************************************************************************
    OpenAirInterface
    Copyright(c) 1999 - 2015 Eurecom

    OpenAirInterface is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.


    OpenAirInterface is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenAirInterface.The full GNU General Public License is
   included in this distribution in the file called "COPYING". If not,
   see <http://www.gnu.org/licenses/>.

  Contact Information
  OpenAirInterface Admin: openair_admin@eurecom.fr
  OpenAirInterface Tech : openair_tech@eurecom.fr
  OpenAirInterface Dev  : openair4g-devel@lists.eurecom.fr

  Address      : Eurecom, Campus SophiaTech, 450 Route des Chappes, CS 50193 - 06904 Biot Sophia Antipolis cedex, FRANCE

*******************************************************************************/

MESSAGE_DEF(GTPV1U_ENB_UPDATE_TUNNEL_REQ,   MESSAGE_PRIORITY_MED, gtpv1u_enb_update_tunnel_req_t,  Gtpv1uUpdateTunnelReq)
MESSAGE_DEF(GTPV1U_ENB_UPDATE_TUNNEL_RESP,  MESSAGE_PRIORITY_MED, gtpv1u_enb_update_tunnel_resp_t, Gtpv1uUpdateTunnelResp)
MESSAGE_DEF(GTPV1U_ENB_DELETE_TUNNEL_REQ,   MESSAGE_PRIORITY_MED, gtpv1u_enb_delete_tunnel_req_t,  Gtpv1uDeleteTunnelReq)
MESSAGE_DEF(GTPV1U_ENB_DELETE_TUNNEL_RESP,  MESSAGE_PRIORITY_MED, gtpv1u_enb_delete_tunnel_resp_t, Gtpv1uDeleteTunnelResp)
MESSAGE_DEF(GTPV1U_ENB_TUNNEL_DATA_IND,     MESSAGE_PRIORITY_MED, gtpv1u_enb_tunnel_data_ind_t,    Gtpv1uTunnelDataInd)
MESSAGE_DEF(GTPV1U_ENB_TUNNEL_DATA_REQ,     MESSAGE_PRIORITY_MED, gtpv1u_enb_tunnel_data_req_t,    Gtpv1uTunnelDataReq)

MESSAGE_DEF(GTPV1U_CREATE_TUNNEL_REQ,   MESSAGE_PRIORITY_MED, Gtpv1uCreateTunnelReq,  gtpv1uCreateTunnelReq)
MESSAGE_DEF(GTPV1U_CREATE_TUNNEL_RESP,  MESSAGE_PRIORITY_MED, Gtpv1uCreateTunnelResp, gtpv1uCreateTunnelResp)
MESSAGE_DEF(GTPV1U_UPDATE_TUNNEL_REQ,   MESSAGE_PRIORITY_MED, Gtpv1uUpdateTunnelReq,  gtpv1uUpdateTunnelReq)
MESSAGE_DEF(GTPV1U_UPDATE_TUNNEL_RESP,  MESSAGE_PRIORITY_MED, Gtpv1uUpdateTunnelResp, gtpv1uUpdateTunnelResp)
MESSAGE_DEF(GTPV1U_DELETE_TUNNEL_REQ,   MESSAGE_PRIORITY_MED, Gtpv1uDeleteTunnelReq,  gtpv1uDeleteTunnelReq)
MESSAGE_DEF(GTPV1U_DELETE_TUNNEL_RESP,  MESSAGE_PRIORITY_MED, Gtpv1uDeleteTunnelResp, gtpv1uDeleteTunnelResp)
MESSAGE_DEF(GTPV1U_TUNNEL_DATA_IND,     MESSAGE_PRIORITY_MED, Gtpv1uTunnelDataInd,    gtpv1uTunnelDataInd)
MESSAGE_DEF(GTPV1U_TUNNEL_DATA_REQ,     MESSAGE_PRIORITY_MED, Gtpv1uTunnelDataReq,    gtpv1uTunnelDataReq)
