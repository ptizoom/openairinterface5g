/*******************************************************************************
    OpenAirInterface
    Copyright(c) 1999 - 2014 Eurecom

    OpenAirInterface is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.


    OpenAirInterface is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenAirInterface.The full GNU General Public License is
   included in this distribution in the file called "COPYING". If not,
   see <http://www.gnu.org/licenses/>.

  Contact Information
  OpenAirInterface Admin: openair_admin@eurecom.fr
  OpenAirInterface Tech : openair_tech@eurecom.fr
  OpenAirInterface Dev  : openair4g-devel@lists.eurecom.fr

  Address      : Eurecom, Campus SophiaTech, 450 Route des Chappes, CS 50193 - 06904 Biot Sophia Antipolis cedex, FRANCE

*******************************************************************************/

// will be called many times, in same file... 
//#ifndef _MESSAGES_DEF_H_ 
//#define _MESSAGES_DEF_H_
#ifndef _COMMON_OAI_
#define _COMMON_OAI_
#endif
// TODO: PTZ160826  NOP not going to work, need to call  end directly.
//or use  lots of macro variates.
// #include "openair2/COMMON/messages_def.h"

// These messages files are mandatory and must always be placed in first position
#include "intertask_messages_def.h"
#include "timer_messages_def.h"

// Messages files used between tasks
#include "openair2/COMMON/phy_messages_def.h"
#include "openair2/COMMON/mac_messages_def.h"
#include "openair2/COMMON/rlc_messages_def.h"
#include "openair2/COMMON/pdcp_messages_def.h"
#include "openair2/COMMON/s1ap_messages_def.h"
#include "openair2/COMMON/rrc_messages_def.h"
#include "openair2/COMMON/nas_messages_def.h"
#if ENABLE_RAL
#include "openair2/COMMON/ral_messages_def.h"
#endif
#include "openair2/COMMON/x2ap_messages_def.h"
#include "openair2/COMMON/sctp_messages_def.h"
#include "openair2/COMMON/udp_messages_def.h"
#include "openair2/COMMON/gtpv1_u_messages_def.h"


//#include "openair3/COMMON/messages_def.h"
//..
// Messages files used between tasks
#include "openair3/COMMON/gtpv1_u_messages_def.h"
// TODO: #include "ip_forward_messages_def.h"
#include "openair3/COMMON/nas_messages_def.h"
// TODO: #include "s11_messages_def.h"
#include "openair3/COMMON/s1ap_messages_def.h"
// TODO: #include "s6a_messages_def.h"
#include "openair3/COMMON/sctp_messages_def.h"
// TODO: #include "sgw_lite_def.h"
#include "openair3/COMMON/udp_messages_def.h"
// TODO: #include "mme_app_messages_def.h"



//#endif

