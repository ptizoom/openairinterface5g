/*******************************************************************************
    OpenAirInterface
    Copyright(c) 1999 - 2014 Eurecom

    OpenAirInterface is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.


    OpenAirInterface is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with OpenAirInterface.The full GNU General Public License is
   included in this distribution in the file called "COPYING". If not,
   see <http://www.gnu.org/licenses/>.

  Contact Information
  OpenAirInterface Admin: openair_admin@eurecom.fr
  OpenAirInterface Tech : openair_tech@eurecom.fr
  OpenAirInterface Dev  : openair4g-devel@lists.eurecom.fr

  Address      : Eurecom, Campus SophiaTech, 450 Route des Chappes, CS 50193 - 06904 Biot Sophia Antipolis cedex, FRANCE

*******************************************************************************/
#ifndef _COMMON_OAI_
#define _COMMON_OAI_
#endif

#include "openair2/COMMON/s1ap_messages_def.h"
#include "openair3/COMMON/sa1p_messages_def.h"


#if 0
#if defined  S1AP_UPLINK_NAS_LOG   
#undef S1AP_UPLINK_NAS_LOG
#undef S1AP_UE_CAPABILITY_IND_LOG
#undef S1AP_INITIAL_CONTEXT_SETUP_LOG
#undef S1AP_NAS_NON_DELIVERY_IND_LOG
#undef S1AP_DOWNLINK_NAS_LOG
#undef S1AP_S1_SETUP_LOG
#undef S1AP_INITIAL_UE_MESSAGE_LOG
#undef S1AP_UE_CONTEXT_RELEASE_REQ_LOG
#undef S1AP_UE_CONTEXT_RELEASE_COMMAND_LOG
#undef S1AP_UE_CAPABILITIES_IND
#undef S1AP_UE_CONTEXT_RELEASE_REQ
#undef S1AP_UE_CONTEXT_RELEASE_COMMAND
#undef S1AP_UE_CONTEXT_RELEASE_COMPLETE
#endif
#if defined  SCTP_INIT_MS
#undef SCTP_INIT_MS
#undef SCTP_DATA_REQ
#undef SCTP_DATA_IND
#undef SCTP_CLOSE_ASSOCIATION
#endif
#if defined  UDP_INIT
#undef UDP_INIT
#undef UDP_DATA_REQ
#undef UDP_DATA_IND
#endif
#endif
