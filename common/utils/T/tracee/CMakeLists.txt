include_directories(
  "${CMAKE_CURRENT_BINARY_DIR}/.."
  #"${PROJECT_BINARY_DIR}/common/utils/T"
  "${CMAKE_CURRENT_SOURCE_DIR}/.."
  )

#-Wall -g -pthread -DT_TRACER -I.

add_executable(tracee
  tracee.c
#/usr/src/openairinterface5g/common/utils/T/tracee/tracee.c:9 : référence indéfinie vers « T_init »
# in T.c
  ${T_SOURCE}
  )

add_dependencies(tracee generate_T)

#PTZ170211 needed for event_id_from_name() get_format 
target_link_libraries(tracee
  oai_tracer
  #Threads::Threads
#/usr/bin/ld: CMakeFiles/tracee.dir/usr/src/openairinterface5g/common/utils/T/T.c.o: référence au symbole non défini «shm_unlink@@GLIBC_2.2.5»
#//lib/x86_64-linux-gnu/librt.so.1: error adding symbols: DSO missing from command line
  ${T_LIB}
  )

install(PROGRAMS tracee
  EXPORT openairinterface-targets
  DESTINATION share/oai/contrib
  COMPONENT dev
)
