#include "view.h"
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

//TODO:PTZ170210 stdout to _stdout, bizarre 
struct _stdout {
  view common;
  pthread_mutex_t lock;
};

static void clear(view *this)
{
  /* do nothing */
}

static void append(view *_this, char *s)
{
  struct _stdout *this = (struct _stdout *)_this;
  if (pthread_mutex_lock(&this->lock)) abort();
  printf("%s\n", s);
  if (pthread_mutex_unlock(&this->lock)) abort();
}

view *new_view_stdout(void)
{
  struct _stdout *ret = calloc(1, sizeof(struct _stdout));
  if (ret == NULL) abort();

  ret->common.clear = clear;
  ret->common.append = (void (*)(view *, ...))append;

  if (pthread_mutex_init(&ret->lock, NULL)) abort();

  return (view *)ret;
}
