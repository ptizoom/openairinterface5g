/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.0  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

#ifndef _SIMULATION_LTE_PHY_UNITARY_DEFS_H_
#define _SIMULATION_LTE_PHY_UNITARY_DEFS_H_

openair0_device openair0;

#if 0
#if 1
#include "SCHED/vars.h"
#else
volatile int oai_exit = 0;
void exit_fun(const char *s) {
    printf("Exiting: %s\n",s);
    oai_exit = 1;
    //exit(-1);
}
#endif
#endif


extern unsigned int dlsch_tbs25[27][25],TBStable[27][110];
extern unsigned char offset_mumimo_llr_drange_fix;

extern unsigned short dftsizes[33];
extern short *ul_ref_sigs[30][2][33];


#endif
