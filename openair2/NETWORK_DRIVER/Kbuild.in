###########################################################################
# Kbuild file for OAI NAS MESH Linux kernel modules     --*-- Makefile --*--
###########################################################################

#
# The parent makefile is expected to define:
#
# OAI_KERNEL_SOURCES : The root of the kernel source tree.
# OAI_KERNEL_OUTPUT : The kernel's output tree.
# OAI_KERNEL_MODULES : A whitespace-separated list of modules to build.
#
# Kbuild provides the variables:
#
# $(src) : The directory containing this Kbuild file.
# $(obj) : The directory where the output from this build is written.
#

#
# Utility macro ASSIGN_PER_OBJ_CFLAGS: to control CFLAGS on a
# per-object basis, Kbuild honors the 'CFLAGS_$(object)' variable.
# E.g., "CFLAGS_nv.o" for CFLAGS that are specific to nv.o. Use this
# macro to assign 'CFLAGS_$(object)' variables for multiple object
# files.
#
# $(1): The object files.
# $(2): The CFLAGS to add for those object files.
#

ASSIGN_PER_OBJ_CFLAGS = \
 $(foreach _cflags_variable, \
 $(addprefix CFLAGS_,$(notdir $(1))), \
 $(eval $(_cflags_variable) += $(2)))


#
# Include the specifics of the individual OAI kernel modules.
#
# Each of these should:
# - Append to 'obj-m', to indicate the kernel module that should be built.
# - Define the object files that should get built to produce the kernel module.
# - Tie into conftest (see the description below).
# - Add any items that should always be built to $(always)
#

always :=

$(foreach _module, $(OAI_KERNEL_MODULES), \
 $(eval include $(src)/$(_module)/$(_module).Kbuild))

#DEFINES  += @_cppflags@
#INCLUDES += @_cflags@

ccflags-y += @_cppflags@
ccflags-y += @_cflags@

#
# Define CFLAGS that apply to all the OAI kernel modules. EXTRA_CFLAGS
# is deprecated since 2.6.24 in favor of ccflags-y, but we need to support
# older kernels which do not have ccflags-y. Newer kernels append
# $(EXTRA_CFLAGS) to ccflags-y for compatibility.
#
#EXTRA_CFLAGS += -I@OPENAIR2_DIR@
#EXTRA_CFLAGS += -I$(src)
EXTRA_CFLAGS += -Wall -MD $(DEFINES) $(INCLUDES)
#
#EXTRA_CFLAGS += -I$(src)/common/inc
#EXTRA_CFLAGS += -Wsign-compare -Wno-cast-qual -Wno-error
EXTRA_CFLAGS += -D__KERNEL__
#EXTRA_CFLAGS += -DMODULE -DNVRM
#EXTRA_CFLAGS += -DOAI_VERSION_STRING=\"367.44\"
#EXTRA_CFLAGS += -Wno-unused-function -Wuninitialized -fno-strict-aliasing -mno-red-zone -mcmodel=kernel
#EXTRA_CFLAGS += -DOAI_UVM_ENABLE
#EXTRA_CFLAGS += -Wno-sign-compare -Wno-format-extra-args
#
# Detect SGI UV systems and apply system-specific optimizations.
#
#ifneq ($(wildcard /proc/sgi_uv),)
# EXTRA_CFLAGS += -DOAI_CONFIG_X86_UV
#endif

#
##TODO: PTZ161019

EXTRA_CFLAGS += \
	$(patsubst CONFIG_%, -DCONFIG_%=1, $(patsubst %=m,%,$(filter %=m,$(EXTRA_KCONFIG)))) \
	$(patsubst CONFIG_%, -DCONFIG_%=1, $(patsubst %=y,%,$(filter %=y,$(EXTRA_KCONFIG))))


##TODO: PTZ161020
#EXTRA_CFLAGS += -fno-common -fno-stack-protector -mpreferred-stack-boundary=4
#EXTRA_CFLAGS += $(if $(SET_X64),-DARCH_64,) $(if $(SET_X64),-mcmodel=kernel,) $(if $(SET_X64),-m64,) 
#EXTRA_CFLAGS += -I@OPENAIR2_DIR@


#
# The conftest.sh script tests various aspects of the target kernel.
# The per-module Kbuild files included above should:
#
# - Append to the OAI_CONFTEST_*_COMPILE_TESTS variables to indicate
# which conftests they require.
# - Append to the OAI_OBJECTS_DEPEND_ON_CONFTEST variable any object files
# that depend on conftest.
#
# The conftest machinery below will run the requested tests and
# generate the appropriate header files.
#

CC ?= cc
HOST_CC ?= $(CC)
LD ?= ld

OAI_CONFTEST_SCRIPT := $(src)/conftest.sh
OAI_CONFTEST_HEADER := $(obj)/conftest/headers.h
OAI_CONFTEST_SCRIPT := ${CMAKE_CURRENT_SOURCE_DIR}/conftest.sh
OAI_CONFTEST_HEADER := ${CMAKE_CURRENT_SOURCE_DIR}/conftest/headers.h

OAI_CONFTEST_CMD := /bin/sh $(OAI_CONFTEST_SCRIPT) \
 "$(CC)" "$(HOST_CC)" $(ARCH) $(OAI_KERNEL_SOURCES) $(OAI_KERNEL_OUTPUT)

OAI_CONFTEST_CFLAGS := $(shell $(OAI_CONFTEST_CMD) build_cflags)
OAI_CONFTEST_CFLAGS += $(filter-out -Werror%,$(KBUILD_CFLAGS))

##TODO: PTZ161026 /oai-nasmesh is not good.
OAI_CONFTEST_COMPILE_TEST_HEADERS := $(obj)/oai-nasmesh/conftest/macros.h
OAI_CONFTEST_COMPILE_TEST_HEADERS += $(obj)/oai-nasmesh/conftest/functions.h
OAI_CONFTEST_COMPILE_TEST_HEADERS += $(obj)/oai-nasmesh/conftest/symbols.h
OAI_CONFTEST_COMPILE_TEST_HEADERS += $(obj)/oai-nasmesh/conftest/types.h
OAI_CONFTEST_COMPILE_TEST_HEADERS += $(obj)/oai-nasmesh/conftest/generic.h
OAI_CONFTEST_COMPILE_TEST_HEADERS :=
OAI_CONFTEST_HEADERS := $(obj)/oai-nasmesh/conftest/patches.h
OAI_CONFTEST_HEADERS += $(obj)/oai-nasmesh/conftest/headers.h
OAI_CONFTEST_HEADERS += $(OAI_CONFTEST_COMPILE_TEST_HEADERS)
OAI_CONFTEST_HEADERS :=

.PHONY: conftest-verbose
conftest-verbose:
	@echo 'OAI_CONFTEST_CMD=$(OAI_CONFTEST_CMD)'
	@echo 'OAI_CONFTEST_CFLAGS=$(OAI_CONFTEST_CFLAGS)'
	@echo 'KBUILD_CFLAGS=$(KBUILD_CFLAGS)'
	@echo 'LINUXINCLUDE=$(LINUXINCLUDE)'
	@echo 'LDFLAGS=$(LDFLAGS)'
	@echo 'ARCH=$(ARCH)'

#
# Generate a header file for a single conftest compile test. Each compile test
# header depends on conftest.sh, as well as the generated conftest/headers.h
# file, which is included in the compile test preamble.
#

$(obj)/conftest/compile-tests/%.h: $(OAI_CONFTEST_SCRIPT) $(OAI_CONFTEST_HEADER)
	@mkdir -p $(obj)/conftest/compile-tests
	@echo " CONFTEST: $(notdir $*)"
	@$(OAI_CONFTEST_CMD) compile_tests '$(OAI_CONFTEST_CFLAGS)' \
	 $(notdir $*) > $@

#
# Concatenate a conftest/*.h header from its constituent compile test headers
#
# $(1): The name of the concatenated header
# $(2): The list of compile tests that make up the header
#

define OAI_GENERATE_COMPILE_TEST_HEADER
 $(obj)/conftest/$(1).h: $(addprefix $(obj)/conftest/compile-tests/,$(addsuffix .h,$(2)))
	@mkdir -p $(obj)/conftest
	@# concatenate /dev/null to prevent cat from hanging when $$^ is empty
	@cat $$^ /dev/null > $$@
	@echo " CONFTEST $$(notdir $$@):"
	@cat $$@
endef

#
# Generate the conftest compile test headers from the lists of compile tests
# provided by the module-specific Kbuild files.
#

OAI_CONFTEST_FUNCTION_COMPILE_TESTS ?=
OAI_CONFTEST_GENERIC_COMPILE_TESTS ?=
OAI_CONFTEST_MACRO_COMPILE_TESTS ?=
OAI_CONFTEST_SYMBOL_COMPILE_TESTS ?=
OAI_CONFTEST_TYPE_COMPILE_TESTS ?=

$(eval $(call OAI_GENERATE_COMPILE_TEST_HEADER,functions,$(OAI_CONFTEST_FUNCTION_COMPILE_TESTS)))
$(eval $(call OAI_GENERATE_COMPILE_TEST_HEADER,generic,$(OAI_CONFTEST_GENERIC_COMPILE_TESTS)))
$(eval $(call OAI_GENERATE_COMPILE_TEST_HEADER,macros,$(OAI_CONFTEST_MACRO_COMPILE_TESTS)))
$(eval $(call OAI_GENERATE_COMPILE_TEST_HEADER,symbols,$(OAI_CONFTEST_SYMBOL_COMPILE_TESTS)))
$(eval $(call OAI_GENERATE_COMPILE_TEST_HEADER,types,$(OAI_CONFTEST_TYPE_COMPILE_TESTS)))

$(obj)/conftest/patches.h: $(OAI_CONFTEST_SCRIPT) conftest-verbose
	@mkdir -p $(obj)/conftest
	@$(OAI_CONFTEST_CMD) patch_check > $@
	@echo " CONFTEST $(notdir $@):"
	@cat $@

$(obj)/conftest/headers.h: $(OAI_CONFTEST_SCRIPT) conftest-verbose
	@mkdir -p $(obj)/conftest
	@$(OAI_CONFTEST_CMD) test_kernel_headers > $@
	@echo " CONFTEST $(notdir $@):"
	@cat $@

clean-dirs := $(obj)/conftest


# For any object files that depend on conftest, declare the dependency here.
$(addprefix $(obj)/,$(OAI_OBJECTS_DEPEND_ON_CONFTEST)): | $(OAI_CONFTEST_HEADERS)
