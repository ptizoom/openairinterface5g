###########################################################################
# Kbuild fragment for oai-ue_ip.ko
###########################################################################

OAI_UE_IP_BUILD_TYPE = debug

MIN_VERSION    := 4
MIN_PATCHLEVEL := 7
MIN_SUBLEVEL   := 0

KERNEL_VERSION_NUMERIC := $(shell echo $$(( $(VERSION) * 65536 + $(PATCHLEVEL) * 256 + $(SUBLEVEL) )))
MIN_VERSION_NUMERIC    := $(shell echo $$(( $(MIN_VERSION) * 65536 + $(MIN_PATCHLEVEL) * 256 + $(MIN_SUBLEVEL) )))

KERNEL_NEW_ENOUGH_FOR_UE_IP := $(shell [ $(KERNEL_VERSION_NUMERIC) -ge $(MIN_VERSION_NUMERIC) ] && echo 1)

#
# Define OAI_UE_IP_{SOURCES,OBJECTS}
#

OAI_UE_IP_OBJECTS =
OAI_UE_IP_UNSUPPORTED_SOURCE := 

ifeq ($(KERNEL_NEW_ENOUGH_FOR_UE_IP),1)
include $(src)/oai-ue_ip/oai-ue_ip-sources.Kbuild
OAI_UE_IP_OBJECTS += $(patsubst %.c, %.o, $(filter-out $(OAI_UE_IP_UNSUPPORTED_SOURCE),$(OAI_UE_IP_SOURCES)))
else
OAI_UE_IP_SOURCES = $(OAI_UE_IP_UNSUPPORTED_SOURCE)
OAI_UE_IP_OBJECTS += $(patsubst %.c, %.o,$(OAI_UE_IP_SOURCES))
endif

# Some linux kernel functions rely on being built with optimizations on and
# to work around this we put wrappers for them in a separate file that's built
# with optimizations on in debug builds and skipped in other builds.
# Notably gcc 4.4 supports per function optimization attributes that would be
# easier to use, but is too recent to rely on for now.
OAI_UE_IP_DEBUG_OPTIMIZED_SOURCE := 
OAI_UE_IP_DEBUG_OPTIMIZED_OBJECT := $(patsubst %.c, %.o, $(OAI_UE_IP_DEBUG_OPTIMIZED_SOURCE))

ifneq ($(OAI_UE_IP_BUILD_TYPE),debug)
# Only build the wrappers on debug builds
OAI_UE_IP_OBJECTS := $(filter-out $(OAI_UE_IP_DEBUG_OPTIMIZED_OBJECT), $(OAI_UE_IP_OBJECTS))
endif

obj-m += oai-ue_ip.o
oai-ue_ip-y := $(OAI_UE_IP_OBJECTS)

OAI_UE_IP_KO = oai-ue_ip.ko

#
# Define oai-ue_ip.ko-specific CFLAGS.
#

ifneq ($(OAI_UE_IP_BUILD_TYPE),release)
OAI_UE_IP_CFLAGS += -DDEBUG -D_DEBUG_
OAI_UE_IP_CFLAGS += -DDEBUG_DEVICE

NAS_DEBUG_RECEIVE := 1
NAS_DEBUG_SEND := 1

ifneq ($(OAI_UE_IP_BUILD_TYPE),develop)
# -DDEBUG is required, in order to allow pr_devel() print statements to
# work:
OAI_UE_IP_CFLAGS += $(call cc-option,-Og,-O0) -g
OAI_UE_IP_CFLAGS += -DNAS_DEBUG_RECEIVE
OAI_UE_IP_CFLAGS += -DNAS_DEBUG_SEND
else
OAI_UE_IP_CFLAGS += -DOAI_UE_IP_DEVELOP
OAI_UE_IP_CFLAGS += -O2
endif
endif
ifneq ($(NAS_DEBUG_RECEIVE),)
OAI_UE_IP_CFLAGS += -DNAS_DEBUG_RECEIVE
endif
ifneq ($(NAS_DEBUG_SEND),)
OAI_UE_IP_CFLAGS += -DNAS_DEBUG_SEND
endif

OAI_UE_IP_CFLAGS += -DLinux
OAI_UE_IP_CFLAGS += -D__linux__
OAI_UE_IP_CFLAGS += -I$(src)/oai-ue_ip

# Avoid even building HMM until the HMM patch is in the upstream kernel.
# Bug 1772628 has details.
#OAI_]BUILD_SUPPORTS_HMM ?= 0
#ifeq ($(OAI_BUILD_SUPPORTS_HMM),1)
#  OAI_UE_IP_CFLAGS += -DOAI_BUILD_SUPPORTS_HMM
#endif

ifneq ($(PDCP_USE_NETLINK),)
OAI_UE_IP_CFLAGS += -DPDCP_USE_NETLINK
OAI_UE_IP_CFLAGS += -DOAI_NW_DRIVER_USE_NETLINK
OAI_UE_IP_CFLAGS += -URTAI
else
ifneq ($(RTAI),)
OAI_UE_IP_CFLAGS += $(shell rtai-config --module-cflags)
OAI_UE_IP_CFLAGS += -DRTAI -D__IN_RTAI__
endif
endif

ifneq ($(LOOPBACK),)
OAI_UE_IP_CFLAGS += -DLOOPBACK_TEST
endif

ifneq ($(ADDRESS_FIX),)
OAI_UE_IP_CFLAGS += -DNAS_ADDRESS_FIX
endif

ifneq ($(ADDRCONF),)
OAI_UE_IP_CFLAGS += -DADDRCONF
endif

ifneq ($(OAI_NW_DRIVER_TYPE_ETHERNET),)
OAI_UE_IP_CFLAGS += -DOAI_NW_DRIVER_TYPE_ETHERNET
OAI_UE_IP_CFLAGS += -D__KERNEL__
endif

$(call ASSIGN_PER_OBJ_CFLAGS, $(OAI_UE_IP_OBJECTS), $(OAI_UE_IP_CFLAGS))

ifeq ($(OAI_UE_IP_BUILD_TYPE),debug)
  # Force optimizations on for the wrappers
  $(call ASSIGN_PER_OBJ_CFLAGS, $(OAI_UE_IP_DEBUG_OPTIMIZED_OBJECT), $(OAI_UE_IP_CFLAGS) -O2)
endif


