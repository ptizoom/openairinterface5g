################################################################################
#   OpenAirInterface
#
#   X2AP sub project
#
#   called with add_subdirectories( ... )
#
# this config file parses the _ASN1C_SOURCES
# http://stackoverflow.com/questions/37703514/dynamically-generated-source-file-list-for-cmake?s=1%7C0.5794
#
find_package(Asn1c CONFIG REQUIRED)
set(_prj_rdx   x2ap)
set(_prj_rdx_u X2AP)

set(INSTALL_INCLUDE_DIR share/oai/x2ap)
set(OAI_X2AP_ASN1C_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/MESSAGES)
if(NOT IS_ABSOLUTE  ${OAI_X2AP_ASN1C_SOURCE_DIR})
  message(AUTHOR_WARNING "oai_x2ap:: OAI_X2AP_ASN1C_SOURCE_DIR=${OAI_X2AP_ASN1C_SOURCE_DIR} not absolute... ")
endif()
set(_conf_p    "${PROJECT_BINARY_DIR}/CMakeFiles/oai_x2ap_asn1c/oai_x2ap_asn1c-config.cmake")
#set(_prj_bin_d "${PROJECT_BINARY_DIR}/src/ep_oai_x2ap_asn1c-build/CMakeFiles")
set(_prj_bin_d "${PROJECT_BINARY_DIR}/src/ep_oai_x2ap_asn1c-build")
set(_prj_bin_p "${_prj_bin_d}/oai_x2ap_asn1c-config.cmake")


set(_cmake_args
  -DX2AP_VERSION:STRING=${X2AP_VERSION}
  -DINSTALL_INCLUDE_DIR:PATH=${INSTALL_INCLUDE_DIR}
  -DCMAKE_MODULE_PATH:PATH=${CMAKE_MODULE_PATH}
  -DCMAKE_INSTALL_PREFIX:PATH=${PROJECT_BINARY_DIR}
  )

ExternalProject_Add(ep_oai_x2ap_asn1c
  PREFIX ${PROJECT_BINARY_DIR}
  DEPENDS ep_asn1c
  SOURCE_DIR  ${OAI_X2AP_ASN1C_SOURCE_DIR}
  CMAKE_ARGS ${_cmake_args}
  INSTALL_DIR ${PROJECT_BINARY_DIR}
  #STEP_TARGETS configure build install
  BUILD_BYPRODUCTS "${_conf_p}" ${OAI_X2AP_ASN1C_SRCS}
  COMMENT "installing OAI X2AP ASN1C generated file"
  )
ExternalProject_Add_Step(ep_oai_x2ap_asn1c config
  DEPENDEES install
  COMMAND ${CMAKE_COMMAND} -E remove "${_conf_p}"
  COMMAND $(MAKE) -C${CMAKE_BINARY_DIR} cmake_check_build_system
  BYPRODUCTS "${_conf_p}"
  COMMENT "asn1c OAI X2AP: config is available once install is done"
  )

if(NOT EXISTS "${_conf_p}")
  message(AUTHOR_WARNING "oai_x2ap::bum...did not find ways to pre-generate ${_conf_p}"
    " by calling ${CMAKE_COMMAND}  ${OAI_X2AP_ASN1C_SOURCE_DIR} ${_cmake_args}")

  file(MAKE_DIRECTORY ${_prj_bin_d})
  execute_process(
    COMMAND ${CMAKE_COMMAND} -Wdev --trace-expand ${_cmake_args} -B${_prj_bin_d} -H${OAI_X2AP_ASN1C_SOURCE_DIR}
    RESULT_VARIABLE _e
    )
  if(NOT EXISTS "${_prj_bin_p}")
    message(FATAL_ERROR "${_e}")
  endif()
  configure_file("${_prj_bin_p}"
    "${_conf_p}"
    COPYONLY
    )
endif()

include("${_conf_p}")


list(APPEND OAI_X2AP_INCLUDE_DIRS
  ${OAI_X2AP_ASN1C_INSTALL_INCLUDE_DIR}
  #  ${OAI_X2AP_ASN1C_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}
  )

message(STATUS "oai_x2ap::OAI_X2AP_ASN1C_INCLUDE_DIRS=${OAI_X2AP_ASN1C_INCLUDE_DIRS} ->- ${OAI_X2AP_INCLUDE_DIRS}")

include_directories(${OAI_X2AP_INCLUDE_DIRS})

set_source_files_properties(${OAI_X2AP_ASN1C_SRCS}
  PROPERTIES
  GENERATED true
  )
add_custom_target(oai_x2ap_regen
  SOURCES
  ${OAI_X2AP_ASN1C_SRCS}
  "${_conf_p}"
  )
add_dependencies(oai_x2ap_regen ep_oai_x2ap_asn1c)

set(OAI_X2AP_SOURCES
  x2ap_common.c
  x2ap_common.h
  )

foreach(_f ${OAI_X2AP_ASN1C_SOURCES} ${OAI_X2AP_SOURCES})
  if("${_f}" MATCHES ".*\\.h$")
    list(APPEND _x2ap_h ${_f})
  endif()
endforeach(_f)

add_library(X2AP_LIB SHARED
  ${OAI_X2AP_SOURCES}
  ${OAI_X2AP_ASN1C_SRCS}
  )
target_link_libraries(X2AP_LIB
  #PTZ170215 T_cache ...
  ${ITTI_LIB}
  #PTZ170210
  #enb_config/_properties NB_eNB_INST hooked into L2/ENB_APP_SRC....
  #L2
  )
add_dependencies(X2AP_LIB
  ep_oai_x2ap_asn1c
  ##TODO: oai_x2ap_regen
  generate_T
  )

set_target_properties(X2AP_LIB
  PROPERTIES
  PUBLIC_HEADER "${_x2ap_h}"
  LINKER_LANGUAGE CXX
  )

install(TARGETS X2AP_LIB
  EXPORT openairinterface-targets
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin
  LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT shlib
  ARCHIVE DESTINATION "${INSTALL_LIB_DIR}" COMPONENT lib
  PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}" COMPONENT dev
  )

set(OAI_X2AP_INCLUDE_DIRS ${OAI_X2AP_INCLUDE_DIRS} PARENT_SCOPE)
set(OAI_X2AP_ASN1C_INSTALL_INCLUDE_DIR ${OAI_X2AP_ASN1C_INSTALL_INCLUDE_DIR} PARENT_SCOPE)
set(OAI_X2AP_ASN1C_SRCS ${OAI_X2AP_ASN1C_SRCS} PARENT_SCOPE)
set(OAI_X2AP_ASN1C_SOURCES ${OAI_X2AP_ASN1C_SOURCES} PARENT_SCOPE)

message(STATUS oai_x2ap::OAI_X2AP_ASN1C_INCLUDE_DIRS=
  ${OAI_X2AP_ASN1C_INSTALL_INCLUDE_DIR}.../asn1_constants.h
  " -OAI_X2AP_INCLUDE_DIRS-" ${OAI_X2AP_INCLUDE_DIRS}
  " --sources>=${OAI_X2AP_ASN1C_SOURCES}"
  " --sources>=${OAI_X2AP_ASN1C_SRCS}"
  )
