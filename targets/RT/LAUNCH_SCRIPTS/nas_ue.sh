#!/bin/bash

set -x

##############################################################
#   check for open air interface working dir is availability
##############################################################
ORIGIN_PATH=$PWD
THIS_SCRIPT_PATH=$(dirname $(readlink -f $0))

OPENAIR_DIR=${OPENAIR_DIR:-${PROJECT_SOURCE_DIR}}
OPENAIR_DIR=${OPENAIR_DIR:-${THIS_SCRIPT_PATH%/targets/*}}

PROJECT_BINARY_DIR=${PROJECT_BINARY_DIR:-${OPENAIR_DIR}/bld}
OPENAIR_BUILD_d=${PROJECT_BINARY_DIR}

## TODO: PTZ161027 fall back to installed package...    #pkg-config oai
if [ ! -f ${OPENAIR_BUILD_d}/Makefile ]
then
    echo "script ran under installed package. might not work"
    OPENAIR_BUILD_d=/usr/local
    #exit 1
fi

## TODO: PTZ161027 a global common oai environment setup  helper!
if [ -n "" ] 
then
    if [ -s $OPENAIR_DIR/cmake_targets/tools/build_helper ]
    then
        source $OPENAIR_DIR/cmake_targets/tools/build_helper
        set_openair_env
    else
        echo "could not find a valid open-air-interface source space: OPENAIR_DIR = ${OPENAIR_DIR}"
    fi
    if [ -f $OPENAIR_DIR/cmake_targets/tools/test_helper ]
    then
        source $OPENAIR_DIR/cmake_targets/tools/test_helper
    fi
fi

echo "setting up UE NAS interface"

# PTZ161027 cd $OPENAIR2_DIR

if [ -f ${PROJECT_BINARY_DIR}/openair2/NETWORK_DRIVER/Makefile ]
then
    
#./openair2/NETWORK_DRIVER/MESH/RB_TOOL/rb_tool.c
#./openair2/NETWORK_DRIVER/LITE/RB_TOOL/rb_tool.c
#


    #make nasmesh_netlink_address_fix.ko 
    #make nasmesh_netlink.ko
    #make nasmesh.ko
    #make rb_tool
    make -C ${PROJECT_BINARY_DIR}/openair2/NETWORK_DRIVER
fi


#route add -net 224.0.0.0 netmask 240.0.0.0 dev eth0

#cd -
#PTZ161027sudo insmod $OPENAIR2_DIR/NAS/DRIVER/MESH/nasmesh.ko
## TODO: PTZ161027 another arch?
#_arch=$(uname -r)
## TODO: PTZ161027  needed when module is old? sudo rmmod oai-nasmesh
## TODO: PTZ161027 
#modules would be installed in /lib/modules/${_arch}/updates/net/wireless/ ?
#/var/lib/dkms/oai/1-PTZ160803_x86_emu/4.7.0-1-amd64/x86_64/module
#sudo insmod ${OPENAIR_BUILD_d}/lib/modules/${_arch}/updates/dkms/oai-nasmesh.ko
## TODO: PTZ161021 other archs...
_karch=$(uname -r)
#PTZ161027sudo insmod $OPENAIR2_DIR/NAS/DRIVER/MESH/nasmesh.ko 
## TODO: PTZ161027  needed when module is old? sudo rmmod oai-nasmesh
_m_ko_p=$OPENAIR_DIR/targets/bin/nasmesh.ko
[ -f ${_m_ko_p} ] || _m_ko_p=${OPENAIR_BUILD_d}/lib/modules/${_karch}/driver/net/wireless/oai-nasmesh.ko
[ -f ${_m_ko_p} ] || _m_ko_p=${OPENAIR_BUILD_d}/lib/modules/${_karch}/updates/dkms/oai-nasmesh.ko
[ -f ${_m_ko_p} ] || _m_ko_p=${OPENAIR_BUILD_d}/var/lib/dkms/oai-nasmesh/1-PTZ160803_x86_emu/${_karch}/x86_64/module/oai-nasmesh.ko
## TODO: PTZ161027 
#modules would be installed in /lib/modules/${_arch}/updates/net/wireless/ ?
#/var/lib/dkms/oai/1-PTZ160803_x86_emu/4.7.0-1-amd64/x86_64/module
#sudo insmod ${OPENAIR_BUILD_d}/lib/modules/${_arch}/updates/dkms/oai-nasmesh.ko
if [ `declare -f -F load_module` ]
then
    load_module ${_m_ko_p}
else
    sudo insmod ${_m_ko_p}
fi

sudo ifconfig oai0 10.0.1.2 netmask 255.255.255.0 broadcast 10.0.1.255

#PTZ161027 $OPENAIR2_DIR/NAS/DRIVER/MESH/RB_TOOL/rb_tool -a -c0 -i0 -z0 -s 10.0.1.2 -t 10.0.1.1 -r 3
#${OPENAIR_BUILD_d}/bin/oai_mesh_rb_tool -a -c0 -i0 -z0 -s 10.0.1.2 -t 10.0.1.1 -r 3 
_rb_tool_p=${OPENAIR_DIR}/targets/bin/rb_tool
[ -x ${_rb_tool_p} ] || _rb_tool_p=${OPENAIR_BUILD_d}/openair2/rb_tool
[ -x ${_rb_tool_p} ] || _rb_tool_p=${OPENAIR_BUILD_d}/bin/rb_tool
[ -x ${_rb_tool_p} ] || _rb_tool_p=${OPENAIR_BUILD_d}/bin/nas_mesh_rb_tool
${_rb_tool_p} -a -c0 -i0 -z0 -s 10.0.1.2 -t 10.0.1.1 -r 3 



echo "end setting up NAS interface"
