#!/bin/bash
set -x
ASN1C_CMD=${ASN1C_CMD:-$(which asn1c)}

function asn1c_run {
    echo running ${ASN1C_CMD}
    ${ASN1C_CMD} -gen-PER -fcompound-names $* 2>&1 \
        | grep -v -- '->' \
        | grep -v '^Compiled' \
        | grep -v sample
    echo done running ${ASN1C_CMD}
}


which asn1c > /dev/null
if [ $? -eq 0 ] ; then echo "asn1c is installed" ; else echo "Please install asn1c (version 0.9.22 or greater)" ; fi

cd $OPENAIR2_DIR/RRC/LITE/MESSAGES \
    && ${ASN1C_CMD} -gen-PER -fcompound-names -fnative-types -fskeletons-copy $OPENAIR2_DIR/RRC/LITE/MESSAGES/asn1c/ASN1_files/EUTRA-RRC-Definitions.asn


